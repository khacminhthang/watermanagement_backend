﻿using WaterManagement.Models.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WaterManagement.Interface.IDAO
{
    public interface IGDuanDAO
    {
        /// <summary>
        /// get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<GDuan> GetById(int id);
        /// <summary>
        /// get all
        /// </summary>
        /// <returns></returns>
        Task<IList<GDuan>> GetAll();
        /// <summary>
        /// insert
        /// </summary>
        /// <param name="gDuan"></param>
        /// <returns></returns>
        Task InsertGDuan(GDuan gDuan);
        /// <summary>
        /// update
        /// </summary>
        /// <param name="gDuan"></param>
        /// <returns></returns>
        Task UpdateGDuan(GDuan gDuan);
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="gDuan"></param>
        /// <returns></returns>
        Task DeleteGDuan(GDuan gDuan);

    }
}
