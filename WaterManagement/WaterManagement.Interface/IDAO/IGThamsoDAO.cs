﻿using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WaterManagement.Interface.IDAO
{
    public interface IGThamsoDAO
    {
        /// <summary>
        /// get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<GThamso> GetById(int id);
        /// <summary>
        /// get all
        /// </summary>
        /// <returns></returns>
        Task<IList<GThamsoViewModel>> GetAll();
        /// <summary>
        /// insert
        /// </summary>
        /// <param name="thamso"></param>
        /// <returns></returns>
        Task Insert(GThamso thamso);
        /// <summary>
        /// update
        /// </summary>
        /// <param name="thamso"></param>
        /// <returns></returns>
        Task Update(GThamso thamso);
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="thamso"></param>
        /// <returns></returns>
        Task Delete(GThamso thamso);
    }
}
