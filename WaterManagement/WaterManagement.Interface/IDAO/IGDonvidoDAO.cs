﻿using WaterManagement.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WaterManagement.Interface.IDAO
{
    public interface IGDonvidoDAO
    {
        /// <summary>
        /// Get expression
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        Task<GDonvido> Get(Expression<Func<GDonvido, bool>> expression);

        /// <summary>
        ///  Get list
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        Task<IList<GDonvido>> GetList();

        /// <summary>
        /// Insert 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task Insert(GDonvido model);

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task Update(GDonvido model);

        /// <summary>
        /// Delete 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task Delete(GDonvido model);
    }
}
