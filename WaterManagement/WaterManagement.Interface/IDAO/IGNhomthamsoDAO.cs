﻿using WaterManagement.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WaterManagement.Interface.IDAO
{
    public interface IGNhomthamsoDAO
    {
        /// <summary>
        /// Get expression
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        Task<GNhomthamso> Get(Expression<Func<GNhomthamso, bool>> expression);

        /// <summary>
        ///  Get list
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        Task<IList<GNhomthamso>> GetList(Expression<Func<GNhomthamso, bool>> expression);

        /// <summary>
        /// Insert GNhomthamso
        /// </summary>
        /// <param name="gNhomthamso"></param>
        /// <returns></returns>
        Task InsertGNhomthamso(GNhomthamso gNhomthamso);

        /// <summary>
        /// Update GNhomthamso
        /// </summary>
        /// <param name="gNhomthamso"></param>
        /// <returns></returns>
        Task UpdateGNhomthamso(GNhomthamso gNhomthamso);

        /// <summary>
        /// Delete GNhomthamso
        /// </summary>
        /// <param name="gNhomthamso"></param>
        /// <returns></returns>
        Task DeleteGNhomthamso(GNhomthamso gNhomthamso);
    }
}
