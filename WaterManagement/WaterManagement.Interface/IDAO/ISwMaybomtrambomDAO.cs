﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;

namespace WaterManagement.Interface.IDAO
{
    public interface ISwMaybomtrambomDAO
    {
        /// <summary>
        /// get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<SwMaybomtrambom> GetById(int id);

        /// <summary>
        /// get all by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<IList<SwMaybomtrambom>> GetAllById(int id);

        /// <summary>
        /// insert
        /// </summary>
        /// <param name="swMaybomtrambom"></param>
        /// <returns></returns>
        Task<SwMaybomtrambom> InsertSwMaybomtrambom(SwMaybomtrambom swMaybomtrambom);

        /// <summary>
        /// update
        /// </summary>
        /// <param name="SwMaybomtrambom"></param>
        /// <returns></returns>
        Task<SwMaybomtrambom> UpdateSwMaybomtrambom(SwMaybomtrambom swMaybomtrambom);

        /// <summary>
        /// delete
        /// </summary>
        /// <param name="SwMaybomtrambom"></param>
        /// <returns></returns>
        Task<SwMaybomtrambom> DeleteSwMaybomtrambom(SwMaybomtrambom swMaybomtrambom);
    }
}
