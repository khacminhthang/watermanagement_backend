﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;

namespace WaterManagement.Interface.IDAO
{
    public interface ISwMaybomdiemkhaithacDAO
    {
        /// <summary>
        /// get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<SwMaybomdiemkhaithac> GetById(int id);

       /// <summary>
       /// get all by id
       /// </summary>
       /// <param name="id"></param>
       /// <returns></returns>
        Task<IList<SwMaybomdiemkhaithac>> GetAll(int id);

        /// <summary>
        /// insert
        /// </summary>
        /// <param name="swMaybomdiemkhaithac"></param>
        /// <returns></returns>
        Task<SwMaybomdiemkhaithac> InsertSwMaybomdiemkhaithac(SwMaybomdiemkhaithac swMaybomdiemkhaithac);

        /// <summary>
        /// update
        /// </summary>
        /// <param name="swMaybomdiemkhaithac"></param>
        /// <returns></returns>
        Task<SwMaybomdiemkhaithac> UpdateSwMaybomdiemkhaithac(SwMaybomdiemkhaithac swMaybomdiemkhaithac);

        /// <summary>
        /// delete
        /// </summary>
        /// <param name="swMaybomdiemkhaithac"></param>
        /// <returns></returns>
        Task<SwMaybomdiemkhaithac> DeleteSwMaybomdiemkhaithac(SwMaybomdiemkhaithac swMaybomdiemkhaithac);
    }
}
