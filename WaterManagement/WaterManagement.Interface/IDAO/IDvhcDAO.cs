﻿using WaterManagement.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WaterManagement.Interface.IDAO
{
    /// <summary>
    /// Dvhc interface
    /// </summary>
    public interface IDvhcDAO
    {
        /// <summary>
        /// Get all
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        Task<IList<Dvhc>> GetAll(Expression<Func<Dvhc, bool>> expression);

        /// <summary>
        /// Get expression
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        Task<Dvhc> Get(Expression<Func<Dvhc, bool>> expression);

        /// <summary>
        /// Insert Dvhc
        /// </summary>
        /// <param name="dvhc"></param>
        /// <returns></returns>
        Task InsertDvhc(Dvhc dvhc);

        /// <summary>
        /// Update Dvhc
        /// </summary>
        /// <param name="dvhc"></param>
        /// <returns></returns>
        Task UpdateDvhc(Dvhc dvhc);

        /// <summary>
        /// Delete dvhc
        /// </summary>
        /// <param name="dvhc"></param>
        /// <returns></returns>
        Task DeleteDvhc(Dvhc dvhc);

        /// <summary>
        /// DeleteRange
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        Task DeleteRange(IList<Dvhc> list);
    }
}
