﻿using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WaterManagement.Interface.IDAO
{
    public interface IGTieuchuanchatluongDAO
    {
        /// <summary>
        /// get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<GTieuchuanchatluong> GetById(int id);
        /// <summary>
        /// get all
        /// </summary>
        /// <returns></returns>
        Task<IList<GTieuchuanchatluongViewModel>> GetAll();
        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="gTieuchuanchatluong"></param>
        /// <returns></returns>
        Task InsertGTieuchuanchatluong(GTieuchuanchatluong gTieuchuanchatluong);
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="gTieuchuanchatluong"></param>
        /// <returns></returns>
        Task UpdateGTieuchuanchatluong(GTieuchuanchatluong gTieuchuanchatluong);
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="gTieuchuanchatluong"></param>
        /// <returns></returns>
        Task DeleteGTieuchuanchatluong(GTieuchuanchatluong gTieuchuanchatluong);

    }
}
