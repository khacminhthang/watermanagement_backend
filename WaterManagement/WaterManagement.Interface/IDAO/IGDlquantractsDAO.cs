﻿using WaterManagement.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WaterManagement.Interface.IDAO
{
    public interface IGDlquantractsDAO
    {
        /// <summary>
        /// Get all
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        Task<IList<GDlquantracts>> GetAll(Expression<Func<GDlquantracts, bool>> expression);
    }
}
