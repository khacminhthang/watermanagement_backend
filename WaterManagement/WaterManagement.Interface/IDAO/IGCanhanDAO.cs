﻿using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WaterManagement.Interface.IDAO
{
    public interface IGCanhanDAO
    {
        /// <summary>
        /// Get expression
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        Task<GCanhan> Get(Expression<Func<GCanhan, bool>> expression);

        /// <summary>
        /// get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<GCanhan> GetbyId(int id);

        /// <summary>
        /// Get all
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        Task<IList<GCanhanViewModel>> GetAll();

        /// <summary>
        /// Insert ca nhan
        /// </summary>
        /// <param name="gcanhan"></param>
        /// <returns></returns>
        Task InsertGCaNhan(GCanhan gcanhan);

        /// <summary>
        /// Delete gcanhan
        /// </summary>
        /// <param name="gcnhan"></param>
        /// <returns></returns>
        Task DeleteGCanhan(GCanhan gcnhan);

        /// <summary>
        /// Update canhan
        /// </summary>
        /// <param name="gcanhan"></param>
        /// <returns></returns>
        Task UpdateGCanhan(GCanhan gcanhan);
    }
}
