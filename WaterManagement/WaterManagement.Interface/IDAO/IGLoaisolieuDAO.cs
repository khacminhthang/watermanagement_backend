﻿using WaterManagement.Models.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WaterManagement.Interface.IDAO
{
    public interface IGLoaisolieuDAO
    {
        /// <summary>
        /// get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<GLoaisolieu> GetById(int id);
        /// <summary>
        /// get all
        /// </summary>
        /// <returns></returns>
        Task<IList<GLoaisolieu>> GetAll();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gLoaisolieu"></param>
        /// <returns></returns>
        Task InsertGLoaisolieu(GLoaisolieu gLoaisolieu);
        /// <summary>
        /// update
        /// </summary>
        /// <param name="gLoaisolieu"></param>
        /// <returns></returns>
        Task UpdateGLoaisolieu(GLoaisolieu gLoaisolieu);
        /// <summary>
        /// delete
        /// </summary>
        /// <param name="gLoaisolieu"></param>
        /// <returns></returns>
        Task DeleteGLoaisolieu(GLoaisolieu gLoaisolieu);

    }
}
