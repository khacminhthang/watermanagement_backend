﻿using WaterManagement.Models.ViewModels;

namespace WaterManagement.Interface.IDAO
{
    /// <summary>
    /// generate geometry 
    /// </summary>
    public interface IGenerateLocationDAO
    {
        /// <summary>
        /// Generate geometry
        /// </summary>
        /// <param name="toax"></param>
        /// <param name="toadoy"></param>
        /// <param name="srId"></param>
        /// <returns></returns>
        GenerateGeometryViewModel GenerateLocation(double toadox, double toadoy, int srId);
    }
}
