﻿using WaterManagement.Models.RequestModel;
using WaterManagement.Models.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WaterManagement.Interface.IDAO
{
    public interface IGObjThamsoDAO
    {
        Task Insert(GObjThamsoRequest model);
        Task<IEnumerable<GObjThamsoViewModel>> Get(int? idObj, short? ObjKey);

    }
}
