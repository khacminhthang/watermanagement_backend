﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;

namespace WaterManagement.Interface.IDAO
{
    /// <summary>
    /// Interface
    /// </summary>
    public interface IGCongtyDAO
    {
        /// <summary>
        /// Get 
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        Task<GCongty> Get(Expression<Func<GCongty, bool>> expression);

        /// <summary>
        /// Get list
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        Task<IList<GCongtyViewModel>> GetAll();

        /// <summary>
        /// Insert GCongty
        /// </summary>
        /// <param name="gCongty"></param>
        /// <returns></returns>
        Task InsertGCongty(GCongty gCongty);

        /// <summary>
        /// Update GCongty
        /// </summary>
        /// <param name="gCongty"></param>
        /// <returns></returns>
        Task UpdateGCongty(GCongty gCongty);

        /// <summary>
        /// Delete GCongty
        /// </summary>
        /// <param name="gCongty"></param>
        /// <returns></returns>
        Task DeleteGCongty(GCongty gCongty);
    }
}
