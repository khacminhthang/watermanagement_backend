﻿using WaterManagement.Models.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WaterManagement.Interface.IDAO
{
    /// <summary>
    /// GPhantich interface
    /// </summary>
    public interface IGPhantichDAO
    {
        /// <summary>
        /// Get by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<GPhantich> GetbyId(int id);

        /// <summary>
        /// Get all GPhantich
        /// </summary>
        /// <returns></returns>
        Task<IList<GPhantich>> GetAllGPhantich();

        /// <summary>
        /// Insert GPhantich
        /// </summary>
        /// <param name="gPhantich"></param>
        /// <returns></returns>
        Task InsertGPhantich(GPhantich gPhantich);

        /// <summary>
        /// Update GPhantich
        /// </summary>
        /// <param name="gPhantich"></param>
        /// <returns></returns>
        Task UpdateGPhantich(GPhantich gPhantich);

        /// <summary>
        /// Delete GPhantich
        /// </summary>
        /// <param name="gPhantich"></param>
        /// <returns></returns>
        Task DeleteGPhantich(GPhantich gPhantich);
    }
}
