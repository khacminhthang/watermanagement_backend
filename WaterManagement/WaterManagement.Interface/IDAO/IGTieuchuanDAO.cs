﻿using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WaterManagement.Interface.IDAO
{
    public interface IGTieuchuanDAO
    {
        /// <summary>
        /// get by id
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        Task<GTieuchuan> GetById(int id);
        /// <summary>
        /// get all
        /// </summary>
        /// <returns></returns>
        Task<IList<GTieuchuanViewModel>> GetAll();
        /// <summary>
        /// insert
        /// </summary>
        /// <param name="gTieuchuan"></param>
        /// <returns></returns>
        Task InsertGTieuChuan(GTieuchuan gTieuchuan);
        /// <summary>
        /// update
        /// </summary>
        /// <param name="gTieuchuan"></param>
        /// <returns></returns>
        Task UpdateGTieuChuan(GTieuchuan gTieuchuan);
        /// <summary>
        /// delete
        /// </summary>
        /// <param name="gTieuchuan"></param>
        /// <returns></returns>
        Task DeleteGTieuChuan(GTieuchuan gTieuchuan);
    }
}
