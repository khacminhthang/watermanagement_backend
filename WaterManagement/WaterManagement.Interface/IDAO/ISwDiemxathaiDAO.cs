﻿using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WaterManagement.Interface.IDAO
{
    /// <summary>
    /// SwDiemxathai interface
    /// </summary>
    public interface ISwDiemxathaiDAO
    {
        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<SwDiemxathai> GetbyId(int id);

        /// <summary>
        /// Get all
        /// </summary>
        /// <returns></returns>
        Task<IList<SwDiemxathaiViewModel>> GetAll();

        /// <summary>
        /// Insert SwDiemxathai
        /// </summary>
        /// <param name="swDiemxathai"></param>
        /// <returns></returns>
        Task<SwDiemxathai> InsertSwDiemxathai(SwDiemxathai swDiemxathai);

        /// <summary>
        /// Update SwDiemxathai
        /// </summary>
        /// <param name="swDiemxathai"></param>
        /// <returns></returns>
        Task<SwDiemxathai> UpdateSwDiemxathai(SwDiemxathai swDiemxathai);

        /// <summary>
        /// Delete SwDiemxathai
        /// </summary>
        /// <param name="swDiemxathai"></param>
        /// <returns></returns>
        Task<SwDiemxathai> DeleteSwDiemxathai(SwDiemxathai swDiemxathai);
    }
}
