﻿using WaterManagement.Models.RequestModel;
using WaterManagement.Models.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WaterManagement.Interface.IDAO
{
    /// <summary>
    /// GObjmedia interface
    /// </summary>
    public interface IGObjmediaDAO
    {
        Task Insert(CreateGObjMedia model);
        Task<IEnumerable<GObjmediaViewModel>> Get(int IdObj, short? ObjKey, int? IdObjmediaType);
    }
}
