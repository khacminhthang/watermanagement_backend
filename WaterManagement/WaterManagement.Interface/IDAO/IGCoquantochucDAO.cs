﻿using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WaterManagement.Interface.IDAO
{
    /// <summary>
    /// Organnization interface
    /// </summary>
    public interface IGCoquantochucDAO
    {
        /// <summary>
        /// Get  by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<GCoquantochuc> Get(Expression<Func<GCoquantochuc, bool>> expression);

        /// <summary>
        /// Tree list 
        /// </summary>
        /// <returns></returns>
        Task<IList<GCoquantochucViewModel>> GetListCoquantochuc();

        /// <summary>
        /// Insert GCoquantochuc
        /// </summary>
        /// <param name="gCoquantochuc"></param>
        /// <returns></returns>
        Task InsertGCoquantochuc(GCoquantochuc gCoquantochuc);

        /// <summary>
        /// Update GCoquantochuc
        /// </summary>
        /// <param name="gCoquantochuc"></param>
        /// <returns></returns>
        Task UpdateGCoquantochuc(GCoquantochuc gCoquantochuc);

        /// <summary>
        /// Delete GCoquantochuc
        /// </summary>
        /// <param name="gCoquantochuc"></param>
        /// <returns></returns>
        Task DeleteGCoquantochuc(GCoquantochuc gCoquantochuc);
        /// <summary>
        /// Delete Range
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        Task UpdateRange(List<GCoquantochuc> list);
    }
}
