﻿using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WaterManagement.Interface.IDAO
{
    /// <summary>
    /// SwDiemkhaithac interface
    /// </summary>
    public interface ISwDiemkhaithacDAO
    {
        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<SwDiemkhaithac> GetbyId(int id);

        /// <summary>
        /// Get all
        /// </summary>
        /// <returns></returns>
        Task<IList<SwDiemkhaithacViewModel>> GetAll();

        /// <summary>
        /// Insert SwDiemkhaithac
        /// </summary>
        /// <param name="swDiemkhaithac"></param>
        /// <returns></returns>
        Task<SwDiemkhaithac> InsertSwDiemkhaithac(SwDiemkhaithac swDiemkhaithac);

        /// <summary>
        /// Update SwDiemkhaithac
        /// </summary>
        /// <param name="swDiemkhaithac"></param>
        /// <returns></returns>
        Task<SwDiemkhaithac> UpdateSwDiemkhaithac(SwDiemkhaithac swDiemkhaithac);

        /// <summary>
        /// Delete SwDiemkhaithac
        /// </summary>
        /// <param name="swDiemkhaithac"></param>
        /// <returns></returns>
        Task<SwDiemkhaithac> DeleteSwDiemkhaithac(SwDiemkhaithac swDiemkhaithac);
    }
}
