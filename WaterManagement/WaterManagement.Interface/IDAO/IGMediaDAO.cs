﻿using WaterManagement.Models.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WaterManagement.Interface.IDAO
{
    /// <summary>
    /// GMedia interface
    /// </summary>
    public interface IGMediaDAO
    {
        /// <summary>
        /// Get by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<GMedia> GetGMediaById(int id);

        /// <summary>
        /// Get all
        /// </summary>
        /// <returns></returns>
        Task<IList<GMedia>> GetAllGMedia();

        /// <summary>
        /// Insert GMedia
        /// </summary>
        /// <param name="gMedia"></param>
        GMedia InsertGMedia(GMedia gMedia);
    }
}
