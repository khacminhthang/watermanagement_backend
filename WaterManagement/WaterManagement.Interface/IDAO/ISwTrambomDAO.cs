﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;

namespace WaterManagement.Interface.IDAO
{
    public interface ISwTrambomDAO
    {
        /// <summary>
        /// get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<SwTrambom> GetById(int id);

        /// <summary>
        /// get all
        /// </summary>
        /// <returns></returns>
        Task<IList<SwTrambomViewModel>> GetAll();

        /// <summary>
        /// insert
        /// </summary>
        /// <param name="swTrambom"></param>
        /// <returns></returns>
        Task<SwTrambom> InsertSwTrambom(SwTrambom swTrambom);

        /// <summary>
        /// update
        /// </summary>
        /// <param name="swTrambom"></param>
        /// <returns></returns>
        Task<SwTrambom> UpdateSwTranbom(SwTrambom swTrambom);

        /// <summary>
        /// delete
        /// </summary>
        /// <param name="swTrambom"></param>
        /// <returns></returns>
        Task<SwTrambom> DeleteSwTrambom(SwTrambom swTrambom);
    }
}
