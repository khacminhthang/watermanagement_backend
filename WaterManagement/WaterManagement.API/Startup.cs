using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WaterManagement.DAO.DAO;
using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using WaterManagement.Business.Business;
using Autofac;
using WaterManagement.DAO.DVHC;
using Microsoft.AspNetCore.Identity;
using Segment.Model;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System;

namespace WaterManagement.API
{
    public class Startup
    {

        public IConfiguration _configuration { get; }
        public IContainer _applicationContainer { get; private set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            var builder = new ConfigurationBuilder();

            builder.AddEnvironmentVariables();
            _configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddEntityFrameworkNpgsql().AddDbContext<DatabaseContext>(opt =>
                opt.UseNpgsql(Configuration.GetConnectionString("DefaultConnection"), o => o.UseNetTopologySuite()));

            //services.AddIdentity<User, IdentityRole>()
            //   .AddEntityFrameworkStores<UserDbContext>()
            //   .AddDefaultTokenProviders();

            // add dbContext Postgresql database
            //services.AddDbContext<DatabaseContext>(options =>
            //{
            //    options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection"), o => o.UseNetTopologySuite());
            //    options.UseLazyLoadingProxies(true);
            //});

            services.AddIdentity<User, IdentityRole>()
            .AddEntityFrameworkStores<DatabaseContext>();

            services.AddControllers().AddNewtonsoftJson();

            services.AddScoped<IUnitOfWork,UnitOfWork>();

            services.AddScoped(typeof(IEfRepository<>), typeof(EfRepository<>));

            services.AddScoped<IGenerateLocationDAO, GenerateLocationDAO>();

            services.AddScoped<IGMediaDAO, GMediaDAO>();
            services.AddScoped<GMediaBusiness>();

            services.AddScoped<IGPhantichDAO, GPhantichDAO>();
            services.AddScoped<GPhantichBusiness>();

            services.AddScoped<IDvhcDAO, DvhcDAO>();
            services.AddScoped<DvhcBusiness>();

            services.AddScoped<IGObjmediaDAO, GObjmediaDAO>();
            services.AddScoped<IGObjThamsoDAO, GObjThamsoDAO>();

            services.AddScoped<IGNhomthamsoDAO, GNhomthamsoDAO>();
            services.AddScoped<GNhomthamsoBusiness>();

            services.AddScoped<IGCanhanDAO, GCanhanDAO>();
            services.AddScoped<GCanhanBusiness>();

            services.AddScoped<IGCongtyDAO, GCongtyDAO>();
            services.AddScoped<GCongtyBusiness>();

            services.AddScoped<IGCoquantochucDAO, GCoquantochucDAO>();
            services.AddScoped<GCoquantochucBusiness>();

            services.AddScoped<IGDlquantractsDAO, GDlquantractsDAO>();

            services.AddScoped<IGDonvidoDAO, GDonvidoDAO>();
            services.AddScoped<GDonvidoBusiness>();

            services.AddScoped<IGLoaisolieuDAO, GLoaisolieuDAO>();
            services.AddScoped<GLoaisolieuBusiness>();

            services.AddScoped<IGDuanDAO, GDuanDAO>();
            services.AddScoped<GDuanBusiness>();

            services.AddScoped<IGThamsoDAO, GThamsoDAO>();
            services.AddScoped<GThamsoBusiness>();

            services.AddScoped<IGTieuchuanDAO, GTieuchuanDAO>();
            services.AddScoped<GTieuchuanBusiness>();

            services.AddScoped<IGTieuchuanchatluongDAO, GTieuchuanchatluongDAO>();
            services.AddScoped<GTieuchuanchatluongBusiness>();

            services.AddScoped<ISwDiemkhaithacDAO, SwDiemkhaithacDAO>();
            services.AddScoped<SwDiemkhaithacBusiness>();

            services.AddScoped<ISwDiemxathaiDAO, SwDiemxathaiDAO>();
            services.AddScoped<SwDiemxathaiBusiness>();

            services.AddScoped<ISwMaybomdiemkhaithacDAO, SwMaybomdiemkhaithacDAO>();
            services.AddScoped<SwMaybomdiemkhaithacBusiness>();

            services.AddScoped<ISwMaybomtrambomDAO, SwMaybomtrambomDAO>();
            services.AddScoped<SwMaybomtrambomBusiness>();

            services.AddScoped<ISwTrambomDAO, SwTrambomDAO>();
            services.AddScoped<SwTrambomBusiness>();

            //Jwt Authentication

            var key = Encoding.UTF8.GetBytes("1234567890123456");

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x => {
                x.RequireHttpsMetadata = false;
                x.SaveToken = false;
                x.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                };
            });

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 4;
            }
            );

            //services.Configure<IdentityOptions>(options =>
            //{
            //    options.Password.RequireDigit = false;
            //    options.Password.RequireNonAlphanumeric = false;
            //    options.Password.RequireLowercase = false;
            //    options.Password.RequireUppercase = false;
            //    options.Password.RequiredLength = 4;
            //}
            //);

            services.AddControllers();
            services.AddScoped<DbContext, DatabaseContext>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();


            // add swagger
            services.AddSwaggerGen();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();

            app.UseHttpsRedirection();

            app.UseCors(x => x
            .AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader());

            app.UseRouting();

            app.UseAuthorization();

            app.UseStaticFiles();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Thanglkm's demo");
                c.RoutePrefix = string.Empty;
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        /// <summary>
        /// servicecollection extensions
        /// </summary>
        /// <param name="services"></param>

        //[Obsolete]
        //private void ApplicationBuilder(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IHostingEnvironment env, ILoggerFactory loggerFactory)
        //{
        //    app.SetupEnv(env, loggerFactory, Configuration);

        //    app.ConfigSwagger();
        //}

    }
}
