﻿using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using WaterManagement.Models.RequestModel;
using WaterManagement.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WaterManagement.API.Controllers
{

    [Route("api/objthamso")]
    [ApiController]
    public class GObjThamsoController : BaseController<GObjThamsoController>
    {
        private readonly IEfRepository<GObjThamso> _efRepository;
        private readonly IEfRepository<GThamso> _gThamsoRepository;
        private readonly IEfRepository<GDonvido> _gDonvidoRepository;
        private readonly IEfRepository<GNhomthamso> _gNhomthamsoRepository;
        private readonly IUnitOfWork _unitOfWork;
        public GObjThamsoController(IEfRepository<GObjThamso> efRepository,
                                                    IUnitOfWork unitOfWork,
                                                    IEfRepository<GThamso> gThamsoRepository,
                                                    IEfRepository<GDonvido> gDonvidoRepository,
                                                    IEfRepository<GNhomthamso> gNhomthamsoRepository)
        {
            _efRepository = efRepository;
            _unitOfWork = unitOfWork;
            _gThamsoRepository = gThamsoRepository;
            _gDonvidoRepository = gDonvidoRepository;
            _gNhomthamsoRepository = gNhomthamsoRepository;
        }

        /// <summary>
        /// Get with idObj and ObjKey
        /// </summary>
        /// <param name="idObj"></param>
        /// <param name="ObjKey"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] int idObj, short ObjKey)
        {
            if (idObj == 0 || ObjKey == 0)
                return BadRequest("Bạn chưa nhập ID các thông số");

            var query = (from a in _efRepository.TableNoTracking
                         join b in _gThamsoRepository.TableNoTracking on a.IdThamso equals b.Id into bb
                         from b in bb.DefaultIfEmpty()
                         join c in _gDonvidoRepository.TableNoTracking on b.IdDonvidomacdinh equals c.Id into cc
                         from c in cc.DefaultIfEmpty()
                         join d in _gNhomthamsoRepository.TableNoTracking on b.IdNhomthamso equals d.Id into dd
                         from d in dd.DefaultIfEmpty()
                         where a.IdObj == idObj && a.ObjKey == ObjKey
                         select new GObjThamsoViewModel
                         {
                             Id = a.Id,
                             IdObj = a.IdObj,
                             Idthamso = a.IdThamso,
                             ObjKey = a.ObjKey,
                             Tenthamso = b.Tenthamso,
                             Tennhom = d.Tennhom,
                             Tendonvido = c.Tendonvido,
                             Kyhieuthamso = b.Kyhieuthamso
                         }

                );

            if (query == null)
                return NoContent();

            var data = await query.ToListAsync();

            return Ok(data);
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id == 0)
                return BadRequest("Bạn chưa nhập Id");

            var toDelete = _efRepository.SingleOrDefault(a => a.Id == id);

            if (toDelete == null)
                return NoContent();

            _efRepository.Delete(toDelete);
            await _unitOfWork.SaveChangeAsync();

            return Ok();
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Insert([FromBody] GObjThamsoRequest request)
        {

            if (request.JsonParameter == null)
                return BadRequest();

            var list = new List<GObjThamso>();
            foreach (var item in request.JsonParameter)
            {
                var tempModel = new GObjThamso();
                tempModel.IdObj = item.IdObj;
                tempModel.IdThamso = item.IdThamso;
                tempModel.ObjKey = item.ObjKey;

                list.Add(tempModel);
            }

            _efRepository.InsertRange(list);
            await _unitOfWork.SaveChangeAsync();

            return Ok();
        }
    }
}
