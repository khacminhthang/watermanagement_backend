﻿using WaterManagement.Business.Business;
using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using WaterManagement.Models.RequestModel;
using WaterManagement.Models.ViewModels;
using WaterManagement.Utils.Common.Extensions;
using WaterManagement.Utils.Common.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WaterManagement.API.Controllers
{
    /// <summary>
    /// Gmedia Controller
    /// </summary>
    [Route("api/gmedia")]
    [ApiController]
    public class GMediaController : BaseController<GMediaController>
    {
        #region Fields

        private readonly GMediaBusiness _gMediaBusiness;
        private readonly IGMediaDAO _gMediaDAO;
        private readonly IEfRepository<GMedia> _efRepository;

        #endregion

        #region Ctor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="gMediaBusiness"></param>
        public GMediaController(GMediaBusiness gMediaBusiness, IGMediaDAO gMediaDAO, IEfRepository<GMedia> efRepository)
        {
            _gMediaBusiness = gMediaBusiness;
            _gMediaDAO = gMediaDAO;
            _efRepository = efRepository;
        }

        #endregion

        #region Method

        /// <summary>
        /// get all
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] int PageSize, int PageNumber, string Type)
        {
            if (PageSize == -1)
                PageSize = int.MaxValue - 1;


            var query = _efRepository.TableNoTracking.Where(a => a.DeletedAt == 0);

            var fileDto = query.Select(a => new GMediaViewModel()
            {
                Id = a.Id,
                IdObj = a.IdObj,
                Link = a.Link,
                Tieude = a.Tieude,
                Thoigiankhoitao = a.Thoigiankhoitao,
                IdLoaitailieu = a.IdLoaitailieu,
                Tacgia = a.Tacgia,
                Nguoiky = a.Nguoiky,
                Note = a.Note,
                CreatedAt = a.CreatedAt,
                UpdatedAt = a.UpdatedAt,
                Checked = false,
                IconFile = "",
                TenFile = Handlers.GetFile(a.Link, 2),
                Type = Handlers.GetFile(a.Link, 1),
                ObjKey = a.ObjKey
            });

            var list = await fileDto.OrderByDescending(o => o.Id)
                      .ToListAsync();

            var resultlist = new PagedList<GMediaViewModel>(list, PageNumber, PageSize);
            if (Type == "img")
            {
                var data = new List<GMediaViewModel>();
                foreach (var item in list)
                {
                    var type = Handlers.GetFile(item.Link, 1);
                    if (type == ".jpg" || type == ".png" || type == ".jpeg " ||
                        type == ".exif " || type == ".gif " || type == ".tiff " || type == ".bmp " || type == ".webp")
                    {
                        data.Add(item);
                    }
                }
                var res = new PagedList<GMediaViewModel>(data, PageNumber, PageSize);
                if (data == null)
                    return NoContent();

                var output = new
                {
                    Paging = res.GetHeader(),
                    Items = res.List
                };

                return Ok(output);
            }



            return Ok(new
            {
                Paging = resultlist.GetHeader(),
                Items = resultlist.List,
            });


        }


        /// <summary>
        /// Search with tieude
        /// </summary>
        /// <returns></returns>
        [HttpGet("Search")]
        public async Task<IActionResult> Search([FromQuery] int PageSize, int PageNumber, string Type, string Tieude)
        {
            if (PageSize == -1)
                PageSize = int.MaxValue - 1;


            var query = _efRepository.TableNoTracking.Where(a => a.DeletedAt == 0 && a.Tieude.Contains(Tieude));

            var fileDto = query.Select(a => new GMediaViewModel()
            {
                Id = a.Id,
                IdObj = a.IdObj,
                Link = a.Link,
                Tieude = a.Tieude,
                Thoigiankhoitao = a.Thoigiankhoitao,
                IdLoaitailieu = a.IdLoaitailieu,
                Tacgia = a.Tacgia,
                Nguoiky = a.Nguoiky,
                Note = a.Note,
                CreatedAt = a.CreatedAt,
                UpdatedAt = a.UpdatedAt,
                Checked = false,
                IconFile = "",
                TenFile = Handlers.GetFile(a.Link, 2),
                Type = Handlers.GetFile(a.Link, 1),
                ObjKey = a.ObjKey

            });

            var list = await fileDto.OrderByDescending(o => o.Id)
                      .ToListAsync();

            var resultlist = new PagedList<GMediaViewModel>(list, PageNumber, PageSize);
            if (Type == "img")
            {
                var data = new List<GMediaViewModel>();
                foreach (var item in list)
                {
                    var type = Handlers.GetFile(item.Link, 1);
                    if (type == ".jpg" || type == ".png" || type == ".jpeg " ||
                        type == ".exif " || type == ".gif " || type == ".tiff " || type == ".bmp " || type == ".webp")
                    {
                        data.Add(item);
                    }
                }
                var res = new PagedList<GMediaViewModel>(data, PageNumber, PageSize);
                if (data == null)
                    return NoContent();

                var output = new
                {
                    Paging = res.GetHeader(),
                    Items = res.List
                };

                return Ok(output);
            }



            return Ok(new
            {
                Paging = resultlist.GetHeader(),
                Items = resultlist.List,
            });


        }

        /// <summary>
        /// Insert by base 64 and type
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> InsertGMedia([FromBody] GMediaRequest request)
        {
            if (request == null)
                return BadRequest();

            var data = await _gMediaBusiness.InsertGMedia(request);


            return Ok(data.Select(a => new
            {
                id = a.Id,
                url = a.Link
            }).ToList());
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Update(int id, [FromBody] GMedia model)
        {
            if (id == 0)
                return BadRequest();
            await _gMediaBusiness.Update(id, model);
            return Ok();

        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete("{Id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Delete(int Id)
        {
            if (Id == 0)
                return BadRequest("Bạn chưa nhập ID!!");

            await _gMediaBusiness.Delete(Id);

            return Ok();

        }
        #endregion
    }
}