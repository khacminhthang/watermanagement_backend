﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using WaterManagement.Utils.Common.Extensions;
using Microsoft.AspNetCore.Http;
using System.Linq.Dynamic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace WaterManagement.API.Controllers
{
    [Route("api/search")]
    [ApiController]
    public class SeachController : ControllerBase
    {
        private readonly IEfRepository<SwDiemkhaithac> _diemkhaithacRepo;
        private readonly IEfRepository<SwDiemxathai> _diemxathaiRepo;
        private readonly IEfRepository<SwTrambom> _trambomRepo;

        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="giengquantracRepo"></param>
        /// <param name="giengkhaithacRepo"></param>
        public SeachController(
                                 IEfRepository<SwDiemkhaithac> diemkhaithacRepo,
                                 IEfRepository<SwDiemxathai> diemxathaiRepo,
                                 IEfRepository<SwTrambom> trambomRepo)
        {
            _diemkhaithacRepo = diemkhaithacRepo;
            _diemxathaiRepo = diemxathaiRepo;
            _trambomRepo = trambomRepo;
        }


        [HttpGet]
        public async Task<IActionResult> Search([FromQuery] string query, short? objKey)
        {

            if (String.IsNullOrEmpty(query))
                return BadRequest("Bạn chưa nhập tên/số hiệu");

            var queryyy = StringExtensions.ConvertToUnSign(query);
            if (objKey.HasValue)
            {
                // nếu là điểm khai thác
                if (objKey.Value == 5)
                {
                    var data = _diemkhaithacRepo.TableNoTracking;

                    var result = data.AsEnumerable().Where(a => StringExtensions.ConvertToUnSign(a.Tendiem).Contains(queryyy)
                                                       || (String.IsNullOrEmpty(a.Sohieudiem) ? "".Contains(queryyy) : a.Sohieudiem.Contains(queryyy)))
                        .OrderByDescending(o => o.Id);

                    return Ok(result.Select(a => new
                    {
                        Id = a.Id,
                        Tendiem = a.Tendiem,
                        Sohieudiem = a.Sohieudiem,
                        Toadox = a.Toadox,
                        Toadoy = a.Toadoy,
                        Caodoz = a.Caodoz,
                        Srid = a.Srid,
                        Vitri = a.Vitri,
                    }));
                }

                // nếu loại trạm là điểm xả thải
                if (objKey.Value == 6)
                {
                    var data = _diemxathaiRepo.TableNoTracking;

                    var result = data.AsEnumerable().Where(a => StringExtensions.ConvertToUnSign(a.Tendiem).Contains(queryyy)
                                                       || (String.IsNullOrEmpty(a.Sohieudiem) ? "".Contains(queryyy) : a.Sohieudiem.Contains(queryyy)))
                        .OrderByDescending(o => o.Id);

                    return Ok(result.Select(a => new
                    {
                        Id = a.Id,
                        Tendiem = a.Tendiem,
                        Sohieudiem = a.Sohieudiem,
                        Toadox = a.Toadox,
                        Toadoy = a.Toadoy,
                        Caodox = a.Caodoz,
                        Srid = a.Srid,
                        Vitri = a.Vitri,
                    }));
                }

                // nếu loại trạm là trạm bơm
                if (objKey.Value == 7)
                {
                    var data = _trambomRepo.TableNoTracking;

                    var result = data.AsEnumerable().Where(a => StringExtensions.ConvertToUnSign(a.Tentrambom).Contains(queryyy)
                                                       || (String.IsNullOrEmpty(a.Sohieutrambom) ? "".Contains(queryyy) : a.Sohieutrambom.Contains(queryyy)))
                        .OrderByDescending(o => o.Id);

                    return Ok(result.Select(a => new
                    {
                        Id = a.Id,
                        Tendiem = a.Tentrambom,
                        Sohieudiem = a.Sohieutrambom,
                        Toadox = a.Toadox,
                        Toadoy = a.Toadoy,
                        Caodox = a.Caodoz,
                        Srid = a.Srid,
                        Vitri = a.Vitri,
                    }));
                }
            }

            // trường hợp objkey null
            return Ok();
        }
    }
}
