﻿using WaterManagement.Business.Business;
using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using WaterManagement.Utils.Common.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace WaterManagement.API.Controllers
{
    /// <summary>
    /// SwDiemxathai controller
    /// </summary>
    [Route("api/swDiemxathai")]
    [ApiController]
    public class SwDiemxathaiController : BaseController<SwDiemxathaiController>
    {
        #region Fields

        private readonly ISwDiemxathaiDAO _swDiemxathaiDAO;
        private readonly SwDiemxathaiBusiness _swDiemxathaiBusiness;
        #endregion

        #region Ctor

        /// <summary>
        /// Constructor
        /// </summary>
        public SwDiemxathaiController(ISwDiemxathaiDAO swDiemxathaiDAO,
                                      SwDiemxathaiBusiness swDiemxathaiBusiness)
        {
            _swDiemxathaiDAO = swDiemxathaiDAO;
            _swDiemxathaiBusiness = swDiemxathaiBusiness;
        }

        #endregion

        #region Method

        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetbyId(int id)
        {
            if (id <= 0)
                return BadRequest();

            // get a SwDiemxathai with the specified id
            var data = await _swDiemxathaiDAO.GetbyId(id);
            if (data == null)
                return NoContent();

            return Ok(new
            {
                Id = data.Id,
                Sohieudiem = data.Sohieudiem,
                Tendiem = data.Tendiem,
                Toadox = data.Toadox,
                Toadoy = data.Toadoy,
                Caodoz = data.Caodoz,
                Srid = data.Srid,
                Vitri = data.Vitri,
                Matinh = data.Matinh,
                Mahuyen = data.Mahuyen,
                Maxa = data.Maxa,
                Thoigianxaydung = data.Thoigianxaydung,
                Thoigianvanhanh = data.Thoigianvanhanh,
                IdDonviquanly = data.IdDonviquanly,
                IdChusudung = data.IdChusudung,
                Thuyvuctiepnhan = data.Thuyvuctiepnhan,
                Phuongthucxa = data.Phuongthucxa,
                Hinhthucdan = data.Hinhthucdan,
                Luuluongxatrungbinh = data.Luuluongxatrungbinh,
                Luuluonglonnhatchophep = data.Luuluonglonnhatchophep,
                Quychuanapdung = data.Quychuanapdung,
                Cothietbigiamsat = data.Cothietbigiamsat,
                Dacapphep = data.Dacapphep,
                Note = data.Note,
                ImgLink = data.ImgLink
            });
        }

        /// <summary>
        /// Get all
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAllPaging([FromQuery] int pageNumber, int pageSize)
        {
            if (pageNumber <= 0)
                return BadRequest("Số trang phải lớn hơn hoặc bằng 1 !");

            if (pageSize == -1)
                pageSize = int.MaxValue - 1;

            var query = await _swDiemxathaiBusiness.GetAll(pageNumber, pageSize);
            if (query == null)
                return NoContent();

            var data = new
            {
                Paging = query.GetHeader(),
                Items = query.List
            };
            //
            return Ok(data);
        }

        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "Sohieudiem": "ABC123",
        ///         "Tendiem": "Đập test",
        ///         "toadox": -71.1043443253471,
        ///         "toadoy": 42.3150676015829,
        ///         "caodoz": 0,
        ///         "srid": 3405,
        ///         "vitri": "cạnh núi",
        ///         "matinh": 1,
        ///         "mahuyen": 2,
        ///         "maxa": 3,
        ///         "idDonviquanly": 2,
        ///         "Thoigianxaydung": "10/10/2018",
        ///         "thoigianvanhanh": "10/10/2020",
        ///         "IdChusudung": 1,
        ///         "IdSong": 1,
        ///         "Thuyvuctiepnhan": "",
        ///         "Phuongthucxa": "",
        ///         "Hinhthucdan": "",
        ///         "Luuluongxatrungbinh": 0,
        ///         "Luuluonglonnhatchophep": 0,
        ///         "Quychuanapdung": "",
        ///         "Cothietbigiamsat": true,
        ///         "Dacapphep": true,
        ///         "note": "Test thôi mà",
        ///         "ImgLink": "",
        ///     }
        ///
        /// </remarks>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> InsertSwDiemxathai([FromBody] SwDiemxathai model)
        {
            if (model == null)
                return BadRequest();


            if (model.Toadox == null)
                return BadRequest("Bạn phải nhập tọa độ x!");

            if (model.Toadoy == null)
                return BadRequest("Bạn phải nhập tọa độ y!");

            if (model.Srid == null || model.Srid <= 0)
                return BadRequest("Bạn phải nhập SRID!");

            await _swDiemxathaiBusiness.InsertSwDiemxathai(model);


            return Ok();
        }

        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "Sohieudiem": "ABC123",
        ///         "Tendiem": "Đập test",
        ///         "toadox": -71.1043443253471,
        ///         "toadoy": 42.3150676015829,
        ///         "caodoz": 0,
        ///         "srid": 3405,
        ///         "vitri": "cạnh núi",
        ///         "matinh": 1,
        ///         "mahuyen": 2,
        ///         "maxa": 3,
        ///         "idDonviquanly": 2,
        ///         "Thoigianxaydung": "10/10/2018",
        ///         "thoigianvanhanh": "10/10/2020",
        ///         "IdChusudung": 1,
        ///         "IdSong": 1,
        ///         "Thuyvuctiepnhan": "",
        ///         "Phuongthucxa": "",
        ///         "Hinhthucdan": "",
        ///         "Luuluongxatrungbinh": 0,
        ///         "Luuluonglonnhatchophep": 0,
        ///         "Quychuanapdung": "",
        ///         "Cothietbigiamsat": true,
        ///         "Dacapphep": true,
        ///         "note": "Test thôi mà",
        ///         "ImgLink": "",
        ///     }
        ///
        /// </remarks>
        [HttpPut("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateSwDiemxathai(int id, [FromBody] SwDiemxathai model)
        {
            if (model == null)
                return BadRequest();

            if (model.Toadox == null)
                return BadRequest("Bạn phải nhập tọa độ x!");

            if (model.Toadoy == null)
                return BadRequest("Bạn phải nhập tọa độ y!");

            if (model.Srid == null || model.Srid <= 0)
                return BadRequest("Bạn phải nhập SRID!");

            // get a SwDiemxathai with the specified id
            var modelToUpdate = await _swDiemxathaiDAO.GetbyId(id);
            if (modelToUpdate == null)
                return NoContent();

            await _swDiemxathaiBusiness.UpdateSwDiemxathai(modelToUpdate, model);


            return Ok();
        }

        /// <summary>
        /// Delete SwDiemxathai
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteSwDiemxathai(int id)
        {
            if (id <= 0)
                return BadRequest();

            // get a SwDiemxathai with the specified id
            var data = await _swDiemxathaiDAO.GetbyId(id);
            if (data == null)
                return NoContent();

            await _swDiemxathaiBusiness.DeleteSwDiemxathai(data);

            // activity log
            Logger.LogInformation($"SwDiemxathaiDelete id: {id}");

            return Ok();
        }

        #endregion
    }
}