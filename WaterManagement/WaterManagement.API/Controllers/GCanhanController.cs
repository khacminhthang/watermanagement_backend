﻿using WaterManagement.Business.Business;
using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using WaterManagement.Utils.Common.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace WaterManagement.API.Controllers
{
    /// <summary>
    /// Controller
    /// </summary>
    [Route("api/gcanhan")]
    [ApiController]
    public class GCanhanController : BaseController<GCanhanController>
    {
        #region Fields

        private readonly IGCanhanDAO _gCanhanDAO;
        private readonly GCanhanBusiness _gCanhanBusiness;

        #endregion

        #region Ctor
        /// <summary>
        ///  Constructor
        /// </summary>
        /// <param name="gCanhanDAO"></param>
        /// <param name="gCanhanBusiness"></param>
        public GCanhanController(IGCanhanDAO gCanhanDAO,
                                    GCanhanBusiness gCanhanBusiness)
        {
            _gCanhanDAO = gCanhanDAO;
            _gCanhanBusiness = gCanhanBusiness;
        }

        #endregion

        #region Method

        /// <summary>
        /// get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetbyId(int id)
        {
            if (id <= 0)
                return BadRequest();

            // get a gCanhan with the specified id 
            var query = await _gCanhanDAO.GetbyId(id);
            if (query == null)
                return NoContent();

            return Ok(new
            {
                id = query.Id,
                hovaten = query.Hovaten,
                diachi = query.Diachi,
                matinh = query.Matinh,
                mahuyen = query.Mahuyen,
                maxa = query.Maxa,
                sodienthoai = query.Sodienthoai,
                loaigiayto = query.Loaigiayto,
                socmthochieu = query.Socmthochieu,
                note = query.Note,
                imgLink = query.ImgLink,

            });
        }

        /// <summary>
        /// Get all canhan
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAllGCaNhan()
        {
            var data = await _gCanhanDAO.GetAll();
            if (data.Count <= 0)
                return NoContent();

            return Ok(data);
        }

        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "Hovaten": "Le Van A",
        ///         "Diachi": "Hai Duong",
        ///         "Matinh": 23,
        ///         "Mahuyen": 1,
        ///         "Maxa": 1,
        ///         "Sodienthoai": 113113113,
        ///         "Socmthochieu": 113113113,
        ///         "Note": "ahihiahihi",
        ///         "ImgLink": "",
        ///     }
        ///
        /// </remarks>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> InsertGCaNhan([FromBody] GCanhan model)
        {
            if (model == null)
                return BadRequest();

            await _gCanhanBusiness.InsertGCanhan(model);

            Logger.LogInformation($"GCanhanInsert model:{StringExtensions.SerializeConvert(model)}");

            return Ok();
        }

        /// <summary>
        /// delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteGCanhan(int id)
        {
            if (id <= 0)
                return BadRequest();

            var data = _gCanhanDAO.GetbyId(id).Result;
            if (data == null)
                return NoContent();


            await _gCanhanBusiness.DeleteGCanhan(data);

            Logger.LogInformation($"GCanhanDelete id:{id}");

            return Ok();
        }

        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "Hovaten": "Nguyen Van Ten",
        ///         "Diachi": "Hai Duong",
        ///         "Matinh": 23,
        ///         "Mahuyen": 1,
        ///         "Maxa": 1,
        ///         "Sodienthoai": 113113113,
        ///         "Socmthochieu": 113113113,
        ///         "Note": "ahihiahihi",
        ///         "ImgLink": "",
        ///     }
        ///
        /// </remarks>
        [HttpPut("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateGCanhan(int id, [FromBody] GCanhan model)
        {
            if (model == null)
                return BadRequest();

            if (String.IsNullOrEmpty(model.Hovaten))
                return BadRequest("Bạn chưa nhập họ và tên");

            var gCanhanToUpdate = _gCanhanDAO.GetbyId(id).Result;
            if (gCanhanToUpdate == null)
                return NoContent();

            await _gCanhanBusiness.UpdateGCanhan(gCanhanToUpdate, model);

            // activity log
            Logger.LogInformation($"GCanhanUpdate model:{StringExtensions.SerializeConvert(model)}");

            return Ok();
        }

        #endregion
    }
}
