﻿using WaterManagement.Business.Business;
using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using WaterManagement.Utils.Common.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace WaterManagement.API.Controllers
{
    /// <summary>
    /// Controller
    /// </summary>
    [Route("api/gcongty")]
    [ApiController]
    public class GCongtyController : BaseController<GCongtyController>
    {
        #region Fields

        private readonly IGCongtyDAO _gCongtyDAO;
        private readonly GCongtyBusiness _gCongtyBusiness;

        #endregion

        #region Ctor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="gCongtyDAO"></param>
        /// <param name="gCongtyBusiness"></param>
        public GCongtyController(IGCongtyDAO gCongtyDAO,
                                  GCongtyBusiness gCongtyBusiness)
        {
            _gCongtyDAO = gCongtyDAO;
            _gCongtyBusiness = gCongtyBusiness;
        }

        #endregion

        #region Method

        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetById(int id)
        {
            if (id <= 0)
                return BadRequest();

            // get a GCongty the specified id
            var data = await _gCongtyDAO.Get(a => a.Id == id && a.DeletedAt == 0);
            if (data == null)
                return NoContent();

            return Ok(new
            {
                Id = data.Id,
                Tencongty = data.Tencongty,
                Diachi = data.Diachi,
                Matinh = data.Matinh,
                Mahuyen = data.Mahuyen,
                Maxa = data.Maxa,
                Sodienthoai = data.Sodienthoai,
                Email = data.Email,
                Note = data.Note,
                ImgLink = data.ImgLink
            });
        }

        /// <summary>
        /// Get all
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var data = await _gCongtyDAO.GetAll();
            if (data.Count <= 0)
                return NoContent();

            return Ok(data);
        }

        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "tencongty": "GMS",
        ///         "diachi": "57 Huynh Thuc Khang",
        ///         "matinh": 33,
        ///         "mahuyen": 330,
        ///         "maxa": 12250,
        ///         "sodienthoai": "19006746",
        ///         "email": "test@gmail.com",
        ///         "note": "",
        ///         "imgLink": ""
        ///     }
        ///
        /// </remarks>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> InsertGCongty([FromBody] GCongty model)
        {
            if (model == null)
                return BadRequest();

            if (string.IsNullOrEmpty(model.Tencongty))
                return BadRequest("Tên công ty không được để trống!");

            if (!ValidationExtension.IsEmail(model.Email))
                return BadRequest("Email không đúng định dạng!");

            await _gCongtyBusiness.InsertGCongty(model);

            //activity log
            Logger.LogInformation($"GCongtyModel: {StringExtensions.SerializeConvert(model)}");

            return Ok();
        }

        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "tencongty": "GMS",
        ///         "diachi": "57 Huynh Thuc Khang",
        ///         "matinh": 33,
        ///         "mahuyen": 330,
        ///         "maxa": 12250,
        ///         "sodienthoai": "19006746",
        ///         "email": "test@gmail.com",
        ///         "note": "",
        ///         "imgLink": ""
        ///     }
        ///
        /// </remarks>
        [HttpPut("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateGCongty(int id, [FromBody] GCongty model)
        {
            if (model == null)
                return BadRequest();

            if (string.IsNullOrEmpty(model.Tencongty))
                return BadRequest("Tên công ty không được để trống!");

            if (!ValidationExtension.IsEmail(model.Email))
                return BadRequest("Email không đúng định dạng!");

            // get a GCongty with the specified id
            var gCongtyToUpdate = await _gCongtyDAO.Get(a => a.Id == id && a.DeletedAt == 0);
            if (gCongtyToUpdate == null)
                return NoContent();

            await _gCongtyBusiness.UpdateGCongty(gCongtyToUpdate, model);

            // activity log
            Logger.LogInformation($"GCongtyEdit id: {id}," +
                                 $"modelOld: {model.Tencongty}" +
                                 $"modelNew: {model.Tencongty}");
            return Ok();
        }

        /// <summary>
        /// Delete GCongty
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteGCongty(int id)
        {
            if (id <= 0)
                return NoContent();

            // get a gCongty with the specified id
            var data = await _gCongtyDAO.Get(a => a.Id == id && a.DeletedAt == 0);
            if (data == null)
                return NoContent();

            await _gCongtyBusiness.DeleteGCongty(data);

            // activity log
            Logger.LogInformation($"DeleteGCongty id: {id}");

            return Ok();
        }

        #endregion
    }
}