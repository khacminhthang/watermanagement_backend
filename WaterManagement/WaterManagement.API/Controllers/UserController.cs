﻿using WaterManagement.Models.Models;
using WaterManagement.Models.RequestModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace WaterManagement.API.Controllers
{
    [Route("api/user-manage")]
    [ApiController]

    public class UserController : BaseController<UserController>
    {
        private UserManager<User> _userManager;
        private SignInManager<User> _singInManager;
        private RoleManager<IdentityRole> _roleManager;


        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="userManager"></param>
        /// <param name="signInManager"></param>
        public UserController(UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _userManager = userManager;
            _singInManager = signInManager;
        }

        /// <summary>
        /// create new user
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "admin")]
        [Route("create-user")]
        //POST : /api/ApplicationUser/Register
        public async Task<IActionResult> Register(CreateUserRequest model)
        {
            var user = new User()
            {
                UserName = model.UserName,
                Email = model.Email,
                FullName = model.FullName
            };

            try
            {
                var result = await _userManager.CreateAsync(user, model.Password);
                await _userManager.AddToRoleAsync(user, model.Role);
                if (result.Succeeded == true)
                    return Ok(result);

                return BadRequest(result.Errors);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get all user
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var list = await _userManager.Users.ToListAsync();

            return Ok(list);
        }

        /// <summary>
        /// Delete a user
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteUser([FromQuery] string Id)
        {
            var user = await _userManager.FindByIdAsync(Id);

            var rolesForUser = await _userManager.GetRolesAsync(user);

            if (rolesForUser.Count() > 0)
            {
                foreach (var item in rolesForUser.ToList())
                {
                    // item should be the name of the role
                    var result = await _userManager.RemoveFromRoleAsync(user, item);
                }
            }

            try
            {
                var result = await _userManager.DeleteAsync(user);
                if (result.Succeeded == true)
                    return Ok(result);

                return BadRequest(result.Errors);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get By ID
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet("get-by-id")]
        public async Task<IActionResult> GetByID(string id)
        {
            var list = await _userManager.Users.ToListAsync();

            var selectedModel = list.SingleOrDefault(a => a.Id == id);

            if (selectedModel == null)
                return NoContent();

            return Ok(selectedModel);
        }

        /// <summary>
        /// Get By ID
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        //[HttpGet("get-roleid-by-userid")]
        //public async Task<IActionResult> GetRoleIDByUserID(string id)
        //{
        //    var list = await _roleManager.R;

        //    var selectedModel = list.SingleOrDefault(a => a.Id == id);

        //    if (selectedModel == null)
        //        return NoContent();

        //    return Ok(selectedModel);
        //}

        /// <summary>
        /// Update a User
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateUser([FromBody] UpdateUserRequest request)
        {
            var list = await _userManager.Users.ToListAsync();

            var selectedModel = list.SingleOrDefault(a => a.Id == request.Id);
            selectedModel.Id = request.Id;
            selectedModel.UserName = request.UserName;
            selectedModel.Email = request.Email;
            selectedModel.FullName = request.FullName;
            selectedModel.PhoneNumber = request.PhoneNumber;

            if (selectedModel == null)
                return NoContent();

            try
            {
                var result = await _userManager.UpdateAsync(selectedModel);
                if (result.Succeeded == true)
                    return Ok(result);

                return BadRequest(result.Errors);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Get By ID
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost("change-password")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> ChangePass([FromBody] ChangePassWordRequest request)
        {
            var list = await _userManager.Users.ToListAsync();

            var user = list.SingleOrDefault(a => a.Id == request.id);

            user.PasswordHash = _userManager.PasswordHasher.HashPassword(user, request.newpassword);
            var result = await _userManager.UpdateAsync(user);
            if (!result.Succeeded)
            {
                return BadRequest(result.Errors);
            }
            return Ok();

        }

        /// <summary>
        /// Login
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login(LoginModel model)
        {
            var user = await _userManager.FindByNameAsync(model.UserName);
            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
            {
                //Get role assigned to the user
                var role = await _userManager.GetRolesAsync(user);
                IdentityOptions _options = new IdentityOptions();

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim("UserID",user.Id.ToString()),
                        new Claim(_options.ClaimsIdentity.RoleClaimType,role.FirstOrDefault())
                    }),
                    Expires = DateTime.UtcNow.AddDays(1),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes("1234567890123456")), SecurityAlgorithms.HmacSha256Signature)
                };
                var tokenHandler = new JwtSecurityTokenHandler();
                var securityToken = tokenHandler.CreateToken(tokenDescriptor);
                var token = tokenHandler.WriteToken(securityToken);
                return Ok(new { token });
            }
            else
                return BadRequest(new { message = "Username or password is incorrect." });
        }

        /// <summary>
        /// Get own profile
        /// </summary>
        /// <returns></returns>
        [HttpGet("profile")]
        //[Authorize]
        //GET : /api/UserProfile
        public async Task<IActionResult> GetUserProfile()
        {
            string userId = User.Claims.First(c => c.Type == "UserID").Value;
            var user = await _userManager.FindByIdAsync(userId);
            return Ok(new
            {
                user.FullName,
                user.Email,
                user.UserName
            });
        }
    }


}
