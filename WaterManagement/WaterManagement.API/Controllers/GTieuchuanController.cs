﻿using WaterManagement.Business.Business;
using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using WaterManagement.Utils.Common.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace WaterManagement.API.Controllers
{
    /// <summary>
    /// GTieuchuan controller
    /// </summary>
    [Route("api/gtieuchuan")]
    [ApiController]
    public class GTieuchuanController : BaseController<GTieuchuanController>
    {
        #region Fields

        private readonly GTieuchuanBusiness _gTieuchuanBusiness;
        private readonly IGTieuchuanDAO _gTieuchuanDAO;

        #endregion

        #region Ctor

        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="gTieuchuanBusiness"></param>
        /// <param name="gTieuchuanDAO"></param>
        public GTieuchuanController(GTieuchuanBusiness gTieuchuanBusiness,
                                   IGTieuchuanDAO gTieuchuanDAO)
        {
            _gTieuchuanBusiness = gTieuchuanBusiness;
            _gTieuchuanDAO = gTieuchuanDAO;
        }

        #endregion

        #region Method
        /// <summary>
        /// get all
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var data = await _gTieuchuanDAO.GetAll();
            if (data.Count <= 0)
                return NoContent();
            return Ok(data);

        }
        /// <summary>
        /// get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetbyId(int id)
        {
            if (id <= 0)
                return BadRequest();

            // get a gTieuchuan with the specified id
            var data = await _gTieuchuanDAO.GetById(id);
            if (data == null)
                return NoContent();

            return Ok(new
            {
                Id = data.Id,
                Tentieuchuan = data.Tentieuchuan,
                Sohieutieuchuan = data.Sohieutieuchuan,
                Coquanbanhanh = data.Coquanbanhanh,
                Thoigianbanhanh = data.Thoigianbanhanh,
                Hientrang = data.Hientrang,
                IdTailieu = data.IdTailieu,
            });
        }

        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "Tentieuchuan": "Test",
        ///         "Sohieutieuchuan": "3123das",
        ///         "Coquanbanhanh": "Test",
        ///         "Thoigianbanhanh": "31/10/2019",
        ///         "Hientrang": "test",
        ///         "IdTailieu": "113"
        ///     }
        ///
        /// </remarks>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> InsertGTieuchuan([FromBody] GTieuchuan model)
        {
            if (model == null)
                return BadRequest();
            if (string.IsNullOrEmpty(model.Tentieuchuan))
                return BadRequest("Tên tiêu chuẩn không được để trống!");

            await _gTieuchuanBusiness.InsertGTieuchuan(model);

            // activity log
            Logger.LogInformation($"GTieuchuanInsert model: {StringExtensions.SerializeConvert(model)}");

            return Ok();
        }

        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "Tentieuchuan": "Test",
        ///         "Sohieutieuchuan": "3123das",
        ///         "Coquanbanhanh": "Test",
        ///         "Thoigianbanhanh": "31/10/2019",
        ///         "Hientrang": "test",
        ///         "IdTailieu": "113"
        ///     }
        ///
        /// </remarks>
        [HttpPut("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateGTieuchuan(int id, [FromBody] GTieuchuan gtieuchuan)
        {
            if (gtieuchuan == null)
                return BadRequest();
            if (string.IsNullOrEmpty(gtieuchuan.Tentieuchuan))
                return BadRequest("Tên tiêu chuẩn không được để trống!");
            // get a gTieuchuan with the specified id
            var gPhantichToUpdate = await _gTieuchuanDAO.GetById(id);
            if (gPhantichToUpdate == null)
                return NoContent();

            await _gTieuchuanBusiness.UpdateGTieuchuan(gPhantichToUpdate, gtieuchuan);

            // activity log
            Logger.LogInformation($"GTieuchuanEdit id: {id}," +
                                   $"model: {StringExtensions.SerializeConvert(gtieuchuan)}");
            return Ok();
        }

        /// <summary>
        /// delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteGTieuchuan(int id)
        {
            if (id <= 0)
                return BadRequest();

            // get a gTieuchuan with the specified id
            var data = await _gTieuchuanDAO.GetById(id);
            if (data == null)
                return NoContent();

            await _gTieuchuanBusiness.DeleteGTieuchuan(data);

            //activity log
            Logger.LogInformation($"GTieuchuanDelete id: {id}");

            return Ok();
        }

        #endregion
    }
}

