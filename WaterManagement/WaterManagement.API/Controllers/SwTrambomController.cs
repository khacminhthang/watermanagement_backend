﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WaterManagement.Business.Business;
using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;

namespace WaterManagement.API.Controllers
{
    [Route("api/swTrambom")]
    [ApiController]
    public class SwTrambomController : BaseController<SwTrambomController>
    {
        #region Fields
        private readonly SwTrambomBusiness _swTrambomBusiness;
        private readonly ISwTrambomDAO _swTrambomDAO;
        #endregion

        #region ctor
        public SwTrambomController(SwTrambomBusiness swTrambomBusiness, ISwTrambomDAO swTrambomDAO)
        {
            _swTrambomBusiness = swTrambomBusiness;
            _swTrambomDAO = swTrambomDAO;
        }
        #endregion

        #region Method

        //[HttpGet]
        //public async Task<IActionResult> GetAll()
        //{
        //    var data = await _swTrambomDAO.GetAll();
        //    if (data.Count <= 0)
        //        return NoContent();
        //    return Ok(data);
        //}

        [HttpGet]
        public async Task<IActionResult> GetAllPaging([FromQuery] int pageNumber, int pageSize)
        {
            if (pageNumber <= 0)
                return BadRequest("Số thứ tự trang phải lớn hơn hoặc bằng 1!");

            if (pageSize == -1)
            {
                pageSize = int.MaxValue - 1;
            }
            var query = await _swTrambomBusiness.GetAll(pageNumber, pageSize);
            if (query == null)
                return NoContent();

            var data = new
            {
                Paging = query.GetHeader(),
                Items = query.List
            };
            //
            return Ok(data);
        }
        /// <summary>
        /// get by it
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetById(int id)
        {
            if (id <= 0)
                return BadRequest();
            var data = await _swTrambomDAO.GetById(id);
            if (data == null)
                return NoContent();
            return Ok(new SwTrambomViewModel
            {
            Sohieudiem = data.Sohieutrambom,
            Tendiem = data.Tentrambom,
            Toadox = data.Toadox,
            Toadoy = data.Toadoy,
            Caodoz = data.Caodoz,
            Srid = data.Srid,
            Vitri = data.Vitri,
            Matinh = data.Matinh,
            Mahuyen = data.Mahuyen,
            Maxa = data.Maxa,
            Thoigianxaydung = data.Thoigianxaydung,
            Thoigianvanhanh = data.Thoigianvanhanh,
            Somaybomtieunuoc = data.Somaybomtieunuoc,
            Somaybomtuoinuoc = data.Somaybomtuoinuoc,
            Khanangbomtuoi = data.Khanangbomtuoi,
            Luuluongtuoithietke = data.Luuluongtuoithietke,
            Luuluongtuoithucte = data.Luuluongtuoithucte,
            Dientichtuoithietke = data.Dientichtuoithietke,
            Dientichtuoithucte = data.Dientichtuoithucte,
            Khanangbomtieu = data.Khanangbomtieu,
            Luuluongtieuthietke = data.Luuluongtieuthietke,
            Luuluongtieuthucte = data.Luuluongtieuthucte,
            Dientichtieuthietke = data.Dientichtieuthietke,
            Dientichtieuthucte = data.Dientichtieuthucte,
            IdVanhanh = data.IdVanhanh,
            Sohothietke = data.Sohothietke,
            Sohothucte = data.Sohothucte,
            Phantramcongsuathuuich = data.Phantramcongsuathuuich,
            Hientrang = data.Hientrang,
            Note = data.Note,
            ImgLink = data.ImgLink
            });
        }

        /// <summary>
        /// insert
        /// </summary>
        /// <param name="swTrambom"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> InsertTrambom([FromBody]SwTrambom swTrambom)
        {
            if (swTrambom == null)
                return BadRequest();
            await _swTrambomBusiness.InsertTrambom(swTrambom);
            return Ok();
        }

        /// <summary>
        /// update
        /// </summary>
        /// <param name="id"></param>
        /// <param name="swTrambom"></param>
        /// <returns></returns>
        [HttpPut("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateTrambom(int id,[FromBody]SwTrambom swTrambom)
        {
            if (swTrambom == null)
                return BadRequest();
            var swTrambomToUpdate = _swTrambomDAO.GetById(id).Result;
            if (swTrambomToUpdate == null)
                return NoContent();
            await _swTrambomBusiness.UpdateTrambom(swTrambomToUpdate, swTrambom);
            return Ok();
        }

        /// <summary>
        /// delete
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteTrambom(int id)
        {
            if (id <= 0)
                return BadRequest();
            var data = _swTrambomDAO.GetById(id).Result;
            if (data == null)
                return NoContent();
            await _swTrambomBusiness.DeleteTrambom(data);
            return Ok();
        }
        #endregion
    }
}
