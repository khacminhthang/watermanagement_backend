﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WaterManagement.Business.Business;
using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;

namespace WaterManagement.API.Controllers
{
    [Route("api/swMaybomtrambom")]
    [ApiController]
    public class SwMaybomtrambomController : BaseController<SwMaybomtrambomController>
    {
        #region Fields
        private readonly SwMaybomtrambomBusiness _swMaybomtrambomBusiness;
        private readonly ISwMaybomtrambomDAO _swMaybomtrambomDAO;
        #endregion

        #region ctor
        public SwMaybomtrambomController(SwMaybomtrambomBusiness swMaybomtrambomBusiness, ISwMaybomtrambomDAO swMaybomtrambomDAO)
        {
            _swMaybomtrambomBusiness = swMaybomtrambomBusiness;
            _swMaybomtrambomDAO = swMaybomtrambomDAO;
        }
        #endregion

        #region Method

        [HttpGet("get-list/{idTrambom:int}")]
        public async Task<IActionResult> GetAll(int idTrambom)
        {
            var data = await _swMaybomtrambomDAO.GetAllById(idTrambom);
            if (data.Count <= 0)
                return NoContent();
            return Ok(data);
        }


        /// <summary>
        /// get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetById(int id)
        {
            if (id <= 0)
                return BadRequest();
            var data = await _swMaybomtrambomDAO.GetById(id);
            if (data == null)
                return NoContent();
            return Ok(data);
        }

        /// <summary>
        /// insert
        /// </summary>
        /// <param name="swTrambom"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> InsertMaybomtrambom([FromBody]SwMaybomtrambom model)
        {
            if (model == null)
                return BadRequest();
            await _swMaybomtrambomBusiness.InsertMaybomtrambom(model);
            return Ok();
        }

        /// <summary>
        /// update
        /// </summary>
        /// <param name="id"></param>
        /// <param name="swTrambom"></param>
        /// <returns></returns>
        [HttpPut("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateMaybomtrambom(int id, [FromBody]SwMaybomtrambom model)
        {
            if (model == null)
                return BadRequest();
            var data = _swMaybomtrambomDAO.GetById(id).Result;
            if (data == null)
                return NoContent();
            await _swMaybomtrambomBusiness.UpdateMaybomtrambom(data, model);
            return Ok();
        }

        /// <summary>
        /// delete
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteMaybomtrambom(int id)
        {
            if (id <= 0)
                return BadRequest();
            var data = _swMaybomtrambomDAO.GetById(id).Result;
            if (data == null)
                return NoContent();
            await _swMaybomtrambomBusiness.DeleteMaybomtrambom(data);
            return Ok();
        }
        #endregion
    }
}
