﻿using WaterManagement.Models.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WaterManagement.API.Controllers
{
    [Route("api/role")]
    [ApiController]

    public class RoleController : BaseController<RoleController>
    {
        private UserManager<User> _userManager;
        private RoleManager<IdentityRole> _roleManager;
        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="userManager"></param>
        /// <param name="roleManager"></param>
        public RoleController(UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        /// <summary>
        /// CreateUserRoles
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> CreateUserRoles([FromQuery] string role)
        {

            IdentityResult roleResult;
            //Adding Admin Role
            var roleCheck = await _roleManager.RoleExistsAsync(role);
            if (!roleCheck)
            {
                //create the roles and seed them to the database
                roleResult = await _roleManager.CreateAsync(new IdentityRole(role));
                return Ok(roleResult);
            }

            return BadRequest();
        }

        /// <summary>
        /// GetAllRole
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAllRole()
        {

            var list = await _roleManager.Roles.ToListAsync();

            return Ok(list);
        }

        /// <summary>
        /// DeleteRole
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteRole([FromQuery] string Id)
        {
            var list = await _roleManager.Roles.ToListAsync();

            var toDelete = list.SingleOrDefault(a => a.Id == Id);

            try
            {
                var result = await _roleManager.DeleteAsync(toDelete);
                if (result.Succeeded == true)
                    return Ok(result);

                return BadRequest(result.Errors);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Get By ID
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet("get-by-id")]
        public async Task<IActionResult> GetByID(string id)
        {
            var list = await _roleManager.Roles.ToListAsync();

            var selectedModel = list.SingleOrDefault(a => a.Id == id);

            if (selectedModel == null)
                return NoContent();

            return Ok(selectedModel);
        }

        /// <summary>
        /// SetAdmin
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpPost("set-admin")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> SetAdmin([FromQuery] string Id)
        {
            var user = await _userManager.FindByIdAsync(Id);
            var res = await _userManager.AddToRoleAsync(user, "admin");
            return Ok(res);
        }

        /// <summary>
        /// Change role
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        [HttpPost("change-role")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> ChangeRole([FromQuery] string Id, string role)
        {
            // get specify user
            var user = await _userManager.FindByIdAsync(Id);

            if (user == null)
                return NoContent();
            // remove old role
            var list = await _roleManager.Roles.ToListAsync();

            foreach (var item in list)
            {
                await _userManager.RemoveFromRoleAsync(user, item.Name);
            }
            // gán role mới cho user
            var res = await _userManager.AddToRoleAsync(user, role);

            if (res.Succeeded == true)
                return Ok();

            return BadRequest(res.Errors);
        }

        /// <summary>
        /// GetAllRole
        /// </summary>
        /// <returns></returns>
        [HttpGet("user-role")]
        public async Task<IActionResult> GetUserRole([FromQuery] string userID)
        {
            var user = await _userManager.FindByIdAsync(userID);
            var rolename = await _userManager.GetRolesAsync(user);
            return Ok(rolename.FirstOrDefault());
        }
    }
}
