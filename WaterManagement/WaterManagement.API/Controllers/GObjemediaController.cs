﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using WaterManagement.Models.RequestModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace WaterManagement.API.Controllers
{

    [Route("api/objemedia")]
    [ApiController]
    public class GObjemediaController : BaseController<GObjemediaController>
    {
        #region Fields

        private readonly IGObjmediaDAO _gObjmediaDAO;
        private readonly IEfRepository<GObjmedia> _efRepository;
        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="gObjmediaDAO"></param>
        public GObjemediaController(IGObjmediaDAO gObjmediaDAO, IEfRepository<GObjmedia> efRepository, IUnitOfWork unitOfWork)
        {
            _gObjmediaDAO = gObjmediaDAO;
            _efRepository = efRepository;
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region Method

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="idObj"></param>
        /// <param name="objType"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> InsertImage([FromBody] CreateGObjMedia request)
        {
            if (request == null)
                return BadRequest();
            if (request.IdObj <= 0)
                return BadRequest();

            //
            await _gObjmediaDAO.Insert(request);

            return Ok();
        }

        /// <summary>
        /// Get all
        /// </summary>
        /// <param name="IdObj"></param>
        /// <param name="ObjKey"></param>
        /// <param name="IdObjmediaType"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] int IdObj, short? ObjKey, int? IdObjmediaType)
        {
            if (IdObj == 0)
                return BadRequest();

            //
            var data = await _gObjmediaDAO.Get(IdObj, ObjKey, IdObjmediaType);

            if (data == null)
                return NoContent();
            return Ok(data);
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id == 0)
                return BadRequest();

            //
            var toDelete = await _efRepository.SingleOrDefaultAsync(a => a.Id == id && a.DeletedAt == 0);

            if (toDelete == null)
                return NoContent();
            toDelete.DeletedAt = 1;
            toDelete.UpdatedAt = DateTime.Now.ToString();

            _efRepository.Update(toDelete);
            await _unitOfWork.SaveChangeAsync();

            return Ok();
        }
        #endregion
    }
}