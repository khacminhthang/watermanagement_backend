﻿using WaterManagement.Business.Business;
using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using WaterManagement.Utils.Common.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace WaterManagement.API.Controllers
{
    /// <summary>
    /// gTieuchuanchatluong controller
    /// </summary>
    [Route("api/gtieuchuanchatluong")]
    [ApiController]
    public class GTieuchuanchatluongController : BaseController<GTieuchuanchatluongController>
    {
        #region Fields

        private readonly GTieuchuanchatluongBusiness _gTieuchuanchatluongBusiness;
        private readonly IGTieuchuanchatluongDAO _gTieuchuanchatluongDAO;

        #endregion

        #region Ctor

        public GTieuchuanchatluongController(GTieuchuanchatluongBusiness gTieuchuanchatluongBusiness,
                                   IGTieuchuanchatluongDAO gTieuchuanchatluongDAO)
        {
            _gTieuchuanchatluongBusiness = gTieuchuanchatluongBusiness;
            _gTieuchuanchatluongDAO = gTieuchuanchatluongDAO;
        }

        #endregion

        #region Method
        /// <summary>
        /// get all
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var data = await _gTieuchuanchatluongDAO.GetAll();
            if (data.Count <= 0)
                return NoContent();
            return Ok(data);

        }
        /// <summary>
        /// get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetbyId(int id)
        {
            if (id <= 0)
                return BadRequest();

            // get a gTieuchuanchatluong with the specified id
            var data = await _gTieuchuanchatluongDAO.GetById(id);
            if (data == null)
                return NoContent();

            return Ok(new
            {
                Id = data.Id,
                IdTieuchuan = data.IdTieuchuan,
                IdThamso = data.IdThamso,
                IdDonvido = data.IdDonvido,
                Gioihantren = data.Gioihantren,
                Gioihanduoi = data.Gioihanduoi,
                Phuongphapthu = data.Phuongphapthu,
                Captieuchuan = data.Captieuchuan,
                Mucdogiamsat = data.Mucdogiamsat
            });
        }
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "idTieuchuan": 1,
        ///         "idThamso": 1,
        ///         "idDonvido": 1,
        ///         "gioihantren": 10,,
        ///         "gioihanduoi": 10,
        ///         "phuongphapthu": "baba",
        ///         "captieuchuan": "cc",
        ///         "mucdogiamsat": "tesst"
        ///         
        ///     }
        ///
        /// </remarks>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> InsertGTieuchuanchatluong([FromBody] GTieuchuanchatluong gTieuchuanchatluong)
        {
            if (gTieuchuanchatluong == null)
                return BadRequest();

            await _gTieuchuanchatluongBusiness.InsertGTieuchuanchatluong(gTieuchuanchatluong);

            // activity log
            Logger.LogInformation($"GTieuchuanchatluong model: {StringExtensions.SerializeConvert(gTieuchuanchatluong)}");

            return Ok();
        }
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "idTieuchuan": 1,
        ///         "idThamso": 1,
        ///         "idDonvido": 1,
        ///         "gioihantren": 10,,
        ///         "gioihanduoi": 10,
        ///         "phuongphapthu": "baba",
        ///         "captieuchuan": "cc",
        ///         "mucdogiamsat": "tesst"
        ///         
        ///     }
        ///
        /// </remarks>
        [HttpPut("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateGTieuchuanchatluong(int id, [FromBody]  GTieuchuanchatluong gTieuchuanchatluong)
        {
            if (gTieuchuanchatluong == null)
                return BadRequest();

            // get a gTieuchuanchatluong with the specified id
            var gTieuchuanchatluongToUpdate = await _gTieuchuanchatluongDAO.GetById(id);
            if (gTieuchuanchatluongToUpdate == null)
                return NoContent();

            await _gTieuchuanchatluongBusiness.UpdateGTieuchuanchatluong(gTieuchuanchatluongToUpdate, gTieuchuanchatluong);

            // activity log
            Logger.LogInformation($"GTieuchuanchatluongEdit id: {id}," +
                                   $"model: {StringExtensions.SerializeConvert(gTieuchuanchatluong)}");
            return Ok();
        }
        /// <summary>
        /// delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteGTieuchuanchatluong(int id)
        {
            if (id <= 0)
                return BadRequest();

            // get a gTieuchuanchatluong with the specified id
            var data = await _gTieuchuanchatluongDAO.GetById(id);
            if (data == null)
                return NoContent();

            await _gTieuchuanchatluongBusiness.DeleteGTieuchuanchatluong(data);

            //activity log
            Logger.LogInformation($"GTieuchuanchatluongDelete id: {id}");

            return Ok();
        }

        #endregion
    }
}
