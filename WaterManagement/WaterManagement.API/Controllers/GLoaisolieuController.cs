﻿using WaterManagement.Business.Business;
using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using WaterManagement.Utils.Common.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace WaterManagement.API.Controllers
{
    [Route("api/gloaisolieu")]
    [ApiController]
    public class GLoaisolieuController : BaseController<GLoaisolieuController>
    {
        #region Fields
        private readonly IGLoaisolieuDAO _gLoaisolieuDAO;
        private readonly GLoaisolieuBusiness _gLoaisolieuBusiness;

        #endregion

        #region Ctor
        public GLoaisolieuController(IGLoaisolieuDAO gLoaisolieuDAO,
                                     GLoaisolieuBusiness gLoaisolieuBusiness)
        {
            _gLoaisolieuDAO = gLoaisolieuDAO;
            _gLoaisolieuBusiness = gLoaisolieuBusiness;
        }
        #endregion

        #region Method
        /// <summary>
        /// get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetById(int id)
        {
            if (id <= 0)
                return BadRequest();

            // get a GLoaisolieu with a specified id
            var data = await _gLoaisolieuDAO.GetById(id);

            return Ok(data);
        }
        /// <summary>
        /// get all
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var data = await _gLoaisolieuDAO.GetAll();
            if (data.Count <= 0)
                return NoContent();

            return Ok(data);
        }
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "tenloaisolieu": "Tên là abc",
        ///         "tenviettat": "abc",
        ///     }
        ///
        /// </remarks>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> InsertGLoaisolieu([FromBody] GLoaisolieu gLoaisolieu)
        {
            if (gLoaisolieu == null)
                return BadRequest();
            if (string.IsNullOrEmpty(gLoaisolieu.Tenloaisolieu))
                return BadRequest("Tên loại số liệu không được để trống!");

            await _gLoaisolieuBusiness.InsertGLoaisolieu(gLoaisolieu);
            //activity log
            Logger.LogInformation($"GLoaisolieuModel: {StringExtensions.SerializeConvert(gLoaisolieu)}");
            return Ok();
        }
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "tenloaisolieu": "Tên là abc",
        ///         "tenviettat": "abc",
        ///     }
        ///
        /// </remarks>
        [HttpPut("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateGLoaisolieu(int id, [FromBody] GLoaisolieu model)
        {
            if (id <= 0)
                return BadRequest();

            if (string.IsNullOrEmpty(model.Tenloaisolieu))
                return BadRequest("Tên loại số liệu không được để trống!");

            //get a GLoaisolieu with a specified id
            var GLoaisolieuToUpdate = await _gLoaisolieuDAO.GetById(id);

            if (GLoaisolieuToUpdate == null)
                return NoContent();

            await _gLoaisolieuBusiness.UpdateGLoaisolieu(GLoaisolieuToUpdate, model);

            // activity log
            Logger.LogInformation($"GLoaisolieuEdit id: {id}," +
                                 $"modelOld: {model.Tenloaisolieu}" +
                                 $"modelNew: {model.Tenviettat}");
            return Ok();

        }
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteGLoaisolieu(int id)
        {
            if (id <= 0)
                return BadRequest();

            //get a GLoaisolieu with a specified id
            var data = await _gLoaisolieuDAO.GetById(id);

            if (data == null)
                return NoContent();

            await _gLoaisolieuBusiness.DeleteGLoaisolieu(data);
            // activity log
            Logger.LogInformation($"DeleteGLoaisolieu id: {id}");

            return Ok();

        }


        #endregion
    }
}
