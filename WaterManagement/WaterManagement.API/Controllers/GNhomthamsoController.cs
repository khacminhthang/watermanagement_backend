﻿using WaterManagement.Business.Business;
using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using WaterManagement.Utils.Common.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace WaterManagement.API.Controllers
{
    /// <summary>
    /// Controller
    /// </summary>
    [Route("api/gNhomthamso")]
    [ApiController]
    public class GNhomthamsoController : BaseController<GNhomthamsoController>
    {
        #region Fields

        private readonly IGNhomthamsoDAO _gNhomthamsoDAO;
        private readonly GNhomthamsoBusiness _gNhomthamsoBusiness;

        #endregion

        #region Ctor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="gNhomthamsoDAO"></param>
        /// <param name="gNhomthamsoBusiness"></param>
        public GNhomthamsoController(IGNhomthamsoDAO gNhomthamsoDAO,
                                     GNhomthamsoBusiness gNhomthamsoBusiness)
        {
            _gNhomthamsoDAO = gNhomthamsoDAO;
            _gNhomthamsoBusiness = gNhomthamsoBusiness;
        }

        #endregion

        #region Method

        /// <summary>
        ///  Get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetbyId(int id)
        {
            if (id <= 0)
                return BadRequest();

            // get a GNhomthamso with the specified id
            var data = await _gNhomthamsoDAO.Get(a => a.Id == id);
            if (data == null)
                return NoContent();

            return Ok(new
            {
                id = data.Id,
                tennhom = data.Tennhom,
                kyhieunhom = data.Kyhieunhom
            });
        }

        /// <summary>
        /// get all
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var data = await _gNhomthamsoDAO.GetList(a => true);
            if (data.Count <= 0)
                return NoContent();

            return Ok(data.Select(a => new
            {
                id = a.Id,
                tennhom = a.Tennhom,
                kyhieunhom = a.Kyhieunhom
            }).OrderByDescending(o => o.id)
              .ToList());
        }

        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "tennhom": "test",
        ///         "kyhieunhom": "ABC"
        ///     }
        ///
        /// </remarks>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> InsertGNhomthamso([FromBody] GNhomthamso model)
        {
            if (model == null)
                return BadRequest();

            await _gNhomthamsoBusiness.InsertGNhomthamso(model);

            // activity log
            Logger.LogInformation($"GNhomthamsoModel: {StringExtensions.SerializeConvert(model)}");

            return Ok();
        }

        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "tennhom": "test",
        ///         "kyhieunhom": "ABC"
        ///     }
        ///
        /// </remarks>
        [HttpPut("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateGNhomthamso(int id, [FromBody] GNhomthamso model)
        {
            if (model == null)
                return BadRequest();

            // get a GNhomthamso with the specified id
            var gNhomthamsoToUpdate = await _gNhomthamsoDAO.Get(a => a.Id == id);
            if (gNhomthamsoToUpdate == null)
                return NoContent();

            await _gNhomthamsoBusiness.UpdateGNhomthamso(gNhomthamsoToUpdate, model);

            // activity log
            Logger.LogInformation($"GNhomthamsoEdit id: {id}," +
                                  $"modelOld: {model.Tennhom}, {model.Kyhieunhom}" +
                                  $"modelNew: {model.Tennhom}, {model.Kyhieunhom}");

            return Ok();

        }

        /// <summary>
        /// Delete GNhomthamso
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteGNhomthamso(int id)
        {
            if (id <= 0)
                return BadRequest();

            // get a GNhomthamso with the specified id
            var model = await _gNhomthamsoDAO.Get(a => a.Id == id);
            if (model == null)
                return NoContent();

            await _gNhomthamsoBusiness.DeleteGNhomthamso(model);

            // activity log
            Logger.LogInformation($"DeleteGNhomthamso: {id}");

            return Ok();
        }

        /// <summary>
        /// Check exist tên nhóm, ký hiệu nhóm
        /// </summary>
        /// <param name="id"></param>
        /// <param name="tennhom"></param>
        /// <param name="kyhieunhom"></param>
        /// <returns></returns>
        [HttpGet("check-exist")]
        public async Task<IActionResult> CheckExist([FromQuery]int? id, string tennhom, string kyhieunhom)
        {
            bool checkExistName = await _gNhomthamsoBusiness.CheckExistName(id, tennhom);
            if (checkExistName)
                return StatusCode(400, new { message = "Tên nhóm đã tồn tại!" });

            bool checkExistSymbol = await _gNhomthamsoBusiness.CheckExistSymbol(id, kyhieunhom);
            if (checkExistSymbol)
                return StatusCode(400, new { message = "Ký hiệu nhóm đã tồn tại!" });

            return Ok(new { message = "ok" });
        }

        #endregion
    }
}