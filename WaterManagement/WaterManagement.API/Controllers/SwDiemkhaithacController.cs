﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WaterManagement.Business.Business;
using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;

namespace WaterManagement.API.Controllers
{
    /// <summary>
    /// SwDiemkhaithac controller
    /// </summary>
    [Route("api/swDiemkhaithac")]
    [ApiController]
    public class SwDiemkhaithacController : BaseController<SwDiemkhaithacController>
    {
        #region Fiedls

        private readonly ISwDiemkhaithacDAO _swDiemkhaithacDAO;
        private readonly SwDiemkhaithacBusiness _swDiemkhaithacBusiness;
        #endregion

        #region Ctor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="swDiemkhaithacDAO"></param>
        /// <param name="swDiemkhaithacBusiness"></param>
        public SwDiemkhaithacController(ISwDiemkhaithacDAO swDiemkhaithacDAO,
                                        SwDiemkhaithacBusiness swDiemkhaithacBusiness)
        {
            _swDiemkhaithacDAO = swDiemkhaithacDAO;
            _swDiemkhaithacBusiness = swDiemkhaithacBusiness;
        }

        #endregion

        #region Method

        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetbyId(int id)
        {
            if (id <= 0)
                return BadRequest();

            // get a SwDiemkhaithac with the specified id
            var data = await _swDiemkhaithacDAO.GetbyId(id);
            if (data == null)
                return NoContent();

            return Ok(new
            {
                Id = data.Id,
                Sohieudiem = data.Sohieudiem,
                Tendiem = data.Tendiem,
                Toadox = data.Toadox,
                Toadoy = data.Toadoy,
                Caodoz = data.Caodoz,
                Srid = data.Srid,
                Vitri = data.Vitri,
                Matinh = data.Matinh,
                Mahuyen = data.Mahuyen,
                Maxa = data.Maxa,
                Thoigianxaydung = data.Thoigianxaydung,
                Thoigianvanhanh = data.Thoigianvanhanh,
                IdDonviquanly = data.IdDonviquanly,
                IdChusudung = data.IdChusudung,
                IdMucdichsudungchinh = data.IdMucdichsudungchinh,
                Luuluongchopheplonnhat = data.Luuluongchopheplonnhat,
                Cothietbigiamsat = data.Cothietbigiamsat,
                IdVanhanh = data.IdVanhanh,
                Thoiluongkhaithac = data.Thoiluongkhaithac,
                Nhamay = data.Nhamay,
                Dacapphep = data.Dacapphep,
                Note = data.Note,
                ImgLink = data.ImgLink
            });
        }

        /// <summary>
        /// Get all
        /// </summary>
        /// <param name="pageNumber">pageNumber</param>
        /// <param name="pageSize">pageSize</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAllPaging([FromQuery] int pageNumber, int pageSize)
        {
            if (pageNumber <= 0)
                return BadRequest("Số thứ tự trang phải lớn hơn hoặc bằng 1!");

            if (pageSize == -1)
            {
                pageSize = int.MaxValue - 1;
            }

            var query = await _swDiemkhaithacBusiness.GetAll(pageNumber, pageSize);
            if (query == null)
                return NoContent();

            var data = new
            {
                Paging = query.GetHeader(),
                Items = query.List
            };
            //
            return Ok(data);
        }


        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "sohieudiem": "ABC123",
        ///         "tendiem": "điểm khai thác test",
        ///         "toadox": -71.1043443253471,
        ///         "toadoy": 42.3150676015829,
        ///         "caodoz": 0,
        ///         "srid": 3405,
        ///         "vitri": "cạnh núi",
        ///         "matinh": 1,
        ///         "mahuyen": 2,
        ///         "maxa": 3,
        ///         "thoigianxaydung": "10/10/2018",
        ///         "thoigianvanhanh": "10/10/2020",
        ///         "idDonviquanly": 1,
        ///         "idChusudung": 1,
        ///         "idMucdichsudungchinh": 1,
        ///         "luuluongchopheplonnhat": 1000,
        ///         "cothietbigiamsat": true,
        ///         "idVanhanh": 1,
        ///         "thoiluongkhaithac": "",
        ///         "nhamay": "",
        ///         "idSong": 1,
        ///         "hochua": "",
        ///         "hotunhien": "",
        ///         "dacapphep": true,
        ///         "note": "Test thôi mà",
        ///         "imgLink": "",
        ///     }
        ///
        /// </remarks>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> InsertSwDiemkhaithac([FromBody] SwDiemkhaithac model)
        {
            if (model == null)
                return BadRequest();

            if (string.IsNullOrEmpty(model.Tendiem))
                return BadRequest("Bạn phải nhập tên điểm khai thác!");

            if (model.Toadox == null)
                return BadRequest("Bạn phải nhập tọa độ x!");

            if (model.Toadoy == null)
                return BadRequest("Bạn phải nhập tọa độ y!");

            if (model.Srid == null || model.Srid <= 0)
                return BadRequest("Bạn phải nhập SRID!");

            await _swDiemkhaithacBusiness.InsertSwDiemkhaithac(model);

            // activity log
            Logger.LogInformation($"SwDiemkhaithacInsert model: {model.Sohieudiem},{model.Tendiem}, {model.Toadox}, {model.Toadoy}");

            return Ok();
        }

        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "Sohieudiem": "ABC123",
        ///         "Tendiem": "Đập test",
        ///         "toadox": -71.1043443253471,
        ///         "toadoy": 42.3150676015829,
        ///         "caodoz": 0,
        ///         "srid": 3405,
        ///         "vitri": "cạnh núi",
        ///         "matinh": 1,
        ///         "mahuyen": 2,
        ///         "maxa": 3,
        ///         "Thoigianxaydung": "10/10/2018",
        ///         "thoigianvanhanh": "10/10/2020",
        ///         "IdDonviquanly": 1,
        ///         "IdChusudung": 1,
        ///         "IdMucdichsudungchinh": 1,
        ///         "Luuluongchopheplonnhat": 1000,
        ///         "Cothietbigiamsat": true,
        ///         "IdVanhanh": 1,
        ///         "Thoiluongkhaithac": "",
        ///         "nhamay": "",
        ///         "IdSong": 1,
        ///         "hochua": "",
        ///         "hotunhien": "",
        ///         "Dacapphep": true,
        ///         "note": "Test thôi mà",
        ///         "ImgLink": "",
        ///     }
        ///
        /// </remarks>
        [HttpPut("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateSwDiemkhaithac(int id, [FromBody] SwDiemkhaithac model)
        {
            if (model == null)
                return BadRequest();

            if (string.IsNullOrEmpty(model.Tendiem))
                return BadRequest("Bạn phải nhập tên điểm khai thác!");

            if (model.Toadox == null)
                return BadRequest("Bạn phải nhập tọa độ x!");

            if (model.Toadoy == null)
                return BadRequest("Bạn phải nhập tọa độ y!");

            if (model.Srid == null || model.Srid <= 0)
                return BadRequest("Bạn phải nhập SRID!");

            //get a SwDiemkhaithac with the specified id
            var modelToUpdate = await _swDiemkhaithacDAO.GetbyId(id);
            if (modelToUpdate == null)
                return NoContent();

            await _swDiemkhaithacBusiness.UpdateSwDiemkhaithac(modelToUpdate, model);

            // activity log
            Logger.LogInformation($"SwDiemkhaithacEdit id: {id}," +
                                 $"modelOld: {modelToUpdate.Sohieudiem}, {modelToUpdate.Tendiem}, {modelToUpdate.Toadox}, {modelToUpdate.Toadoy}," +
                                 $"modelNew: {model.Sohieudiem}, {model.Tendiem}, {model.Toadox}, {model.Toadoy}");

            return Ok();
        }

        /// <summary>
        /// Delete SwDiemkhaithac
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteSwDiemkhaithac(int id)
        {
            if (id <= 0)
                return BadRequest();

            // get a SwDiemkhaithac with the specified id
            var data = await _swDiemkhaithacDAO.GetbyId(id);
            if (data == null)
                return NoContent();

            await _swDiemkhaithacBusiness.DeleteSwDiemkhaithac(data);

            // activity log
            Logger.LogInformation($"SwDiemkhaithacDelete id: {id}");

            return Ok();
        }

        #endregion
    }
}