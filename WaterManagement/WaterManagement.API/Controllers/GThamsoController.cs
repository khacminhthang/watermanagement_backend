﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using WaterManagement.Utils.Common.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;
using WaterManagement.Business.Business;

namespace WaterManagement.API.Controllers
{
    [Route("api/gthamso")]
    [ApiController]
    public class GThamsoController : BaseController<GThamsoController>
    {
        #region Fields

        private readonly IGThamsoDAO _gThamsoDAO;
        private readonly GThamsoBusiness _gThamsoBusiness;
        private readonly IEfRepository<GThamso> _efRepository;
        #endregion

        #region Ctor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gThamsoDAO"></param>
        /// <param name="gThamsoBusiness"></param>
        public GThamsoController(IGThamsoDAO gThamsoDAO,
                                  GThamsoBusiness gThamsoBusiness, IEfRepository<GThamso> efRepository)
        {
            _gThamsoDAO = gThamsoDAO;
            _gThamsoBusiness = gThamsoBusiness;
            _efRepository = efRepository;
        }

        #endregion
        #region Method

        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetById(int id)
        {
            if (id <= 0)
                return BadRequest();

            // get a GCongty the specified id
            var data = await _gThamsoDAO.GetById(id);
            if (data == null)
                return NoContent();

            return Ok(new
            {
                Id = data.Id,
                Tenthamso = data.Tenthamso,
                Kyhieuthamso = data.Kyhieuthamso,
                IdNhomthamso = data.IdNhomthamso,
                IdDonvidomacdinh = data.IdDonvidomacdinh,

            });
        }
        /// <summary>
        /// get all
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var data = await _gThamsoDAO.GetAll();
            if (data.Count <= 0)
                return NoContent();
            return Ok(data);
        }
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "Tenthamso": "Test",
        ///         "Sohieutieuchuan": "3123das",
        ///         "Kyhieuthamso": "Test",
        ///         "IdNhomthamso": "123",
        ///         "IdDonvidomacdinh": "113"
        ///         
        ///     }
        ///
        /// </remarks>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Insert([FromBody] GThamso model)
        {
            if (model == null)
                return BadRequest();
            if (string.IsNullOrEmpty(model.Tenthamso))
                return BadRequest("Tên tham số không được để trống!");

            await _gThamsoBusiness.Insert(model);

            // activity log
            Logger.LogInformation($"GThamso model: {StringExtensions.SerializeConvert(model)}");

            return Ok();
        }
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "Tenthamso": "Test",
        ///         "Sohieutieuchuan": "3123das",
        ///         "Kyhieuthamso": "Test",
        ///         "IdNhomthamso": "123",
        ///         "IdDonvidomacdinh": "113"
        ///         
        ///     }
        ///
        /// </remarks>
        [HttpPut("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Update(int id, [FromBody] GThamso gThamso)
        {
            if (gThamso == null)
                return BadRequest();
            if (string.IsNullOrEmpty(gThamso.Tenthamso))
                return BadRequest("Tên tham số không được để trống!");
            // get a gPhantich with the specified id
            var gThamsoToUpdate = await _gThamsoDAO.GetById(id);
            if (gThamsoToUpdate == null)
                return NoContent();

            await _gThamsoBusiness.Update(gThamsoToUpdate, gThamso);

            // activity log
            Logger.LogInformation($"GThamsoEdit id: {id}," +
                                   $"model: {StringExtensions.SerializeConvert(gThamso)}");
            return Ok();
        }
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteThamso(int id)
        {
            if (id <= 0)
                return NoContent();

            // get a gCongty with the specified id
            var data = await _gThamsoDAO.GetById(id);
            if (data == null)
                return NoContent();

            await _gThamsoBusiness.Delete(data);

            // activity log
            Logger.LogInformation($"DeleteThamso id: {id}");

            return Ok();
        }

        /// <summary>
        /// get by idnhomthamso
        /// </summary>
        /// <returns></returns>
        [HttpGet("idnhomthamso/{id:int}")]
        public async Task<IActionResult> GetByIdnhomthamso(int id)
        {
            var data = await _efRepository.TableNoTracking.Where(a => a.IdNhomthamso == id).ToListAsync();

            if (data.Count <= 0)
                return NoContent();

            return Ok(data);
        }
        #endregion
    }
}
