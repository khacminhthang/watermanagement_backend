﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace WaterManagement.API.Controllers
{
    /// <summary>
    /// Base controller
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class BaseController<T> : ControllerBase where T : BaseController<T>
    {
        private ILogger<T> _logger;

        /// <summary>
        /// ILogger
        /// </summary>
        protected ILogger<T> Logger => _logger ?? (_logger = HttpContext?.RequestServices.GetService<ILogger<T>>());
    }
}
