﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WaterManagement.Business.Business;
using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;

namespace WaterManagement.API.Controllers
{
    [Route("api/swMaybomdiemkhaithac")]
    [ApiController]
    public class SwMaybomdiemkhaithacController : BaseController<SwMaybomdiemkhaithacController>
    {
        #region Fields
        private readonly SwMaybomdiemkhaithacBusiness _swMaybomdiemkhaithacBusiness;
        private readonly ISwMaybomdiemkhaithacDAO _swMaybomdiemkhaithacDAO;
        #endregion

        #region ctor
        public SwMaybomdiemkhaithacController(SwMaybomdiemkhaithacBusiness swMaybomdiemkhaithacBusiness, ISwMaybomdiemkhaithacDAO swMaybomdiemkhaithacDAO)
        {
            _swMaybomdiemkhaithacBusiness = swMaybomdiemkhaithacBusiness;
            _swMaybomdiemkhaithacDAO = swMaybomdiemkhaithacDAO;
        }
        #endregion

        #region Method

        [HttpGet("get-list/{idDiemkhaithac:int}")]
        public async Task<IActionResult> GetAll(int idDiemkhaithac)
        {
            var data = await _swMaybomdiemkhaithacDAO.GetAll(idDiemkhaithac);
            if (data.Count <= 0)
                return NoContent();
            return Ok(data);
        }


        /// <summary>
        /// get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetById(int id)
        {
            if (id <= 0)
                return BadRequest();
            var data = await _swMaybomdiemkhaithacDAO.GetById(id);
            if (data == null)
                return NoContent();
            return Ok();
        }

        /// <summary>
        /// insert
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> InsertMaybomdiemkhaithac([FromBody]SwMaybomdiemkhaithac model)
        {
            if (model == null)
                return BadRequest();
            await _swMaybomdiemkhaithacBusiness.InsertMaybomdiemkhaithac(model);
            return Ok();
        }

        /// <summary>
        /// update
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateMaybomdiemkhaithac(int id, [FromBody]SwMaybomdiemkhaithac model)
        {
            if (model == null)
                return BadRequest();
            var swTrambomToUpdate = _swMaybomdiemkhaithacDAO.GetById(id).Result;
            if (swTrambomToUpdate == null)
                return NoContent();
            await _swMaybomdiemkhaithacBusiness.UpdateMaybomdiemkhaithac(swTrambomToUpdate, model);
            return Ok();
        }

        /// <summary>
        /// delete
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteMaybomdiemkhaithac(int id)
        {
            if (id <= 0)
                return BadRequest();
            var data = _swMaybomdiemkhaithacDAO.GetById(id).Result;
            if (data == null)
                return NoContent();
            await _swMaybomdiemkhaithacBusiness.DeleteMaybomdiemkhaithac(data);
            return Ok();
        }
        #endregion
    }
}