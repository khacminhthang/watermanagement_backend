﻿using WaterManagement.Business.Business;
using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using WaterManagement.Utils.Common.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace WaterManagement.API.Controllers
{
    /// <summary>
    /// Controller
    /// </summary>
    [Route("api/gcoquantochuc")]
    [ApiController]
    public class GCoquantochucController : BaseController<GCoquantochucController>
    {
        #region Fields

        private readonly IGCoquantochucDAO _gCoquantochucDAO;
        private readonly GCoquantochucBusiness _gCoquantochucBusiness;

        #endregion

        #region Ctor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="gCoquantochucDAO"></param>
        /// <param name="gCoquantochucBusiness"></param>
        public GCoquantochucController(IGCoquantochucDAO gCoquantochucDAO,
                                        GCoquantochucBusiness gCoquantochucBusiness)
        {
            _gCoquantochucDAO = gCoquantochucDAO;
            _gCoquantochucBusiness = gCoquantochucBusiness;
        }

        #endregion

        #region Method

        /// <summary>
        ///  Get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetbyId(int id)
        {
            if (id <= 0)
                return BadRequest();

            // get a gCoquantochuc with the specified id
            var data = await _gCoquantochucDAO.Get(a => a.Id == id && a.DeletedAt == 0);
            if (data == null)
                return NoContent();

            return Ok(new
            {
                Id = data.Id,
                Idcha = data.Idcha,
                Tencoquan = data.Tencoquan,
                Diachi = data.Diachi,
                Sodienthoai = data.Sodienthoai,
                Email = data.Email,
                Matinh = data.Matinh,
                Mahuyen = data.Mahuyen,
                Maxa = data.Maxa,
                Note = data.Note,
                ImgLink = data.ImgLink
            });
        }

        /// <summary>
        /// Get all
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var data = await _gCoquantochucDAO.GetListCoquantochuc();

            return Ok(data);
        }

        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "idcha": "",
        ///         "tencoquan": "SI",
        ///         "diachi": "57 Huynh Thuc Khang",
        ///         "matinh": 33,
        ///         "mahuyen": 330,
        ///         "maxa": 12250,
        ///         "sodienthoai": "19006746",
        ///         "email": "test@gmail.com",
        ///         "note": "",
        ///         "imgLink": ""
        ///     }
        ///
        /// </remarks>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> InsertGCoquantochuc([FromBody] GCoquantochuc model)
        {
            if (model == null)
                return BadRequest();

            if (string.IsNullOrEmpty(model.Tencoquan))
                return BadRequest("Tên cơ quan không được để trống!");

            await _gCoquantochucBusiness.InsertGCoquantochuc(model);

            // activity log
            Logger.LogInformation($"GCoquantochucInsert model: {StringExtensions.SerializeConvert(model)}");

            return Ok();
        }

        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "id": 2,
        ///         "idcha": 1,
        ///         "tencoquan": "Trung tâm - SI",
        ///         "diachi": "57 Huynh Thuc Khang",
        ///         "matinh": 33,
        ///         "mahuyen": 330,
        ///         "maxa": 12250,
        ///         "sodienthoai": "19006746",
        ///         "email": "test@gmail.com",
        ///         "note": "",
        ///         "imgLink": ""
        ///     }
        ///
        /// </remarks>
        [HttpPut("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateGCoquantochuc(int id, [FromBody] GCoquantochuc model)
        {
            if (model == null)
                return BadRequest();

            if (string.IsNullOrEmpty(model.Tencoquan))
                return BadRequest("Tên cơ quan không được để trống!");

            // get a GCoquantochuc with the specified id
            var gCoquantochucToUpdate = await _gCoquantochucDAO.Get(a => a.Id == id && a.DeletedAt == 0);
            if (gCoquantochucToUpdate == null)
                return NoContent();

            await _gCoquantochucBusiness.UpdateGCoquantochuc(gCoquantochucToUpdate, model);

            // activity log
            Logger.LogInformation($"GCoquantochucEdit id: {id}," +
                                    $"modelOld: {model.Tencoquan}" +
                                    $"modelNew: {model.Tencoquan}");

            return Ok();
        }

        /// <summary>
        /// Delete GCoquantochuc
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteGCoquantochuc(int id)
        {
            if (id <= 0)
                return BadRequest();

            // get a gCoquantochuc with the specified id
            var data = await _gCoquantochucDAO.Get(a => a.Id == id && a.DeletedAt == 0);

            await _gCoquantochucBusiness.DeleteGCoquantochuc(data);

            // activity log
            Logger.LogInformation($"GCoquantochucDelete id: {id}");

            return Ok();
        }

        #endregion
    }
}