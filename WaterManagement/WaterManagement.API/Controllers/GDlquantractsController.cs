﻿using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using WaterManagement.Models.RequestModel;
using WaterManagement.Models.ViewModels;
using WaterManagement.Utils.Common.Extensions;
using WaterManagement.Utils.Common.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace WaterManagement.API.Controllers
{
    /// <summary>
    /// GDlquantractsController
    /// </summary>
    [Route("api/gDlquantracts")]
    [ApiController]
    public class GDlquantractsController : BaseController<GDlquantractsController>
    {
        private readonly IEfRepository<GDlquantracts> _efRepository;
        private readonly IUnitOfWork _unitOfWork;
        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="efRepository"></param>
        /// <param name="unitOfWork"></param>
        public GDlquantractsController(IEfRepository<GDlquantracts> efRepository, IUnitOfWork unitOfWork)
        {
            _efRepository = efRepository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// get data by year
        /// </summary>
        /// <param name="IdTramquantrac"></param>
        /// <param name="ObjKey"></param>
        /// <param name="IdThamsodo"></param>
        /// <param name="year"></param>
        /// <param name="PageSize"></param>
        /// <param name="PageNumber"></param>
        /// <returns></returns>
        [HttpGet("year")]
        public async Task<IActionResult> GetAllDataInYear([FromQuery]int IdTramquantrac, int ObjKey, int IdThamsodo, int year, int PageSize, int PageNumber)
        {
            if (IdTramquantrac <= 0)
                return BadRequest("Bạn phải nhập id trạm quan trắc!");

            if (IdThamsodo <= 0)
                return BadRequest("Bạn phải nhập id tham số đo!");

            if (year <= 0)
                return BadRequest("Bạn phải nhập năm đo!");

            if (PageSize == -1)
                PageSize = int.MaxValue - 1;

            var query = _efRepository.TableNoTracking.Where(a => a.DeletedAt == 0
                                                       && a.IdThamsodo.Value == IdThamsodo
                                                       && a.IdTramquantrac.Value == IdTramquantrac
                                                       && a.ObjKey.Value == ObjKey
                                                       && a.Namdo.Value == year);
            if (query == null)
                return NoContent();

            var list = query.Select(a => new GDlquantractsViewModel
            {
                Id = a.Id,
                IdTramquantrac = a.IdTramquantrac,
                ObjKey = a.ObjKey,
                IdThamsodo = a.IdThamsodo,
                Phuongphapdo = a.Phuongphapdo,
                Namdo = a.Namdo,
                Thangdo = a.Thangdo,
                Ngaydo = a.Ngaydo,
                Thoigiando = a.Thoigiando,
                FullTime = a.FullTime,
                Ketquado = a.Ketquado ?? 0,
                Loaisolieu = a.Loaisolieu,
                Note = a.Note,
                AvgKetquado = Math.Round((GetAvg(a.Ngaydo, a.Namdo, query.ToList(), a.Thangdo)), 2, MidpointRounding.AwayFromZero)
            });
            var data = await list.OrderBy(a => a.Thangdo).ThenBy(a => a.Ngaydo).ToListAsync();

            var output = new PagedList<GDlquantractsViewModel>(data, PageNumber, PageSize);

            return Ok(new
            {
                Paging = output.GetHeader(),
                Items = output.List
            });
        }

        /// <summary>
        /// year-month
        /// </summary>
        /// <param name="IdTramquantrac"></param>
        /// <param name="ObjKey"></param>
        /// <param name="IdThamsodo"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="PageSize"></param>
        /// <param name="PageNumber"></param>
        /// <returns></returns>
        [HttpGet("year-month")]
        public async Task<IActionResult> GetAllDataInYearMonth([FromQuery]int IdTramquantrac, int ObjKey, int IdThamsodo, int year, int month, int PageSize, int PageNumber)
        {
            if (IdTramquantrac <= 0)
                return BadRequest("Bạn phải nhập id trạm quan trắc!");

            if (IdThamsodo <= 0)
                return BadRequest("Bạn phải nhập id tham số đo!");

            if (year <= 0)
                return BadRequest("Bạn phải nhập năm đo!");

            if (PageSize == -1)
                PageSize = int.MaxValue - 1;

            var query = await _efRepository.TableNoTracking.Where(a => a.DeletedAt == 0
                                                        && a.IdThamsodo.Value == IdThamsodo
                                                        && a.IdTramquantrac.Value == IdTramquantrac
                                                        && a.Thangdo.Value == month
                                                        && a.ObjKey.Value == ObjKey
                                                        && a.Namdo.Value == year).ToListAsync();
            if (query.Count <= 0)
                return NoContent();

            var data = new PagedList<GDlquantracts>(query, PageNumber, PageSize);

            return Ok(new
            {
                Paging = data.GetHeader(),
                Items = data.List
            });
        }

        /// <summary>
        /// GetAllDataTimeFromTimeTo
        /// </summary>
        /// <param name="IdTramquantrac"></param>
        /// <param name="ObjKey"></param>
        /// <param name="IdThamsodo"></param>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <param name="PageSize"></param>
        /// <param name="PageNumber"></param>
        /// <returns></returns>
        [HttpGet("time-from-to")]
        public async Task<IActionResult> GetAllDataTimeFromTimeTo([FromQuery] int IdTramquantrac, int ObjKey, int IdThamsodo, string timeFrom, string timeTo, int PageSize, int PageNumber)
        {
            if (PageSize == -1)
                PageSize = int.MaxValue - 1;
            // Convert string to datetime
            DateTime from = Convert.ToDateTime(timeFrom, DatetimeExtension.GetCultureInfo());
            DateTime to = Convert.ToDateTime(timeTo, DatetimeExtension.GetCultureInfo());
            //
            var query = await _efRepository.TableNoTracking.Where(a => a.DeletedAt == 0
                                                    && a.IdTramquantrac == IdTramquantrac
                                                    && a.IdThamsodo.Value == IdThamsodo
                                                    && a.ObjKey.Value == ObjKey).ToListAsync();
            var data = new List<GDlquantracts>();

            //TODO:
            string[] formats = { "dd/MM/yyyy", "MM/yyyy", "yyyy", "dd/MM/yyyy HH:mm:ss" };
            var checkTimeFrom = DatetimeExtension.CheckFormatDatetime(timeFrom, formats);
            var checkTimeTo = DatetimeExtension.CheckFormatDatetime(timeTo, formats);

            if ((checkTimeFrom == "dd/MM/yyyy HH:mm:ss" || checkTimeFrom == "dd/MM/yyyy")
                 && (checkTimeTo == "dd/MM/yyyy HH:mm:ss" || checkTimeTo == "dd/MM/yyyy"))
            {
                data = query.AsEnumerable().Where(a =>
                Convert.ToDateTime(a.FullTime, DatetimeExtension.GetCultureInfo()) >= Convert.ToDateTime(timeFrom, DatetimeExtension.GetCultureInfo())
                && Convert.ToDateTime(a.FullTime, DatetimeExtension.GetCultureInfo()) < Convert.ToDateTime(timeTo, DatetimeExtension.GetCultureInfo()).AddDays(1))
                     .OrderBy(a => a.FullTime)
                     .ToList();
            }
            if (checkTimeFrom == "MM/yyyy" && checkTimeTo == "MM/yyyy")
            {
                DateTime dtTimeFrom = Convert.ToDateTime(timeFrom, DatetimeExtension.GetCultureInfo());
                DateTime dtTimeTo = Convert.ToDateTime(timeTo, DatetimeExtension.GetCultureInfo());

                DateTime enmnth = new DateTime(dtTimeTo.Year, dtTimeTo.Month, DateTime.DaysInMonth(dtTimeTo.Year, dtTimeTo.Month));

                data = query.AsEnumerable().Where(a => Convert.ToDateTime(a.FullTime, DatetimeExtension.GetCultureInfo()) >= dtTimeFrom
                                           && Convert.ToDateTime(a.FullTime, DatetimeExtension.GetCultureInfo()) < enmnth.AddDays(1))
                                         .OrderBy(a => a.FullTime)
                                         .ToList();
            }
            if (checkTimeFrom == "yyyy" && checkTimeTo == "yyyy")
            {
                data = query.AsEnumerable().Where(a => a.Namdo >= Convert.ToInt32(timeFrom) && a.Namdo <= Convert.ToInt32(timeTo)).OrderBy(a => a.FullTime).ToList();
            }

            var output = new PagedList<GDlquantracts>(data, PageNumber, PageSize);

            return Ok(new
            {
                Paging = output.GetHeader(),
                Items = output.List
            });
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Insert([FromBody] GDlquantracts model)
        {
            if (model == null)
                return BadRequest("Bạn chưa thêm đối tượng mới");

            var Fulltime = Handlers.ConvertFullTimeToDateTime(model.FullTime);

            model.Ngaydo = Fulltime.Ngaydo;
            model.Thangdo = Fulltime.Thangdo;
            model.Namdo = Fulltime.Namdo;
            model.Thoigiando = Fulltime.Thoigiando;
            model.CreatedAt = DateTime.Now.ToString();
            model.DeletedAt = 0;

            _efRepository.Insert(model);
            await _unitOfWork.SaveChangeAsync();

            return Ok();
        }

        /// <summary>
        /// Update model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Update(int id, [FromBody] GDlquantracts model)
        {
            if (id == 0)
                return BadRequest("Bạn chưa nhập ID");

            var toUpdateModel = await _efRepository.SingleOrDefaultAsync(a => a.Id == id && a.DeletedAt == 0);

            if (toUpdateModel == null)
                return NoContent();

            toUpdateModel.IdTramquantrac = model.IdTramquantrac;
            toUpdateModel.ObjKey = model.ObjKey;
            toUpdateModel.IdThamsodo = model.IdThamsodo;
            toUpdateModel.Phuongphapdo = model.Phuongphapdo;
            toUpdateModel.Namdo = model.Namdo;
            toUpdateModel.Thangdo = model.Thangdo;
            toUpdateModel.Ngaydo = model.Ngaydo;
            toUpdateModel.Thoigiando = model.Thoigiando;
            toUpdateModel.FullTime = model.FullTime;
            toUpdateModel.Ketquado = model.Ketquado;
            toUpdateModel.Loaisolieu = model.Loaisolieu;
            toUpdateModel.Note = model.Note;
            toUpdateModel.CreatedAt = model.CreatedAt;
            toUpdateModel.UpdatedAt = DateTime.Now.ToString();
            toUpdateModel.Status = model.Status;


            _efRepository.Update(toUpdateModel);
            await _unitOfWork.SaveChangeAsync();

            return Ok();
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:long}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Delete(long id)
        {
            if (id == 0)
                return BadRequest("Bạn chưa nhập ID");

            var toDelete = await _efRepository.SingleOrDefaultAsync(a => a.Id == id && a.DeletedAt == 0);

            if (toDelete == null)
                return NoContent();
            toDelete.DeletedAt = 1;
            toDelete.UpdatedAt = DateTime.Now.ToString();

            _efRepository.Update(toDelete);
            await _unitOfWork.SaveChangeAsync();

            return Ok();
        }

        /// <summary>
        /// Get with a specify number of records
        /// </summary>
        /// <param name="IdTramquantrac"></param>
        /// <param name="ObjKey"></param>
        /// <param name="IdThamsodo"></param>
        /// <param name="Take"></param>
        /// <param name="PageSize"></param>
        /// <param name="PageNumber"></param>
        /// <returns></returns>
        [HttpGet("Take")]
        public async Task<IActionResult> GetTake([FromQuery]int IdTramquantrac, int ObjKey, int IdThamsodo, int Take, int PageSize, int PageNumber)
        {
            if (IdTramquantrac <= 0)
                return BadRequest("Bạn phải nhập id trạm quan trắc!");

            if (IdThamsodo <= 0)
                return BadRequest("Bạn phải nhập id tham số đo!");

            if (Take <= 0)
                return BadRequest("Bạn phải nhập số lượng bản ghi cần lấy!");

            if (PageSize == -1)
                PageSize = int.MaxValue - 1;

            var query = _efRepository.TableNoTracking.Where(a => a.DeletedAt == 0
                                                       && a.IdThamsodo.Value == IdThamsodo
                                                       && a.IdTramquantrac.Value == IdTramquantrac
                                                       && a.ObjKey.Value == ObjKey);

            var list = await query.OrderByDescending(a => a.Namdo).ThenByDescending( a => a.Thangdo).ThenByDescending(a => a.Ngaydo)
                       .Take(Take)
                       .OrderBy(a => a.Namdo).ThenBy(a => a.Thangdo).ThenBy(a => a.Ngaydo)
                       .ToListAsync();

            if (list.Count() <= 0)
                return NoContent();

            var data = new PagedList<GDlquantracts>(list, PageNumber, PageSize);

            return Ok(new
            {
                Paging = data.GetHeader(),
                Items = data.List
            });
        }

        /// <summary>
        /// Get the lastest day records
        /// </summary>
        /// <returns></returns>
        [HttpGet("lastest")]
        public async Task<IActionResult> GetLastestRecords([FromQuery] int IdTramquantrac, int ObjKey, int IdThamsodo)
        {
            if (IdTramquantrac <= 0)
                return BadRequest("Bạn phải nhập id trạm quan trắc!");

            if (IdThamsodo <= 0)
                return BadRequest("Bạn phải nhập id tham số đo!");

            var query = await _efRepository.TableNoTracking.Where(a => a.DeletedAt == 0
                                           && a.IdThamsodo.Value == IdThamsodo
                                           && a.IdTramquantrac.Value == IdTramquantrac
                                           && a.ObjKey.Value == ObjKey).ToListAsync();

            if (query.Count() == 0)
                return NoContent();

            // get the lastest date
            var lastestRecord = query.OrderByDescending(o => o.FullTime).First();

            // get only the date from date time
            string lastestDate = Convert.ToDateTime(lastestRecord.FullTime).ToString("yyyy-MM-dd");

            var data = query.Where(a => a.FullTime.Contains(lastestDate)).OrderBy(o => o.FullTime);

            if (data.Count() == 0)
                return NoContent();

            return Ok(data);
        }

        // tính trung bình dữ liệu đo theo ngày
        public static double GetAvg(int? ngaydo, int? year, List<GDlquantracts> list, int? thangdo)
        {
            double sum = 0;

            var query = list.FindAll(a => a.Namdo == year && a.Thangdo == thangdo
                                     && a.Ngaydo == ngaydo).ToList();

            foreach (var item in query)
            {
                if (item.Ketquado != 0)
                {
                    sum += (double)item.Ketquado;
                }

            }

            return sum / (query.Count());
        }

        /// <summary>
        /// Upload excel
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("upload-excel")]
        //[Authorize(Roles = "admin")]
        public async Task<IActionResult> UploadExcel([FromForm] UploadDlquantractsRequest request)
        {
            string fileExtension = Path.GetExtension(request.File.FileName);

            if (fileExtension != ".xls" && fileExtension != ".xlsx")
                return BadRequest("Định dạng file không đúng!");

            List<GDlquantracts> data = new List<GDlquantracts>();

            using (var stream = new MemoryStream())
            {
                await request.File.CopyToAsync(stream);
                using (var package = new ExcelPackage(stream))
                {
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    // Lấy ra trang đầu tiên của file excel
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.FirstOrDefault();

                    //lấy gia giá trị năm tại dòng 3, cột 7
                    var year = worksheet.GetValue(3, 7).ToString();

                    // giá trịc mặc định của month
                    int month = 1;

                    // giá trịc mặc định của month
                    int day = 1;

                    for (int row = 5; row <= 35; row++)
                    {
                        for (int column = 2; column <= 13; column++)
                        {
                            try
                            {
                                var value = worksheet.GetValue(row, column);
                                if (value != null && value.ToString() != "-")
                                {
                                    double giaTriDo = double.Parse(value.ToString());

                                    // chuyển đổi từ giá trị từ int sang format thời gian
                                    string fullTime = Handlers.ConvertTimeInIntToString(Int32.Parse(year), month, day, "yyyy-MM-dd");

                                    var item = new GDlquantracts()
                                    {
                                        IdTramquantrac = request.IdTramquantrac,
                                        ObjKey = request.ObjKey,
                                        IdThamsodo = request.IdThamsodo,
                                        Ngaydo = day,
                                        Thangdo = month,
                                        Namdo = Int32.Parse(year),
                                        Ketquado = giaTriDo,
                                        FullTime = fullTime,
                                        CreatedAt = DateTime.Now.ToString(),
                                    };
                                    data.Add(item);
                                }
                                month++;
                            }
                            catch (Exception ex)
                            {
                                return BadRequest($"Giá trị '{worksheet.GetValue(row, column)}' không hợp lệ tại dòng số {row}, cột {column}.");
                            }
                        }

                        day++;
                        month = 1;
                    }
                }
            }

            // insert
            _efRepository.InsertRange(data);
            //Commit
            await _unitOfWork.SaveChangeAsync();

            return Ok();
        }
    }
}
