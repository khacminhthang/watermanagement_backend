﻿using WaterManagement.Business.Business;
using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using WaterManagement.Utils.Common.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace WaterManagement.API.Controllers
{

    [Route("api/donvido")]
    [ApiController]
    public class GDonvidoController : BaseController<GDonvidoController>
    {
        private readonly IGDonvidoDAO _gDonvidoDAO;
        private readonly GDonvidoBusiness _gDonvidoBusiness;
        public GDonvidoController(IGDonvidoDAO gDonvidoDAO, GDonvidoBusiness gDonvidoBusiness)
        {
            _gDonvidoDAO = gDonvidoDAO;
            _gDonvidoBusiness = gDonvidoBusiness;
        }

        /// <summary>
        /// Get all 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var data = await _gDonvidoDAO.GetList();
            if (data.Count <= 0)
                return NoContent();

            return Ok(data);
        }

        /// <summary>
        /// GetByID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetById(int id)
        {
            if (id == 0)
                return BadRequest("Bạn chưa nhận ID");

            var data = await _gDonvidoDAO.Get(a => a.Id == id);

            if (data == null)
                return NoContent();

            return Ok(data);
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Insert([FromBody] GDonvido model)
        {
            await _gDonvidoBusiness.Insert(model);
            Logger.LogInformation($"GDonvidoInsert model: {StringExtensions.SerializeConvert(model)}");
            return Ok();
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Update(int id, [FromBody] GDonvido model)
        {
            if (id == 0)
                return BadRequest();

            var toUpdateModel = await _gDonvidoDAO.Get(a => a.Id == id);

            toUpdateModel.Tendonvido = model.Tendonvido;
            toUpdateModel.Kyhieudonvido = model.Kyhieudonvido;

            await _gDonvidoDAO.Update(toUpdateModel);

            return Ok();
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id == 0)
                return BadRequest();

            var deleteModel = await _gDonvidoDAO.Get(a => a.Id == id);

            await _gDonvidoDAO.Delete(deleteModel);

            return Ok();
        }
    }
}
