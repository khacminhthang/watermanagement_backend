﻿using WaterManagement.Business.Business;
using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using WaterManagement.Utils.Common.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace WaterManagement.API.Controllers
{
    /// <summary>
    /// GPhantich controller
    /// </summary>
    [Route("api/gphantich")]
    [ApiController]
    public class GPhantichController : BaseController<GPhantichController>
    {
        #region Fields

        private readonly GPhantichBusiness _gPhantichBusiness;
        private readonly IGPhantichDAO _gPhantichDAO;

        #endregion

        #region Ctor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="gPhantichBusiness"></param>
        /// <param name="gPhantichDAO"></param>
        public GPhantichController(GPhantichBusiness gPhantichBusiness,
                                   IGPhantichDAO gPhantichDAO)
        {
            _gPhantichBusiness = gPhantichBusiness;
            _gPhantichDAO = gPhantichDAO;
        }

        #endregion

        #region Method

        /// <summary>
        /// Get by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetbyId(int id)
        {
            if (id <= 0)
                return BadRequest();

            // get a gPhantich with the specified id
            var data = await _gPhantichDAO.GetbyId(id);
            if (data == null)
                return NoContent();

            return Ok(new
            {
                Id = data.Id,
                IdMau = data.IdMau,
                Sohieuphantich = data.Sohieuphantich,
                Nguoiphantich = data.Nguoiphantich,
                Nguoikiemtra = data.Nguoikiemtra,
                Thoigianphantich = data.Thoigianphantich,
                Note = data.Note
            });
        }

        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "idMau": 1,
        ///         "sohieuphantich": "3123das",
        ///         "nguoiphantich": "Nguyen Van A",
        ///         "nguoikiemtra": "Nguyen Van B",
        ///         "thoigianphantich": "10/10/2019",
        ///         "note": "Test thôi mà"
        ///     }
        ///
        /// </remarks>
        [HttpPost]
        public async Task<IActionResult> InsertGPhantich([FromBody] GPhantich model)
        {
            if (model == null)
                return BadRequest();

            await _gPhantichBusiness.InsertGPhantich(model);

            // activity log
            Logger.LogInformation($"GPhantichInsert model: {StringExtensions.SerializeConvert(model)}");

            return Ok();
        }

        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "idMau": 1,
        ///         "sohieuphantich": "3123das",
        ///         "nguoiphantich": "Nguyen Van A",
        ///         "nguoikiemtra": "Nguyen Van B",
        ///         "thoigianphantich": "10/10/2019",
        ///         "note": "Test thôi mà"
        ///     }
        ///
        /// </remarks>
        [HttpPut("{id:int}")]
        public async Task<IActionResult> UpdateGPhantich(int id, [FromBody] GPhantich gPhantich)
        {
            if (gPhantich == null)
                return BadRequest();

            // get a gPhantich with the specified id
            var gPhantichToUpdate = await _gPhantichDAO.GetbyId(id);
            if (gPhantichToUpdate == null)
                return NoContent();

            await _gPhantichBusiness.UpdateGPhantich(gPhantichToUpdate, gPhantich);

            // activity log
            Logger.LogInformation($"GPhantichEdit id: {id}," +
                                   $"model: {StringExtensions.SerializeConvert(gPhantich)}");
            return Ok();
        }

        /// <summary>
        /// Delete Gphantich
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> DeleteGPhantich(int id)
        {
            if (id <= 0)
                return BadRequest();

            // get a gPhantich with the specified id
            var data = await _gPhantichDAO.GetbyId(id);
            if (data == null)
                return NoContent();

            await _gPhantichBusiness.DeleteGPhantich(data);

            //activity log
            Logger.LogInformation($"GPhantichDelete id: {id}");

            return Ok();
        }

        #endregion
    }
}