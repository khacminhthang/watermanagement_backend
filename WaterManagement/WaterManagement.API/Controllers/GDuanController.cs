﻿using WaterManagement.Business.Business;
using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using WaterManagement.Utils.Common.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace WaterManagement.API.Controllers
{
    [Route("api/gduan")]
    [ApiController]
    public class GDuanController : BaseController<GDuanController>
    {
        #region Fields

        private readonly IGDuanDAO _gDuanDAO;
        private readonly GDuanBusiness _gDuanBusiness;

        #endregion

        #region Ctor
        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="gDuanDAO"></param>
        /// <param name="gDuanBusiness"></param>
        public GDuanController(IGDuanDAO gDuanDAO, GDuanBusiness gDuanBusiness)
        {
            _gDuanDAO = gDuanDAO;
            _gDuanBusiness = gDuanBusiness;
        }
        #endregion

        #region Method
        /// <summary>
        /// get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetById(int id)
        {
            if (id <= 0)
                return BadRequest();

            // get a GDuan with specified id
            var data = await _gDuanDAO.GetById(id);

            if (data == null)
                return NoContent();

            return Ok(data);
        }
        /// <summary>
        /// get all
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var data = await _gDuanDAO.GetAll();

            if (data.Count <= 0)
                return NoContent();

            return Ok(data);

        }
        /// <summary>
        /// insert
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> InsertGDuan([FromBody] GDuan model)
        {
            if (model == null)
                return BadRequest();
            if (string.IsNullOrEmpty(model.ProjectName))
                return BadRequest("Tên dự án không được để trống!");

            await _gDuanBusiness.InsertGDuan(model);

            // activity log
            Logger.LogInformation($"GDuan model: {StringExtensions.SerializeConvert(model)}");

            return Ok();
        }
        /// <summary>
        /// update
        /// </summary>
        /// <param name="id"></param>
        /// <param name="gDuan"></param>
        /// <returns></returns>
        [HttpPut("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateGDuan(int id, [FromBody] GDuan gDuan)
        {
            if (gDuan == null)
                return BadRequest();

            if (string.IsNullOrEmpty(gDuan.ProjectName))
                return BadRequest("Tên dự án không được để trống!");

            // get a GDuan with a specified id
            var gDuanToUpdate = await _gDuanDAO.GetById(id);
            if (gDuanToUpdate == null)
                return NoContent();

            await _gDuanBusiness.UpdateGDuan(gDuanToUpdate, gDuan);
            // activity log
            Logger.LogInformation($"GDuanEdit id: {id}," +
                                   $"model: {StringExtensions.SerializeConvert(gDuan)}");
            return Ok();
        }
        /// <summary>
        /// delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteGDuan(int id)
        {
            if (id <= 0)
                return NoContent();
            //get a GDuan with a specified id
            var data = await _gDuanDAO.GetById(id);
            if (data == null)
                return NoContent();

            await _gDuanBusiness.DeleteGDuan(data);

            // activity log
            Logger.LogInformation($"DeleteGDuanid: {id}");
            return Ok();
        }
        #endregion

    }
}
