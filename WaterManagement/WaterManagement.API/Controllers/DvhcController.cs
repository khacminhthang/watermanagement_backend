﻿using WaterManagement.Business.Business;
using WaterManagement.Interface.IDAO;
using WaterManagement.Utils.Common;
using WaterManagement.Utils.Common.Enums;
using WaterManagement.Utils.Common.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;
using static WaterManagement.Business.Models.Dvhc.DvhcModel;
using WaterManagement.API.Controllers;

namespace GMS.API.Controllers
{
    /// <summary>
    /// dvhc controller 
    /// </summary>
    [Route("api")]
    [ApiController]
    public class DvhcController : BaseController<DvhcController>
    {
        #region Fields

        private readonly IDvhcDAO _dvhcDAO;
        private readonly DvhcBusiness _dvhcBusiness;

        #endregion

        #region Ctor

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="dvhcDAO"></param>
        /// <param name="dvhcBusiness"></param>
        public DvhcController(IDvhcDAO dvhcDAO,
                              DvhcBusiness dvhcBusiness)
        {
            _dvhcDAO = dvhcDAO;
            _dvhcBusiness = dvhcBusiness;
        }

        #endregion

        #region Method

        /// <summary>
        /// Get all province
        /// </summary>
        /// <returns></returns>
        [HttpGet("tinh")]
        public async Task<IActionResult> GetAllProvince()
        {
            var data = await _dvhcDAO.GetAll(a => a.Parentid == null && a.Status == Status.Active);
            if (data.Count <= 0)
                return NoContent();

            return Ok(data.Select(a => new
            {
                id = a.Id,
                tendvhc = a.Tendvhc,
                matinh = a.Matinh
            }).OrderByDescending(o => o.id)
              .ToList());
        }

        /// <summary>
        /// Get all district
        /// </summary>
        /// <param name="matinh"></param>
        /// <returns></returns>
        [HttpGet("huyen/{matinh}")]
        public async Task<IActionResult> GetAllDistrict(string matinh)
        {
            if (string.IsNullOrEmpty(matinh))
                return BadRequest("Bạn phải nhập mã tỉnh!");

            var data = await _dvhcDAO.GetAll(a => a.Matinh == matinh
                                            && a.Status == Status.Active
                                            && !a.Mahuyen.Contains(AppConstants.mahuyen)
                                            && a.Maxa.Contains(AppConstants.maxa));
            if (data.Count <= 0)
                return NoContent();

            return Ok(data.Select(a => new
            {
                id = a.Id,
                tendvhc = a.Tendvhc,
                matinh = a.Matinh,
                mahuyen = a.Mahuyen
            }).OrderByDescending(o => o.id)
              .ToList());
        }

        /// <summary>
        /// Get all ward
        /// </summary>
        /// <param name="mahuyen"></param>
        /// <returns></returns>
        [HttpGet("xa/{mahuyen}")]
        public async Task<IActionResult> GetAllWard(string mahuyen)
        {
            if (string.IsNullOrEmpty(mahuyen))
                return BadRequest("Bạn phải nhập mã huyện!");

            var data = await _dvhcDAO.GetAll(a => a.Mahuyen == mahuyen
                                            && a.Status == Status.Active
                                            && !a.Maxa.Contains(AppConstants.maxa));
            if (data.Count <= 0)
                return NoContent();

            return Ok(data.Select(a => new
            {
                id = a.Id,
                tendvhc = a.Tendvhc,
                matinh = a.Matinh,
                mahuyen = a.Mahuyen,
                maxa = a.Maxa
            }).OrderByDescending(o => o.id)
              .ToList());
        }

        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "name": "Tỉnh test",
        ///         "provinceCode": 100
        ///     }
        ///
        /// </remarks>
        [HttpPost("tinh")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> InsertProvince([FromBody] InputProvinceModel model)
        {
            if (model == null)
                return BadRequest();

            if (string.IsNullOrEmpty(model.Name))
                return BadRequest("Tên tỉnh/thành phố không được để trống!");

            if (string.IsNullOrEmpty(model.ProvinceCode))
                return BadRequest("Mã tỉnh/thành phố không được để trống!");

            // check exist tendvhc
            bool checkExistName = await _dvhcBusiness.CheckExistProvinceName(model.Name);
            if (checkExistName)
                return BadRequest("Tên đơn vị hành chính tỉnh/thành phố đã tồn tại!");

            // check exist matinh
            bool checkExistMatinh = await _dvhcBusiness.CheckExistProvinceCode(model.ProvinceCode);
            if (checkExistMatinh)
                return BadRequest("Mã tỉnh/thành phố đã tồn tại!");

            await _dvhcBusiness.InsertProvince(model);

            // activity log
            Logger.LogInformation($"DvhcModel Province: {StringExtensions.SerializeConvert(model)}");

            return Ok();
        }

        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "name": "Tỉnh Hưng Yên",
        ///         "provinceCode": 33
        ///     }
        ///
        /// </remarks>
        [HttpPut("tinh/{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateProvince(int id, [FromBody] InputProvinceModel model)
        {
            if (id <= 0)
                return BadRequest("Id tỉnh/thành phố không được để trống!");

            if (model == null)
                return BadRequest();

            if (string.IsNullOrEmpty(model.Name))
                return BadRequest("Tên đơn vị hành chính không được để trống!");

            // get a Dvhc with the specified id
            var dvhcToUpdate = await _dvhcDAO.Get(a => a.Id == id && a.Status == 1);
            if (dvhcToUpdate == null)
                return NoContent();

            // check exist tendvhc
            bool checkExistName = await _dvhcBusiness.CheckExistProvinceName(model.Name, dvhcToUpdate);
            if (checkExistName)
                return BadRequest("Tên tỉnh/thành phố đã tồn tại!");

            // check exist matinh
            bool checkExistMatinh = await _dvhcBusiness.CheckExistProvinceCode(model.ProvinceCode, dvhcToUpdate);
            if (checkExistMatinh)
                return BadRequest("Mã tỉnh/thành phố đã tồn tại!");

            await _dvhcBusiness.UpdateProvince(dvhcToUpdate, model);

            // activity log
            Logger.LogInformation($"dvhcEdit Province id: {id}," +
                                  $"modelOld: {dvhcToUpdate.Tendvhc}, {dvhcToUpdate.Matinh}" +
                                  $"modelNew: {model.Name}, {model.ProvinceCode}");

            return Ok();
        }

        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "id": 21,
        ///         "name": "Test thôi mà",
        ///         "districtCode": "999",
        ///         "provinceCode": "33"
        ///     }
        ///
        /// </remarks>
        [HttpPost("huyen")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> InsertDistrict([FromBody] InputDistrictModel model)
        {
            if (model == null)
                return BadRequest("Thông tin model không được để trống!");

            if (model.Id <= 0)
                return BadRequest("Id tỉnh/thành phố không được để trống!");

            if (string.IsNullOrEmpty(model.DistrictCode))
                return BadRequest("Mã quận/huyện/thị xã không được để trống!");

            if (string.IsNullOrEmpty(model.Name))
                return BadRequest("Tên đơn vị hành chính không được để trống!");

            // check exist tendvhc
            bool checkExistName = await _dvhcBusiness.CheckExistDistrictName(model.Id, model.Name);
            if (checkExistName)
                return BadRequest("Tên đơn vị hành chính quận/huyện/thị xã đã tồn tại!");

            // check exist districtcode
            bool checkExistDistrictCode = await _dvhcBusiness.CheckExistDistrictCode(model.Id, model.DistrictCode);
            if (checkExistDistrictCode)
                return BadRequest("Mã quận/huyện/thị xã đã tồn tại!");

            await _dvhcBusiness.InsertDistrict(model);

            // activity log
            Logger.LogInformation($"DvhcModel District: {StringExtensions.SerializeConvert(model)}");

            return Ok();
        }

        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "name": "Test thôi mà",
        ///         "districtCode": "999"
        ///     }
        ///
        /// </remarks>
        [HttpPut("huyen/{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateDistrict(int id, [FromBody] InputDistrictModel model)
        {
            if (id <= 0)
                return BadRequest("Id tỉnh/thành phố không được để trống!");

            if (model == null)
                return BadRequest("Thông tin model không được để trống!");

            if (string.IsNullOrEmpty(model.DistrictCode))
                return BadRequest("Mã quận/huyện/thị xã không được để trống!");

            if (string.IsNullOrEmpty(model.Name))
                return BadRequest("Tên đơn vị hành chính không được để trống!");

            // get a Dvhc with the specified id
            var dvhcToUpdate = await _dvhcDAO.Get(a => a.Id == id && a.Status == 1);
            if (dvhcToUpdate == null)
                return NoContent();

            // check exist tendvhc
            bool checkExistName = await _dvhcBusiness.CheckExistDistrictName(model.Id, model.Name, dvhcToUpdate);
            if (checkExistName)
                return BadRequest("Tên đơn vị hành chính quận/huyện/thị xã đã tồn tại!");

            // check exist districtcode
            bool checkExistDistrictCode = await _dvhcBusiness.CheckExistDistrictCode(model.Id, model.DistrictCode, dvhcToUpdate);
            if (checkExistDistrictCode)
                return BadRequest("Mã quận/huyện/thị xã đã tồn tại!");

            await _dvhcBusiness.UpdateDistrict(dvhcToUpdate, model);

            // activity log
            Logger.LogInformation($"dvhcEdit District districtId: {id}," +
                                  $"modelOld: {dvhcToUpdate.Tendvhc}, {dvhcToUpdate.Mahuyen}" +
                                  $"modelNew: {model.Name}, {model.DistrictCode}");

            return Ok();
        }

        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "id": 21,
        ///         "name": "Test thôi mà",
        ///         "provinceCode": "33",
        ///         "districtCode": "999",
        ///         "wardCode": "99999"
        ///     }
        ///
        /// </remarks>
        [HttpPost("xa")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> InsertWard([FromBody] InputWardModel model)
        {
            if (model == null)
                return BadRequest("Thông tin model không được để trống!");

            if (model.Id <= 0)
                return BadRequest("Id quận/huyện/thị xã không được để trống!");

            if (string.IsNullOrEmpty(model.Name))
                return BadRequest("Tên đơn vị hành chính không được để trống!");

            if (string.IsNullOrEmpty(model.ProvinceCode))
                return BadRequest("Mã tỉnh/thành phố không được để trống!");

            if (string.IsNullOrEmpty(model.DistrictCode))
                return BadRequest("Mã quận/huyện/thị xã không được để trống!");

            if (string.IsNullOrEmpty(model.WardCode))
                return BadRequest("Mã xã/phường/thị trấn không được để trống!");

            // check exist tendvhc
            bool checkExistName = await _dvhcBusiness.CheckExistWardName(model.Id, model.Name);
            if (checkExistName)
                return BadRequest("Tên đơn vị hành chính xã/phường/thị trấn đã tồn tại!");

            // check exist wardCode
            bool checkExistWardCode = await _dvhcBusiness.CheckExistWardCode(model.Id, model.WardCode);
            if (checkExistWardCode)
                return BadRequest("Mã xã/phường/thị trấn đã tồn tại!");

            await _dvhcBusiness.InsertWard(model);

            // activity log
            Logger.LogInformation($"DvhcModel Ward: {StringExtensions.SerializeConvert(model)}");

            return Ok();
        }

        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "name": "Test thôi mà",
        ///         "wardCode": "99999"
        ///     }
        ///
        /// </remarks>
        [HttpPut("xa/{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateWard(int id, [FromBody]InputWardModel model)
        {
            if (id <= 0)
                return BadRequest("Id quận/huyện/thị xã không được để trống!");

            if (model == null)
                return BadRequest("Thông tin model không được để trống!");

            if (string.IsNullOrEmpty(model.Name))
                return BadRequest("Tên đơn vị hành chính không được để trống!");

            if (string.IsNullOrEmpty(model.WardCode))
                return BadRequest("Mã xã/phường/thị trấn không được để trống!");

            // get a Dvhc with the specified id
            var dvhcToUpdate = await _dvhcDAO.Get(a => a.Id == id && a.Status == 1);
            if (dvhcToUpdate == null)
                return NoContent();

            // check exist tendvhc
            bool checkExistName = await _dvhcBusiness.CheckExistWardName(model.Id, model.Name, dvhcToUpdate);
            if (checkExistName)
                return BadRequest("Tên đơn vị hành chính xã/phường/thị trấn đã tồn tại!");

            // check exist wardCode
            bool checkExistWardCode = await _dvhcBusiness.CheckExistWardCode(model.Id, model.WardCode, dvhcToUpdate);
            if (checkExistWardCode)
                return BadRequest("Mã xã/phường/thị trấn đã tồn tại!");

            await _dvhcBusiness.UpdateWard(dvhcToUpdate, model);

            // activity log
            Logger.LogInformation($"dvhcEdit Ward WardId: {id}," +
                                  $"modelOld: {dvhcToUpdate.Tendvhc}, {dvhcToUpdate.Maxa}" +
                                  $"modelNew: {model.Name}, {model.WardCode}");

            return Ok();
        }

        /// <summary>
        /// Delete province
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("tinh/{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteProvince(int id)
        {
            if (id == 0)
                return BadRequest("Bạn chưa nhập ID");

            await _dvhcBusiness.DeleteProvince(id);

            return Ok();
        }

        /// <summary>
        /// DeleteProvince
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("huyen/{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteDistrict(int id)
        {
            if (id == 0)
                return BadRequest("Bạn chưa nhập ID");

            await _dvhcBusiness.DeleteDistrict(id);

            return Ok();
        }

        /// <summary>
        /// DeleteWard
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("xa/{id:int}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteWard(int id)
        {
            if (id == 0)
                return BadRequest("Bạn chưa nhập ID");

            await _dvhcBusiness.DeleteWard(id);

            return Ok();
        }

        #endregion
    }
}