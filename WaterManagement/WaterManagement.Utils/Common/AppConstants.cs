﻿
namespace WaterManagement.Utils.Common
{
    /// <summary>
    /// App Constants
    /// </summary>
    public class AppConstants
    {
        #region 

        // GFts Table
        public const string AttributeName = "name";
        public const string Location = "location";
        public const string Organization = "organization";

        //
        public const string mahuyen = "000";
        public const string maxa = "00000";

        //
        public const short deleted_at = 0;

        //
        public const int typeMtTramkttv = 1;
        public const int typeGwTram = 2;
        public const int typeGwGiengquantrac = 3;
        public const int typeSwDap = 4;
        public const int typeSwDiemxathai = 5;
        public const int typeGMau = 6;
        public const int typeSwDiemkhaithac = 7;
        public const int typeGwGiengkhaithac = 8;

        #endregion
    }
}
