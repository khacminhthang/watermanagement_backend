﻿using System.IO;

namespace WaterManagement.Utils.Common
{
    /// <summary>
    /// File IO
    /// </summary>
    public static class FileIO
    {
        /// <summary>
        /// Create Directory
        /// </summary>
        /// <param name="pathDir"></param>
        public static void CreateDirectory(string pathDir)
        {
            // If the directory doesn't exist, create it.
            if (!Directory.Exists(pathDir))
            {
                Directory.CreateDirectory(pathDir);
            }
        }
    }
}
