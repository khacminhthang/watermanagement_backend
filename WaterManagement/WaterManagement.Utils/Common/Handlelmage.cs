﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace WaterManagement.Utils.Common
{
    /// <summary>
    /// Handle Images
    /// </summary>
    public static class HandleImage
    {
        #region Method

        /// <summary>
        /// save image from base 64
        /// </summary>
        public static void SaveImageFromBase64(string base64, string pathToSave, string fileExtensions)
        {
            Image image = Base64ToImage(base64);

            // get width and height from image
            var resultWidthAndHeight = GetWidthAndHeightImage(base64);

            //
            HandleImageUpload(image, resultWidthAndHeight.Item1, resultWidthAndHeight.Item2, pathToSave + fileExtensions, fileExtensions);
        }

        /// <summary>
        /// Convert base 64 to image
        /// </summary>
        /// <returns></returns>
        public static Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);

            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);

            Image image = Image.FromStream(ms, true);

            return image;
        }

        /// <summary>
        /// Handle image upload
        /// </summary>
        public static void HandleImageUpload(Image bmpImage, int maxWidth, int maxHeight, string filePath, string fileExtension)
        {
            Image image = ResizeImage(bmpImage, maxWidth, maxHeight);

            ImageFormat imageFormat = ImageFormat.Jpeg;

            switch (fileExtension)
            {
                case ".png":
                    imageFormat = ImageFormat.Png;
                    break;

                case ".gif":
                    imageFormat = ImageFormat.Gif;
                    break;

                case ".icon":
                    imageFormat = ImageFormat.Icon;
                    break;

                case ".bmp":
                    imageFormat = ImageFormat.Bmp;
                    break;
            }

            image.Save(filePath, imageFormat);
        }

        /// <summary>
        /// Resize image
        /// </summary>
        /// <param name="image"></param>
        /// <param name="maxWidth"></param>
        /// <param name="maxHeight"></param>
        /// <returns></returns>
        public static Image ResizeImage(Image image, int maxWidth, int maxHeight)
        {
            if (image.Height < maxHeight && image.Width < maxWidth)
                return image;

            using (image)
            {
                Double xRatio = (double)image.Width / maxWidth;
                Double yRatio = (double)image.Height / maxHeight;
                Double ratio = Math.Max(xRatio, yRatio);
                int nnx = (int)Math.Floor(image.Width / ratio);
                int nny = (int)Math.Floor(image.Height / ratio);
                Bitmap cpy = new Bitmap(nnx, nny, PixelFormat.Format32bppArgb);
                using (Graphics gr = Graphics.FromImage(cpy))
                {
                    gr.Clear(Color.Transparent);

                    // This is said to give best quality when resizing images
                    gr.InterpolationMode = InterpolationMode.HighQualityBicubic;

                    gr.DrawImage(image,
                        new Rectangle(0, 0, nnx, nny),
                        new Rectangle(0, 0, image.Width, image.Height),
                        GraphicsUnit.Pixel);
                }

                return cpy;
            }
        }

        /// <summary>
        /// Get width, height of base64 image
        /// </summary>
        /// <returns></returns>
        public static Tuple<int, int> GetWidthAndHeightImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] image = Convert.FromBase64String(base64String);

            using (var ms = new MemoryStream(image))
            {
                Image img = Image.FromStream(ms);
                return new Tuple<int, int>(img.Width, img.Height);
            }
        }

        #endregion
    }
}
