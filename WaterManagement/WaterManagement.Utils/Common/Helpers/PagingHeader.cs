﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WaterManagement.Utils.Common.Helpers
{
    public class PagingHeader
    {
        #region Fields

        public int TotalItems { get; }
        public int PageNumber { get; }
        public int PageSize { get; }
        public int TotalPages { get; }

        #endregion

        #region Ctor

        public PagingHeader(int totalItems, int pageNumber, int pageSize, int totalPages)
        {
            this.TotalItems = totalItems;
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.TotalPages = totalPages;
        }

        #endregion
    }
}
