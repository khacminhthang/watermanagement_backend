﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WaterManagement.Utils.Common.Helpers
{
    /// <summary>
    /// Paged list
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PagedList<T>
    {
        #region Fields

        public int TotalItems { get; }
        public int PageNumber { get; }
        public int PageSize { get; }
        public List<T> List { get; }

        #endregion

        #region Ctor

        public PagedList(IList<T> source, int pageNumber, int pageSize)
        {
            this.TotalItems = source.Count();
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.List = source
                            .Skip(pageSize * (pageNumber - 1))
                            .Take(pageSize)
                            .ToList();
        }

        #endregion

        #region Properties

        public int TotalPages => (int)Math.Ceiling(this.TotalItems / (double)this.PageSize);

        #endregion

        #region Method

        public PagingHeader GetHeader()
        {
            return new PagingHeader(this.TotalItems, this.PageNumber, this.PageSize, this.TotalPages);
        }

        #endregion
    }
}
