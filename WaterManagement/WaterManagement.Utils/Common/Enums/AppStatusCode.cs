﻿
namespace WaterManagement.Utils.Common.Enums
{
    /// <summary>
    /// App status code
    /// </summary>
    public static class AppStatusCode
    {
        public static readonly int NoContent = 204;
        public static readonly int BadRequest = 400;
        public static readonly int Forbid = 403;
        public static readonly int NotFound = 404;
    }
}
