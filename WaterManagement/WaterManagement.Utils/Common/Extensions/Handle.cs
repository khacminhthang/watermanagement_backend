﻿using System;

namespace WaterManagement.Utils.Common.Extensions
{
    public class Handlers
    {
        /// <summary>
        /// ConvertFullTimeToDateTime
        /// </summary>
        /// <param name="FullTime"></param>
        /// <returns></returns>
        public static OutputTime ConvertFullTimeToDateTime(string FullTime)
        {
            var time = new OutputTime();
            //TODO:
            string[] formats = { "yyyy-MM-dd", "yyyy-MM", "yyyy", "yyyy-MM-dd HH:mm:ss", "dd/MM/yyyy HH:mm:ss" };

            var checkDatetime = DatetimeExtension.CheckFormatDatetime(FullTime, formats);
            DateTime dt;
            switch (checkDatetime)
            {
                case "yyyy-MM":
                    dt = Convert.ToDateTime(FullTime, DatetimeExtension.GetCultureInfo());
                    time.Ngaydo = null;
                    time.Thangdo = dt.Month;
                    time.Namdo = dt.Year;
                    time.Thoigiando = null;
                    break;

                case "yyyy-MM-dd":
                    dt = Convert.ToDateTime(FullTime, DatetimeExtension.GetCultureInfo());
                    time.Ngaydo = dt.Day;
                    time.Thangdo = dt.Month;
                    time.Namdo = dt.Year;
                    time.Thoigiando = null;
                    break;

                case "yyyy":
                    time.Ngaydo = null;
                    time.Thangdo = null;
                    time.Namdo = Convert.ToInt32(FullTime);
                    time.Thoigiando = null;
                    break;

                case "yyyy-MM-dd HH:mm:ss":
                    dt = Convert.ToDateTime(FullTime, DatetimeExtension.GetCultureInfo());
                    time.Ngaydo = dt.Day;
                    time.Thangdo = dt.Month;
                    time.Namdo = dt.Year;
                    time.Thoigiando = dt.TimeOfDay.ToString();
                    break;
                case "dd/MM/yyyy HH:mm:ss":
                    dt = Convert.ToDateTime(FullTime, DatetimeExtension.GetCultureInfo());
                    time.Ngaydo = dt.Day;
                    time.Thangdo = dt.Month;
                    time.Namdo = dt.Year;
                    time.Thoigiando = dt.TimeOfDay.ToString();
                    break;
            }

            return time;
        }

        /// <summary>
        /// OutputTime
        /// </summary>
        public class OutputTime
        {
            /// <summary>
            /// Gets or sets the Ngaydo
            /// </summary>
            public int? Ngaydo { get; set; }

            /// <summary>
            /// Gets or sets the Thangdo
            /// </summary>
            public int? Thangdo { get; set; }

            /// <summary>
            /// Gets or sets the Namdo
            /// </summary>
            public int? Namdo { get; set; }

            /// <summary>
            /// Gets or sets the Thoigiando
            /// </summary>
            public string Thoigiando { get; set; }
        }

        /// <summary>
        /// Get  File name and type
        /// </summary>
        /// <param name="link"></param>
        /// <param name="option"></param>
        /// <returns></returns>
        public static String GetFile(string link, int option)
        {
            string[] splitType = link.Split(new char[] { '/', '.', '\\' });
            // return type of file
            if (option == 1)
                return "." + splitType[splitType.Length - 1];

            // return name of file
            if (option == 2)
                return splitType[splitType.Length - 2];

            return "";
        }

        /// <summary>
        /// Get File Path
        /// </summary>
        /// <param name="link"></param>
        /// <returns></returns>
        public static String GetFilePath(string link)
        {
            string[] splitString = link.Split(new string[] { "images" }, StringSplitOptions.None);

            return "wwwroot/images" + splitString[splitString.Length - 1];

            //Sample return : wwwroot/images/2020/3/27/img_E7A65C07D027486D83296AF497C4AB67.jpg
        }

        /// <summary>
        /// ConvertTimeInIntToString
        /// </summary>
        /// <param name="FullTime"></param>
        /// <returns></returns>
        public static string ConvertTimeInIntToString(int year, int month, int day, string format)
        {
            string[] formats = { "yyyy-MM-dd", "yyyy-MM", "yyyy", "yyyy-MM-dd HH:mm:ss", "dd/MM/yyyy HH:mm:ss" };

            // chuỗi giá trị tháng
            string mm = month < 10 ? $"0{month.ToString()}" : $"{month.ToString()}";
            // chuỗi giá trị ngày
            string dd = day < 10 ? $"0{day.ToString()}" : $"{day.ToString()}";

            string yyyy = year.ToString();

            string result = "";

            switch (format)
            {
                case "dd-MM-yyyy":
                    result = dd + '-' + mm + '-' + yyyy;
                    break;
                case "yyyy-MM-dd":
                    result = yyyy + '-' + mm + '-' + dd;
                    break;
            }

            return result;
        }
    }
}
