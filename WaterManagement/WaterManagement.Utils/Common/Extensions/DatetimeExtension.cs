﻿using System;
using System.Globalization;

namespace WaterManagement.Utils.Common.Extensions
{
    /// <summary>
    /// Datetime extensions
    /// </summary>
    public class DatetimeExtension
    {
        #region Constants

        public const string LANGUAGE_CULTURE_NAME = "vi-VN";

        #endregion

        #region Method

        /// <summary>
        /// Get culture info
        /// </summary>
        /// <returns></returns>
        public static CultureInfo GetCultureInfo()
        {
            return new CultureInfo(LANGUAGE_CULTURE_NAME);
        }

        /// <summary>
        /// Convert Date
        /// </summary>
        /// <param name="Date">Date</param>
        /// <param name="DateFormat"> format date</param>
        /// <returns> Date format as DateFormat</returns>
        public static string ConvertDate(DateTime date, string dateFormat)
        {
            if (date != null)
            {
                return date.ToString(dateFormat);
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// Convert string Datetime
        /// </summary>
        /// <param name="DateTime">string Datetime to convert</param>
        /// <param name="DateFormat">string format date</param>
        /// <returns>string Datetime format as DateFormat</returns>
        public static string ConvertDateTime(string dateTime, string DateFormat)
        {
            if (dateTime.Length > 0)
            {
                DateTime dt = Convert.ToDateTime(dateTime);
                return dt.ToString(DateFormat);
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        ///  Convert string Time
        /// </summary>
        /// <param name="Time">string Time to convert</param>
        /// <param name="TimeFormat">string format time</param>
        /// <returns>string Time format as TimeFormat</returns>
        public static string ConvertTime(string Time, string TimeFormat)
        {
            if (Time.Length > 0)
            {
                DateTime dt = Convert.ToDateTime(Time);
                return dt.ToString(TimeFormat);
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// Dayofweek
        /// </summary>
        /// <param name="Date"></param>
        /// <returns></returns>
        public static string DayOfWeek(DateTime Date)
        {
            string strReturn = "";
            if (Date != null)
            {
                switch (Date.DayOfWeek.ToString())
                {
                    case "Sunday":
                        strReturn = "Chủ nhật";
                        break;
                    case "Monday":
                        strReturn = "Thứ 2";
                        break;
                    case "Tuesday":
                        strReturn = "Thứ 3";
                        break;
                    case "Wednesday":
                        strReturn = "Thứ 4";
                        break;
                    case "Thursday":
                        strReturn = "Thứ 5";
                        break;
                    case "Friday":
                        strReturn = "Thứ 6";
                        break;
                    case "Saturday":
                        strReturn = "Thứ 7";
                        break;
                }
            }

            return strReturn;
        }

        /// <summary>
        /// Check If input datetime
        /// </summary>
        /// <param name="strDateTime"></param>
        /// <returns></returns>
        public static string CheckFormatDatetime(string strDateTime, string[] formats)
        {
            DateTime dateValue;
            string formatDateValue = "";

            foreach (string dateStringFormat in formats)
            {
                if (DateTime.TryParseExact(strDateTime, dateStringFormat,
                                           CultureInfo.InvariantCulture,
                                           DateTimeStyles.None,
                                           out dateValue))

                    formatDateValue = dateStringFormat;
            }

            return formatDateValue;
        }
        #endregion
    }
}
