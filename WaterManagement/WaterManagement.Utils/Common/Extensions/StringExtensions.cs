﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;

namespace WaterManagement.Utils.Common.Extensions
{
    /// <summary>
    /// String Extensions
    /// </summary>
    public static class StringExtensions
    {
        #region Method

        /// <summary>
        /// Convert string to IEnumerable
        /// example str ="1,2,3,4"; output IEnumberable int
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static IEnumerable<int> ConvertString(this string str)
        {
            return str.Split(',', StringSplitOptions.RemoveEmptyEntries)
                        .Select(s => new { Success = int.TryParse(s, out var value), value })
                        .Where(w => w.Success)
                        .Select(s => s.value);
        }

        /// <summary>
        /// Gen uid "B06A3C7262A5453BB1A7FC7FB69053FD"
        /// </summary>
        /// <returns></returns>
        public static string GenUid()
        {
            return Guid.NewGuid().ToString("N").ToUpper();
        }

        /// <summary>
        /// Convert object to Json
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static string SerializeConvert(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        /// <summary>
        /// Random string
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static string RandomString(int size)
        {
            Random rnd = new Random();
            string srds = "";
            string[] str = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            for (int i = 0; i < size; i++)
            {
                srds = srds + str[rnd.Next(0, 51)];
            }

            return srds;
        }

        /// <summary>
        /// Random number
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static string RandomNumber(int size)
        {
            Random rnd = new Random();
            string srds = "";
            string[] str = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
            for (int i = 0; i < size; i++)
            {
                srds = srds + str[rnd.Next(0, 10)];
            }

            return srds;
        }

        /// <summary>
        /// Convert To UnSign
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ConvertToUnSign(string input)
        {
            if (string.IsNullOrEmpty(input))
                return "";

            input = input.Trim();
            for (int i = 0x20; i < 0x30; i++)
            {
                input = input.Replace(((char)i).ToString(), " ");
            }
            Regex regex = new Regex(@"\p{IsCombiningDiacriticalMarks}+");
            string str = input.Normalize(NormalizationForm.FormD);
            string str2 = regex.Replace(str, string.Empty).Replace('đ', 'd').Replace('Đ', 'D');
            while (str2.IndexOf("?") >= 0)
            {
                str2 = str2.Remove(str2.IndexOf("?"), 1);
            }

            return str2;
        }

        /// <summary>
        /// Check Is null or empty
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsNotNullOrEmpty(this string value)
        {
            return !string.IsNullOrEmpty(value);
        }

        /// <summary>
        /// Get name column in table
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="memberAccess"></param>
        /// <returns></returns>
        public static string GetMemberName<T, TValue>(Expression<Func<T, TValue>> memberAccess)
        {
            return ((MemberExpression)memberAccess.Body).Member.Name;
        }

        /// <summary>
        /// Get name of class
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static string NameOfClass<T>()
        {
            return typeof(T).Name;
        }

        /// <summary>
        /// get name of class variable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static string NameOfClassVariable(object a)
        {
            return a.GetType().Name;
        }

        /// <summary>
        ///  Get month before, after
        /// </summary>
        /// <param name="monthCurrent"></param>
        /// <returns></returns>
        public static Tuple<int, int> GetMonth(int monthCurrent)
        {
            int monthBefor;
            int monthAfter;

            monthBefor = monthCurrent - 1;
            monthAfter = monthCurrent + 1;
            if (monthBefor == 0)
            {
                monthBefor = 12;
            }

            if (monthAfter == 13)
            {
                monthAfter = 1;
            }

            return new Tuple<int, int>(monthBefor, monthAfter);
        }

        /// <summary>
        /// Get year
        /// </summary>
        /// <param name="yearCurrent"></param>
        /// <returns></returns>
        public static Tuple<int, int> GetYear(int yearCurrent)
        {
            int yearBefor;
            int yearhAfter;

            yearBefor = yearCurrent - 1;
            yearhAfter = yearCurrent + 1;

            return new Tuple<int, int>(yearBefor, yearhAfter);
        }

        #endregion
    }
}
