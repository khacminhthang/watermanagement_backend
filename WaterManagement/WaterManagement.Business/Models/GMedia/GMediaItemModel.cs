﻿using System;

namespace WaterManagement.Business.Models.GMedia
{
    /// <summary>
    /// GMedia Item Model
    /// </summary>
    public partial class GMediaItemModel
    {
        public double? Toadox { get; set; }
        public double? Toadoy { get; set; }
        public int Srid { get; set; }
        public string Vitri { get; set; }
        public string Tieude { get; set; }
        public string Thoigiankhoitao { get; set; }
        public string Note { get; set; }
        public String Base64 { get; set; }
    }
}
