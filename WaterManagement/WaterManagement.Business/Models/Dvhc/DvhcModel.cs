﻿
namespace WaterManagement.Business.Models.Dvhc
{
    public class DvhcModel
    {
        /// <summary>
        /// Input province Model
        /// </summary>
        public class InputProvinceModel
        {
            public string Name { get; set; }
            public string ProvinceCode { get; set; }
        }

        /// <summary>
        /// input district model
        /// </summary>
        public class InputDistrictModel
        {
            public int Id { get; set; }
            public string ProvinceCode { get; set; }
            public string Name { get; set; }
            public string DistrictCode { get; set; }
        }

        /// <summary>
        /// input ward model
        /// </summary>
        public class InputWardModel
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string ProvinceCode { get; set; }
            public string DistrictCode { get; set; }
            public string WardCode { get; set; }

        }
    }
}
