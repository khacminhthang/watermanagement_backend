﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using System;
using System.Threading.Tasks;
namespace WaterManagement.Business.Business
{
    public class GCanhanBusiness
    {
        #region Fields

        private readonly IGCanhanDAO _gCanhanDAO;

        #endregion

        #region Ctor

        public GCanhanBusiness(IGCanhanDAO gCanhanDAO)
        {
            _gCanhanDAO = gCanhanDAO;

        }

        #endregion

        #region Method

        /// <summary>
        /// Update ca nhan
        /// </summary>
        /// <param name="gCanhan"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task UpdateGCanhan(GCanhan gCanhan, GCanhan entity)
        {
            if (gCanhan == null)
                throw new ArgumentNullException(nameof(gCanhan));
            //

            gCanhan.Hovaten = entity.Hovaten;
            gCanhan.Diachi = entity.Diachi;
            gCanhan.Matinh = entity.Matinh;
            gCanhan.Mahuyen = entity.Mahuyen;
            gCanhan.Maxa = entity.Maxa;
            gCanhan.Sodienthoai = entity.Sodienthoai;
            gCanhan.Email = entity.Email;
            gCanhan.Loaigiayto = entity.Loaigiayto;
            gCanhan.Socmthochieu = entity.Socmthochieu;
            gCanhan.Note = entity.Note;
            gCanhan.ImgLink = entity.ImgLink;
            gCanhan.UpdatedAt = DateTime.Now.ToString();
            //
            await _gCanhanDAO.UpdateGCanhan(gCanhan);
        }
        /// <summary>
        /// Insert ca nhan
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task InsertGCanhan(GCanhan model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            model.CreatedAt = DateTime.Now.ToString();

            await _gCanhanDAO.InsertGCaNhan(model);
        }
        /// <summary>
        /// delete ca nhan
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task DeleteGCanhan(GCanhan model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            model.DeletedAt = 1;
            model.UpdatedAt = DateTime.Now.ToString();

            await _gCanhanDAO.DeleteGCanhan(model);
        }
        #endregion
    }
}
