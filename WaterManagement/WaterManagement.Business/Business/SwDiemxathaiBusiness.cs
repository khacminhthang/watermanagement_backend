﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;
using WaterManagement.Utils.Common.Helpers;
using System;
using System.Threading.Tasks;

namespace WaterManagement.Business.Business
{
    public class SwDiemxathaiBusiness
    {
        #region Fields

        private readonly ISwDiemxathaiDAO _swDiemxathaiDAO;
        private readonly IGenerateLocationDAO _generateLocationDAO;

        #endregion

        #region Ctor

        public SwDiemxathaiBusiness(ISwDiemxathaiDAO swDiemxathaiDAO,
                                    IGenerateLocationDAO generateLocationDAO)
        {
            _swDiemxathaiDAO = swDiemxathaiDAO;
            _generateLocationDAO = generateLocationDAO;
        }

        #endregion

        #region Method

        /// <summary>
        /// Insert SwDiemxathai
        /// </summary>
        /// <param name="swDiemxathai"></param>
        /// <returns></returns>
        public async Task InsertSwDiemxathai(SwDiemxathai swDiemxathai)
        {
            if (swDiemxathai == null)
                throw new ArgumentNullException(nameof(swDiemxathai));

            swDiemxathai.Geom = _generateLocationDAO.GenerateLocation(swDiemxathai.Toadox.Value,
                                                               swDiemxathai.Toadoy.Value,
                                                               swDiemxathai.Srid.Value).Geometry;
            swDiemxathai.CreatedAt = DateTime.Now.ToString();
            await _swDiemxathaiDAO.InsertSwDiemxathai(swDiemxathai);

        }

        /// <summary>
        /// Update SwDiemxathai
        /// </summary>
        /// <param name="swDiemxathai"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task UpdateSwDiemxathai(SwDiemxathai swDiemxathai, SwDiemxathai entity)
        {
            if (swDiemxathai == null)
                throw new ArgumentNullException(nameof(swDiemxathai));

            swDiemxathai.Geom = _generateLocationDAO.GenerateLocation(entity.Toadox.Value,
                                                                      entity.Toadoy.Value,
                                                                      entity.Srid.Value).Geometry;
            swDiemxathai.Sohieudiem = entity.Sohieudiem;
            swDiemxathai.Tendiem = entity.Tendiem;
            swDiemxathai.Toadox = entity.Toadox;
            swDiemxathai.Toadoy = entity.Toadoy;
            swDiemxathai.Caodoz = entity.Caodoz;
            swDiemxathai.Srid = entity.Srid;
            swDiemxathai.Vitri = entity.Vitri;
            swDiemxathai.Matinh = entity.Matinh;
            swDiemxathai.Mahuyen = entity.Mahuyen;
            swDiemxathai.Maxa = entity.Maxa;
            swDiemxathai.Thoigianxaydung = entity.Thoigianxaydung;
            swDiemxathai.Thoigianvanhanh = entity.Thoigianvanhanh;
            swDiemxathai.IdDonviquanly = entity.IdDonviquanly;
            swDiemxathai.IdChusudung = entity.IdChusudung;
            swDiemxathai.Thuyvuctiepnhan = entity.Thuyvuctiepnhan;
            swDiemxathai.Phuongthucxa = entity.Phuongthucxa;
            swDiemxathai.Hinhthucdan = entity.Hinhthucdan;
            swDiemxathai.Luuluongxatrungbinh = entity.Luuluongxatrungbinh;
            swDiemxathai.Luuluonglonnhatchophep = entity.Luuluonglonnhatchophep;
            swDiemxathai.Quychuanapdung = entity.Quychuanapdung;
            swDiemxathai.Cothietbigiamsat = entity.Cothietbigiamsat;
            swDiemxathai.Dacapphep = entity.Dacapphep;
            swDiemxathai.Note = entity.Note;
            swDiemxathai.ImgLink = entity.ImgLink;
            swDiemxathai.UpdatedAt = DateTime.Now.ToString();
            swDiemxathai.ObjKey = entity.ObjKey;
            swDiemxathai.IdBody = entity.IdBody;
            swDiemxathai.BodyKey = entity.BodyKey;

            await _swDiemxathaiDAO.UpdateSwDiemxathai(swDiemxathai);


        }

        /// <summary>
        /// Delete SwDiemxathai
        /// </summary>
        /// <param name="swDiemxathai"></param>
        /// <returns></returns>
        public async Task DeleteSwDiemxathai(SwDiemxathai swDiemxathai)
        {
            if (swDiemxathai == null)
                throw new ArgumentNullException(nameof(swDiemxathai));

            swDiemxathai.UpdatedAt = DateTime.Now.ToString();
            swDiemxathai.DeletedAt = 1;
            await _swDiemxathaiDAO.DeleteSwDiemxathai(swDiemxathai);
        }

        /// <summary>
        /// Get all
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<PagedList<SwDiemxathaiViewModel>> GetAll(int pageNumber, int pageSize)
        {
            var data = await _swDiemxathaiDAO.GetAll();

            return new PagedList<SwDiemxathaiViewModel>(data, pageNumber, pageSize);
        }

        #endregion
    }
}
