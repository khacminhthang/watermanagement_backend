﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using System;
using System.Threading.Tasks;

namespace WaterManagement.Business.Business
{
    public class GThamsoBusiness
    {
        #region Fields

        private readonly IGThamsoDAO _gthamsoDAO;

        #endregion

        #region Ctor
        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="gthamsoDAO"></param>
        public GThamsoBusiness(IGThamsoDAO gthamsoDAO)
        {
            _gthamsoDAO = gthamsoDAO;

        }

        #endregion

        #region Method
        /// <summary>
        /// insert
        /// </summary>
        /// <param name="gThamso"></param>
        /// <returns></returns>
        public async Task Insert(GThamso gThamso)
        {
            if (gThamso == null)
                throw new ArgumentNullException(nameof(gThamso));

            await _gthamsoDAO.Insert(gThamso);

        }
        /// <summary>
        /// update
        /// </summary>
        /// <param name="gThamso"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task Update(GThamso gThamso, GThamso entity)
        {
            if (gThamso == null)
                throw new ArgumentNullException(nameof(gThamso));

            gThamso.Tenthamso = entity.Tenthamso;
            gThamso.Kyhieuthamso = entity.Kyhieuthamso;
            gThamso.IdNhomthamso = entity.IdNhomthamso;
            gThamso.IdDonvidomacdinh = entity.IdDonvidomacdinh;

            await _gthamsoDAO.Update(gThamso);

        }
        /// <summary>
        /// delete
        /// </summary>
        /// <param name="gThamso"></param>
        /// <returns></returns>
        public async Task Delete(GThamso gThamso)
        {
            if (gThamso == null)
                throw new ArgumentNullException(nameof(gThamso));

            await _gthamsoDAO.Delete(gThamso);

        }

        #endregion


    }
}
