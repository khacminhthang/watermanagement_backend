﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using WaterManagement.Utils.Common.Extensions;
using System;
using System.Threading.Tasks;

namespace WaterManagement.Business.Business
{
    public class GNhomthamsoBusiness
    {
        #region Fields

        private readonly IGNhomthamsoDAO _gNhomthamsoDAO;

        #endregion

        #region Ctor

        public GNhomthamsoBusiness(IGNhomthamsoDAO gNhomthamsoDAO)
        {
            _gNhomthamsoDAO = gNhomthamsoDAO;
        }

        #endregion

        #region Method

        /// <summary>
        /// Insert GNhomthamso
        /// </summary>
        /// <param name="gNhomthamso"></param>
        /// <returns></returns>
        public async Task InsertGNhomthamso(GNhomthamso gNhomthamso)
        {
            if (gNhomthamso == null)
                throw new ArgumentNullException(nameof(gNhomthamso));

            await _gNhomthamsoDAO.InsertGNhomthamso(gNhomthamso);
        }

        /// <summary>
        /// Update GNhomthamso
        /// </summary>
        /// <param name="gNhomthamso"></param>
        /// <returns></returns>
        public async Task UpdateGNhomthamso(GNhomthamso gNhomthamso, GNhomthamso entity)
        {
            if (gNhomthamso == null)
                throw new ArgumentNullException(nameof(gNhomthamso));

            gNhomthamso.Tennhom = entity.Tennhom;
            gNhomthamso.Kyhieunhom = entity.Kyhieunhom;
            await _gNhomthamsoDAO.UpdateGNhomthamso(gNhomthamso);
        }

        /// <summary>
        /// Delete GNhomthamso
        /// </summary>
        /// <param name="gNhomthamso"></param>
        /// <returns></returns>
        public async Task DeleteGNhomthamso(GNhomthamso gNhomthamso)
        {
            if (gNhomthamso == null)
                throw new ArgumentNullException(nameof(gNhomthamso));

            await _gNhomthamsoDAO.DeleteGNhomthamso(gNhomthamso);
        }

        /// <summary>
        ///  Check tên nhóm
        /// </summary>
        /// <param name="id"></param>
        /// <param name="tennhom"></param>
        /// <returns></returns>
        public async Task<bool> CheckExistName(int? id, string tennhom)
        {
            if (string.IsNullOrEmpty(tennhom))
                return false;

            bool kq = false;
            var model = new GNhomthamso();
            if (id != null && id > 0)
            {
                model = await _gNhomthamsoDAO.Get(a => a.Id == id &&
                                                 StringExtensions.ConvertToUnSign(a.Tennhom.Trim().ToLower())
                                                .Contains(StringExtensions.ConvertToUnSign(tennhom.Trim().ToLower())));
                if (model != null)
                    kq = true;
            }
            else
            {
                model = await _gNhomthamsoDAO.Get(a => StringExtensions.ConvertToUnSign(a.Tennhom.Trim().ToLower())
                                                      .Contains(StringExtensions.ConvertToUnSign(tennhom.Trim().ToLower())));
                if (model != null)
                    kq = true;
            }

            return kq;
        }

        /// <summary>
        ///  Check exist ký hiệu nhóm
        /// </summary>
        /// <param name="id"></param>
        /// <param name="tennhom"></param>
        /// <returns></returns>
        public async Task<bool> CheckExistSymbol(int? id, string kyhieunhom)
        {
            if (string.IsNullOrEmpty(kyhieunhom))
                return false;

            bool kq = false;
            var model = new GNhomthamso();
            if (id != null && id > 0)
            {
                model = await _gNhomthamsoDAO.Get(a => a.Id == id &&
                                                 StringExtensions.ConvertToUnSign(a.Kyhieunhom.Trim().ToLower())
                                                .Contains(StringExtensions.ConvertToUnSign(kyhieunhom.Trim().ToLower())));
                if (model != null)
                    kq = true;
            }
            else
            {
                model = await _gNhomthamsoDAO.Get(a => StringExtensions.ConvertToUnSign(a.Kyhieunhom.Trim().ToLower())
                                                      .Contains(StringExtensions.ConvertToUnSign(kyhieunhom.Trim().ToLower())));
                if (model != null)
                    kq = true;
            }

            return kq;
        }

        #endregion
    }
}
