﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using System;
using System.Threading.Tasks;

namespace WaterManagement.Business.Business
{
    public class GTieuchuanchatluongBusiness
    {
        #region Fields

        private readonly IGTieuchuanchatluongDAO _gTieuchuanchatluongDAO;

        #endregion

        #region Ctor
        public GTieuchuanchatluongBusiness(IGTieuchuanchatluongDAO gTieuchuanchatluongDAO)
        {
            _gTieuchuanchatluongDAO = gTieuchuanchatluongDAO;
        }

        #endregion

        #region Method
        /// <summary>
        /// insert
        /// </summary>
        /// <param name="gTieuchuanchatluong"></param>
        /// <returns></returns>
        public async Task InsertGTieuchuanchatluong(GTieuchuanchatluong gTieuchuanchatluong)
        {
            if (gTieuchuanchatluong == null)
                throw new ArgumentNullException(nameof(gTieuchuanchatluong));

            await _gTieuchuanchatluongDAO.InsertGTieuchuanchatluong(gTieuchuanchatluong);

        }
        /// <summary>
        /// update
        /// </summary>
        /// <param name="gTieuchuanchatluong"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task UpdateGTieuchuanchatluong(GTieuchuanchatluong gTieuchuanchatluong, GTieuchuanchatluong entity)
        {
            if (gTieuchuanchatluong == null)
                throw new ArgumentNullException(nameof(gTieuchuanchatluong));

            gTieuchuanchatluong.IdTieuchuan = entity.IdTieuchuan;
            gTieuchuanchatluong.IdThamso = entity.IdThamso;
            gTieuchuanchatluong.IdDonvido = entity.IdDonvido;
            gTieuchuanchatluong.Gioihantren = entity.Gioihantren;
            gTieuchuanchatluong.Gioihanduoi = entity.Gioihanduoi;
            gTieuchuanchatluong.Phuongphapthu = entity.Phuongphapthu;
            gTieuchuanchatluong.Captieuchuan = entity.Captieuchuan;
            gTieuchuanchatluong.Mucdogiamsat = entity.Mucdogiamsat;

            await _gTieuchuanchatluongDAO.UpdateGTieuchuanchatluong(gTieuchuanchatluong);
        }
        /// <summary>
        /// delete
        /// </summary>
        /// <param name="gTieuchuanchatluong"></param>
        /// <returns></returns>
        public async Task DeleteGTieuchuanchatluong(GTieuchuanchatluong gTieuchuanchatluong)
        {
            if (gTieuchuanchatluong == null)
                throw new ArgumentNullException(nameof(gTieuchuanchatluong));

            await _gTieuchuanchatluongDAO.DeleteGTieuchuanchatluong(gTieuchuanchatluong);
        }

        #endregion
    }
}
