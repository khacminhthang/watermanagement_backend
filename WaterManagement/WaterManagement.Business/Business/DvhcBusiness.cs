﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using WaterManagement.Utils.Common;
using WaterManagement.Utils.Common.Extensions;
using System;
using System.Threading.Tasks;
using static WaterManagement.Business.Models.Dvhc.DvhcModel;

namespace WaterManagement.Business.Business
{
    public class DvhcBusiness
    {
        #region Fields

        private readonly IDvhcDAO _dvhcDAO;

        #endregion

        #region Ctor

        public DvhcBusiness(IDvhcDAO dvhcDAO)
        {
            _dvhcDAO = dvhcDAO;
        }

        #endregion

        #region Method

        /// <summary>
        /// Insert dvhc province
        /// </summary>
        /// <param name="dvhc"></param>
        /// <returns></returns>
        public async Task InsertProvince(InputProvinceModel model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            var dvhc = new Dvhc();
            dvhc.Tendvhc = model.Name;
            dvhc.Matinh = model.ProvinceCode;
            dvhc.Mahuyen = AppConstants.mahuyen;
            dvhc.Maxa = AppConstants.maxa;
            dvhc.Status = 1;
            await _dvhcDAO.InsertDvhc(dvhc);
        }

        /// <summary>
        ///  Update province
        /// </summary>
        /// <param name="dvhc"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task UpdateProvince(Dvhc dvhc, InputProvinceModel entity)
        {
            if (dvhc == null)
                throw new ArgumentNullException(nameof(dvhc));

            dvhc.Tendvhc = entity.Name;
            dvhc.Matinh = entity.ProvinceCode;
            await _dvhcDAO.UpdateDvhc(dvhc);
        }

        /// <summary>
        /// Insert District
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task InsertDistrict(InputDistrictModel model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            var dvhc = new Dvhc();
            dvhc.Tendvhc = model.Name;
            dvhc.Parentid = model.Id;
            dvhc.Matinh = model.ProvinceCode;
            dvhc.Mahuyen = model.DistrictCode;
            dvhc.Status = 1;
            dvhc.Maxa = AppConstants.maxa;
            await _dvhcDAO.InsertDvhc(dvhc);
        }

        /// <summary>
        /// Update district
        /// </summary>
        /// <param name="dvhc"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task UpdateDistrict(Dvhc dvhc, InputDistrictModel entity)
        {
            if (dvhc == null)
                throw new ArgumentNullException(nameof(dvhc));

            dvhc.Tendvhc = entity.Name;
            dvhc.Mahuyen = entity.DistrictCode;
            await _dvhcDAO.UpdateDvhc(dvhc);
        }

        /// <summary>
        /// insert ward 
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public async Task InsertWard(InputWardModel model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            var dvhc = new Dvhc();
            dvhc.Tendvhc = model.Name;
            dvhc.Parentid = model.Id;
            dvhc.Matinh = model.ProvinceCode;
            dvhc.Mahuyen = model.DistrictCode;
            dvhc.Maxa = model.WardCode;
            dvhc.Status = 1;
            await _dvhcDAO.InsertDvhc(dvhc);
        }

        /// <summary>
        /// Update ward
        /// </summary>
        /// <param name="dvhc"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task UpdateWard(Dvhc dvhc, InputWardModel entity)
        {
            if (dvhc == null)
                throw new ArgumentNullException(nameof(dvhc));

            dvhc.Tendvhc = entity.Name;
            dvhc.Maxa = entity.WardCode;
            await _dvhcDAO.UpdateDvhc(dvhc);
        }

        /// <summary>
        ///  Check exist ten dvhc
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<bool> CheckExistProvinceName(string provinceName, Dvhc dvhc = null)
        {
            if (string.IsNullOrEmpty(provinceName))
                return false;
            //
            var kq = false;
            var dvhcObj = new Dvhc();
            if (dvhc != null)
            {
                dvhcObj = await _dvhcDAO.Get(a => StringExtensions.ConvertToUnSign(a.Tendvhc.Trim().ToLower()) ==
                                                StringExtensions.ConvertToUnSign(provinceName.Trim().ToLower())
                                              && a.Parentid == null
                                              && a.Status == 1
                                              && a.Id != dvhc.Id);

                if (dvhcObj != null)
                    kq = true;
            }
            else
            {
                dvhcObj = await _dvhcDAO.Get(a => StringExtensions.ConvertToUnSign(a.Tendvhc.Trim().ToLower()) ==
                                           StringExtensions.ConvertToUnSign(provinceName.Trim().ToLower())
                                            && a.Parentid == null
                                            && a.Status == 1);

                if (dvhcObj != null)
                    kq = true;

            }

            return kq;
        }

        /// <summary>
        /// Check exist matinh
        /// </summary>
        /// <param name="provinceId"></param>
        /// <returns></returns>
        public async Task<bool> CheckExistProvinceCode(string provinceCode, Dvhc dvhc = null)
        {
            if (string.IsNullOrEmpty(provinceCode))
                return false;
            //
            var kq = false;
            var dvhcObj = new Dvhc();
            if (dvhc != null)
            {
                dvhcObj = await _dvhcDAO.Get(a => a.Matinh == provinceCode
                                            && a.Maxa == AppConstants.maxa
                                            && a.Mahuyen == AppConstants.mahuyen
                                            && a.Status == 1
                                            && a.Id != dvhc.Id
                                            && a.Parentid == null);

                if (dvhcObj != null)
                    kq = true;
            }
            else
            {
                dvhcObj = await _dvhcDAO.Get(a => a.Matinh == provinceCode
                                             && a.Maxa == AppConstants.maxa
                                             && a.Mahuyen == AppConstants.mahuyen
                                             && a.Status == 1
                                             && a.Parentid == null);

                if (dvhcObj != null)
                    kq = true;
            }

            return kq;
        }

        /// <summary>
        /// Check exist district name
        /// </summary>
        /// <param name="districtName"></param>
        /// <param name="dvhc"></param>
        /// <returns></returns>
        public async Task<bool> CheckExistDistrictName(int provinceId, string districtName, Dvhc dvhc = null)
        {
            if (string.IsNullOrEmpty(districtName))
                return false;
            //
            var kq = false;
            var dvhcObj = new Dvhc();
            if (dvhc != null)
            {
                dvhcObj = await _dvhcDAO.Get(a => StringExtensions.ConvertToUnSign(a.Tendvhc.Trim().ToLower()) ==
                                                StringExtensions.ConvertToUnSign(districtName.Trim().ToLower())
                                              && a.Parentid == provinceId
                                              && a.Status == 1
                                              && a.Id != dvhc.Id);

                if (dvhcObj != null)
                    kq = true;
            }
            else
            {
                dvhcObj = await _dvhcDAO.Get(a => StringExtensions.ConvertToUnSign(a.Tendvhc.Trim().ToLower()) ==
                                                  StringExtensions.ConvertToUnSign(districtName.Trim().ToLower())
                                            && a.Parentid == provinceId
                                            && a.Status == 1);

                if (dvhcObj != null)
                    kq = true;

            }

            return kq;
        }

        /// <summary>
        /// check exist 
        /// </summary>
        /// <param name="provinceId"></param>
        /// <param name="districtCode"></param>
        /// <param name="dvhc"></param>
        /// <returns></returns>
        public async Task<bool> CheckExistDistrictCode(int provinceId, string districtCode, Dvhc dvhc = null)
        {
            if (string.IsNullOrEmpty(districtCode))
                return false;
            //
            var kq = false;
            var dvhcObj = new Dvhc();
            if (dvhc != null)
            {
                dvhcObj = await _dvhcDAO.Get(a => a.Mahuyen == districtCode
                                            && a.Maxa == AppConstants.maxa
                                            && a.Status == 1
                                            && a.Id != dvhc.Id
                                            && a.Parentid == provinceId);

                if (dvhcObj != null)
                    kq = true;
            }
            else
            {
                dvhcObj = await _dvhcDAO.Get(a => a.Mahuyen == districtCode
                                             && a.Maxa == AppConstants.maxa
                                             && a.Status == 1
                                             && a.Parentid == provinceId);

                if (dvhcObj != null)
                    kq = true;
            }

            return kq;
        }

        /// <summary>
        ///  check exist ward name
        /// </summary>
        /// <param name="districtId"></param>
        /// <param name="wardName"></param>
        /// <param name="dvhc"></param>
        /// <returns></returns>
        public async Task<bool> CheckExistWardName(int districtId, string wardName, Dvhc dvhc = null)
        {
            if (string.IsNullOrEmpty(wardName))
                return false;
            //
            var kq = false;
            var dvhcObj = new Dvhc();
            if (dvhc != null)
            {
                dvhcObj = await _dvhcDAO.Get(a => StringExtensions.ConvertToUnSign(a.Tendvhc.Trim().ToLower()) ==
                                                StringExtensions.ConvertToUnSign(wardName.Trim().ToLower())
                                              && a.Parentid == districtId
                                              && a.Status == 1
                                              && a.Id != dvhc.Id);

                if (dvhcObj != null)
                    kq = true;
            }
            else
            {
                dvhcObj = await _dvhcDAO.Get(a => StringExtensions.ConvertToUnSign(a.Tendvhc.Trim().ToLower()) ==
                                                  StringExtensions.ConvertToUnSign(wardName.Trim().ToLower())
                                            && a.Parentid == districtId
                                            && a.Status == 1);

                if (dvhcObj != null)
                    kq = true;

            }

            return kq;
        }

        /// <summary>
        /// check exist ward code
        /// </summary>
        /// <param name="districtId"></param>
        /// <param name="wardCode"></param>
        /// <param name="dvhc"></param>
        /// <returns></returns>
        public async Task<bool> CheckExistWardCode(int districtId, string wardCode, Dvhc dvhc = null)
        {
            if (string.IsNullOrEmpty(wardCode))
                return false;
            //
            var kq = false;
            var dvhcObj = new Dvhc();
            if (dvhc != null)
            {
                dvhcObj = await _dvhcDAO.Get(a => a.Maxa == wardCode
                                            && a.Status == 1
                                            && a.Id != dvhc.Id
                                            && a.Parentid == districtId);

                if (dvhcObj != null)
                    kq = true;
            }
            else
            {
                dvhcObj = await _dvhcDAO.Get(a => a.Maxa == wardCode
                                             && a.Maxa == AppConstants.maxa
                                             && a.Status == 1
                                             && a.Parentid == districtId);

                if (dvhcObj != null)
                    kq = true;
            }

            return kq;
        }

        /// <summary>
        /// DeleteProvince
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteProvince(int id)
        {
            var province = await _dvhcDAO.Get(a => a.Id == id);

            var deleteModel = await _dvhcDAO.GetAll(a => a.Matinh == province.Matinh);
            // delete

            await _dvhcDAO.DeleteRange(deleteModel);
        }

        /// <summary>
        /// Delete District
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteDistrict(int id)
        {
            var district = await _dvhcDAO.Get(a => a.Id == id);

            var deleteModel = await _dvhcDAO.GetAll(a => a.Mahuyen == district.Mahuyen);
            // delete

            await _dvhcDAO.DeleteRange(deleteModel);
        }

        /// <summary>
        /// DeleteWard
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteWard(int id)
        {
            var ward = await _dvhcDAO.Get(a => a.Id == id);

            // delete
            await _dvhcDAO.DeleteDvhc(ward);
        }
        #endregion
    }
}
