﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using System;
using System.Threading.Tasks;

namespace WaterManagement.Business.Business
{
    public class GCongtyBusiness
    {
        #region Fields

        private readonly IGCongtyDAO _gCongtyDAO;

        #endregion

        #region Ctor

        public GCongtyBusiness(IGCongtyDAO gCongtyDAO)
        {
            _gCongtyDAO = gCongtyDAO;
        }

        #endregion

        #region Method

        /// <summary>
        /// Insert GCongty
        /// </summary>
        /// <param name="gCongty"></param>
        /// <returns></returns>
        public async Task InsertGCongty(GCongty gCongty)
        {
            if (gCongty == null)
                throw new ArgumentNullException(nameof(gCongty));

            gCongty.CreatedAt = DateTime.Now.ToString();
            gCongty.DeletedAt = 0;
            await _gCongtyDAO.InsertGCongty(gCongty);

        }

        /// <summary>
        /// Update GCongty
        /// </summary>
        /// <param name="gCongty"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task UpdateGCongty(GCongty gCongty, GCongty entity)
        {
            if (gCongty == null)
                throw new ArgumentNullException(nameof(gCongty));

            gCongty.Tencongty = entity.Tencongty;
            gCongty.Diachi = entity.Diachi;
            gCongty.Matinh = entity.Matinh;
            gCongty.Mahuyen = entity.Mahuyen;
            gCongty.Maxa = entity.Maxa;
            gCongty.Sodienthoai = entity.Sodienthoai;
            gCongty.Email = entity.Email;
            gCongty.Note = entity.Note;
            gCongty.ImgLink = entity.ImgLink;
            gCongty.UpdatedAt = DateTime.Now.ToString();
            await _gCongtyDAO.UpdateGCongty(gCongty);
        }

        /// <summary>
        /// Delete GCongty
        /// </summary>
        /// <param name="gCongty"></param>
        /// <returns></returns>
        public async Task DeleteGCongty(GCongty gCongty)
        {
            if (gCongty == null)
                throw new ArgumentNullException(nameof(gCongty));

            gCongty.UpdatedAt = DateTime.Now.ToString();
            gCongty.DeletedAt = 1;
            await _gCongtyDAO.DeleteGCongty(gCongty);
        }

        #endregion
    }
}
