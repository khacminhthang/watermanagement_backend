﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using System;
using System.Threading.Tasks;

namespace WaterManagement.Business.Business
{
    public class GLoaisolieuBusiness
    {
        #region Fields
        private readonly IGLoaisolieuDAO _gLoaisolieuDAO;

        #endregion

        #region Ctor
        /// <summary>
        /// contructor
        /// </summary>
        /// <param name="gLoaisolieuDAO"></param>
        public GLoaisolieuBusiness(IGLoaisolieuDAO gLoaisolieuDAO)
        {
            _gLoaisolieuDAO = gLoaisolieuDAO;
        }

        #endregion

        #region Method
        /// <summary>
        /// insert
        /// </summary>
        /// <param name="gLoaisolieu"></param>
        /// <returns></returns>
        public async Task InsertGLoaisolieu(GLoaisolieu gLoaisolieu)
        {
            if (gLoaisolieu == null)
                throw new ArgumentNullException(nameof(gLoaisolieu));

            await _gLoaisolieuDAO.InsertGLoaisolieu(gLoaisolieu);
        }
        /// <summary>
        /// update
        /// </summary>
        /// <param name="gLoaisolieu"></param>
        /// <returns></returns>
        public async Task UpdateGLoaisolieu(GLoaisolieu gLoaisolieu, GLoaisolieu entity)
        {
            if (gLoaisolieu == null)
                throw new ArgumentNullException(nameof(gLoaisolieu));

            gLoaisolieu.Tenloaisolieu = entity.Tenloaisolieu;
            gLoaisolieu.Tenviettat = entity.Tenviettat;

            await _gLoaisolieuDAO.UpdateGLoaisolieu(gLoaisolieu);
        }
        /// <summary>
        /// delete
        /// </summary>
        /// <param name="gLoaisolieu"></param>
        /// <returns></returns>
        public async Task DeleteGLoaisolieu(GLoaisolieu gLoaisolieu)
        {
            if (gLoaisolieu == null)
                throw new ArgumentNullException(nameof(gLoaisolieu));

            await _gLoaisolieuDAO.DeleteGLoaisolieu(gLoaisolieu);
        }

        #endregion
    }
}
