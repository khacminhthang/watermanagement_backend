﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using System;
using System.Threading.Tasks;

namespace WaterManagement.Business.Business
{
    public class GTieuchuanBusiness
    {
        #region Fields

        private readonly IGTieuchuanDAO _gTieuchuanDAO;

        #endregion

        #region Ctor
        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="gTieuchuanDAO"></param>
        public GTieuchuanBusiness(IGTieuchuanDAO gTieuchuanDAO)
        {
            _gTieuchuanDAO = gTieuchuanDAO;
        }

        #endregion

        #region Method

        /// <summary>
        /// insert
        /// </summary>
        /// <param name="gTieuchuan"></param>
        /// <returns></returns>
        public async Task InsertGTieuchuan(GTieuchuan gTieuchuan)
        {
            if (gTieuchuan == null)
                throw new ArgumentNullException(nameof(gTieuchuan));

            await _gTieuchuanDAO.InsertGTieuChuan(gTieuchuan);

        }

        /// <summary>
        /// update
        /// </summary>
        /// <param name="gTieuchuan"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task UpdateGTieuchuan(GTieuchuan gTieuchuan, GTieuchuan entity)
        {
            if (gTieuchuan == null)
                throw new ArgumentNullException(nameof(gTieuchuan));

            gTieuchuan.Tentieuchuan = entity.Tentieuchuan;
            gTieuchuan.Sohieutieuchuan = entity.Sohieutieuchuan;
            gTieuchuan.Thoigianbanhanh = entity.Thoigianbanhanh;
            gTieuchuan.Coquanbanhanh = entity.Coquanbanhanh;
            gTieuchuan.Hientrang = entity.Hientrang;
            gTieuchuan.IdTailieu = entity.IdTailieu;

            await _gTieuchuanDAO.UpdateGTieuChuan(gTieuchuan);
        }
        /// <summary>
        /// delete
        /// </summary>
        /// <param name="gTieuchuan"></param>
        /// <returns></returns>
        public async Task DeleteGTieuchuan(GTieuchuan gTieuchuan)
        {
            if (gTieuchuan == null)
                throw new ArgumentNullException(nameof(gTieuchuan));

            await _gTieuchuanDAO.DeleteGTieuChuan(gTieuchuan);
        }

        #endregion
    }
}

