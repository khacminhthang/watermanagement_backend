﻿using System;
using System.Threading.Tasks;
using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;
using WaterManagement.Utils.Common;
using WaterManagement.Utils.Common.Helpers;

namespace WaterManagement.Business.Business
{
    public class SwMaybomdiemkhaithacBusiness
    {
        #region Fields
        private readonly ISwMaybomdiemkhaithacDAO _swMaybomdiemkhaithacDAO;
        #endregion

        #region Ctor
        public SwMaybomdiemkhaithacBusiness(ISwMaybomdiemkhaithacDAO swMaybomdiemkhaithacDAO)
        {
            _swMaybomdiemkhaithacDAO = swMaybomdiemkhaithacDAO;
        }
        #endregion

        #region Methods
        public async Task InsertMaybomdiemkhaithac(SwMaybomdiemkhaithac swMaybomdiemkhaithac)
        {
            if (swMaybomdiemkhaithac == null)
                throw new ArgumentNullException(nameof(swMaybomdiemkhaithac));
            swMaybomdiemkhaithac.CreatedAt = DateTime.Now.ToString();
            await _swMaybomdiemkhaithacDAO.InsertSwMaybomdiemkhaithac(swMaybomdiemkhaithac);
        }

        public async Task UpdateMaybomdiemkhaithac(SwMaybomdiemkhaithac swMaybomdiemkhaithac, SwMaybomdiemkhaithac model)
        {
            if (swMaybomdiemkhaithac == null)
                throw new ArgumentNullException(nameof(swMaybomdiemkhaithac));
            swMaybomdiemkhaithac.IdDiemkhaithac = model.IdDiemkhaithac;
            swMaybomdiemkhaithac.Loaimaybom = model.Loaimaybom;
            swMaybomdiemkhaithac.Dosaulapdat = model.Dosaulapdat;
            swMaybomdiemkhaithac.Thoigianlapdat = model.Thoigianlapdat;
            swMaybomdiemkhaithac.Congsuat = model.Congsuat;
            swMaybomdiemkhaithac.Note = model.Note;
            swMaybomdiemkhaithac.UpdatedAt = DateTime.Now.ToString();
            await _swMaybomdiemkhaithacDAO.UpdateSwMaybomdiemkhaithac(swMaybomdiemkhaithac);
        }

        public async Task DeleteMaybomdiemkhaithac(SwMaybomdiemkhaithac swMaybomdiemkhaithac)
        {
            if (swMaybomdiemkhaithac == null)
                throw new ArgumentNullException(nameof(swMaybomdiemkhaithac));
            swMaybomdiemkhaithac.UpdatedAt = DateTime.Now.ToString();
            swMaybomdiemkhaithac.DeletedAt = 1;
            await _swMaybomdiemkhaithacDAO.DeleteSwMaybomdiemkhaithac(swMaybomdiemkhaithac);
        }
        #endregion
    }
}
