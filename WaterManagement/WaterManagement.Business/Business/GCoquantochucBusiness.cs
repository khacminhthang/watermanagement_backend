﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using System;
using System.Threading.Tasks;

namespace WaterManagement.Business.Business
{
    public class GCoquantochucBusiness
    {
        #region Fields

        private readonly IGCoquantochucDAO _gCoquantochucDAO;

        #endregion

        #region Ctor

        public GCoquantochucBusiness(IGCoquantochucDAO gCoquantochucDAO)
        {
            _gCoquantochucDAO = gCoquantochucDAO;
        }

        #endregion

        #region Method

        /// <summary>
        /// Insert GCoquantochuc
        /// </summary>
        /// <param name="gCoquantochuc"></param>
        /// <returns></returns>
        public async Task InsertGCoquantochuc(GCoquantochuc gCoquantochuc)
        {
            if (gCoquantochuc == null)
                throw new ArgumentNullException(nameof(gCoquantochuc));

            gCoquantochuc.CreatedAt = DateTime.Now.ToString();
            gCoquantochuc.DeletedAt = 0;
            await _gCoquantochucDAO.InsertGCoquantochuc(gCoquantochuc);
        }

        /// <summary>
        /// Update GCoquantochuc
        /// </summary>
        /// <param name="gCoquantochuc"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task UpdateGCoquantochuc(GCoquantochuc gCoquantochuc, GCoquantochuc entity)
        {
            if (gCoquantochuc == null)
                throw new ArgumentNullException(nameof(gCoquantochuc));

            gCoquantochuc.Idcha = entity.Idcha;
            gCoquantochuc.Tencoquan = entity.Tencoquan;
            gCoquantochuc.Diachi = entity.Diachi;
            gCoquantochuc.Sodienthoai = entity.Sodienthoai;
            gCoquantochuc.Email = entity.Email;
            gCoquantochuc.Matinh = entity.Matinh;
            gCoquantochuc.Mahuyen = entity.Mahuyen;
            gCoquantochuc.Maxa = entity.Maxa;
            gCoquantochuc.Note = entity.Note;
            gCoquantochuc.ImgLink = entity.ImgLink;
            gCoquantochuc.UpdatedAt = DateTime.Now.ToString();
            await _gCoquantochucDAO.UpdateGCoquantochuc(gCoquantochuc);
        }

        /// <summary>
        /// Delete GCoquantochuc
        /// </summary>
        /// <param name="gCoquantochuc"></param>
        /// <returns></returns>
        public async Task DeleteGCoquantochuc(GCoquantochuc gCoquantochuc)
        {
            if (gCoquantochuc == null)
                throw new ArgumentNullException(nameof(gCoquantochuc));

            gCoquantochuc.UpdatedAt = DateTime.Now.ToString();
            gCoquantochuc.DeletedAt = 1;
            await _gCoquantochucDAO.DeleteGCoquantochuc(gCoquantochuc);

        }

        #endregion
    }
}
