﻿using System;
using System.Threading.Tasks;
using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;
using WaterManagement.Utils.Common;
using WaterManagement.Utils.Common.Helpers;

namespace WaterManagement.Business.Business
{
    public class SwTrambomBusiness
    {
        #region Fields
        private readonly ISwTrambomDAO _swTrambomDAO;
        private readonly IGenerateLocationDAO _generateLocationDAO;
        #endregion

        #region ctor
        public SwTrambomBusiness(ISwTrambomDAO swTrambomDAO, IGenerateLocationDAO generateLocationDAO)
        {
            _swTrambomDAO = swTrambomDAO;
            _generateLocationDAO = generateLocationDAO;
        }
        #endregion

        #region Method
        /// <summary>
        /// insert
        /// </summary>
        /// <param name="swTrambom"></param>
        /// <returns></returns>
         public async Task InsertTrambom(SwTrambom swTrambom)
        {
            if (swTrambom == null)
                throw new ArgumentNullException(nameof(swTrambom));
            swTrambom.Geom = _generateLocationDAO.GenerateLocation(swTrambom.Toadox.Value, swTrambom.Toadoy.Value,swTrambom.Srid.Value).Geometry;
            swTrambom.CreatedAt = DateTime.Now.ToString();
            await _swTrambomDAO.InsertSwTrambom(swTrambom);
        }


        /// <summary>
        /// update
        /// </summary>
        /// <param name="swTrambom"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task UpdateTrambom(SwTrambom swTrambom, SwTrambom model)
        {
            if (swTrambom == null)
                throw new ArgumentNullException(nameof(swTrambom));
            swTrambom.Geom = _generateLocationDAO.GenerateLocation(model.Toadox.Value, model.Toadoy.Value, model.Srid.Value)
                .Geometry;
            swTrambom.Sohieutrambom = model.Sohieutrambom;
            swTrambom.Tentrambom = model.Tentrambom;
            swTrambom.Toadox = model.Toadox;
            swTrambom.Toadoy = model.Toadoy;
            swTrambom.Caodoz = model.Caodoz;
            swTrambom.Srid = model.Srid;
            swTrambom.Vitri = model.Vitri;
            swTrambom.Matinh = model.Matinh;
            swTrambom.Mahuyen = model.Mahuyen;
            swTrambom.Maxa = model.Maxa;
            swTrambom.Thoigianxaydung = model.Thoigianxaydung;
            swTrambom.Thoigianvanhanh = model.Thoigianvanhanh;
            swTrambom.Somaybomtieunuoc = model.Somaybomtieunuoc;
            swTrambom.Somaybomtuoinuoc = model.Somaybomtuoinuoc;
            swTrambom.Khanangbomtuoi = model.Khanangbomtuoi;
            swTrambom.Luuluongtuoithietke = model.Luuluongtuoithietke;
            swTrambom.Luuluongtuoithucte = model.Luuluongtuoithucte;
            swTrambom.Dientichtuoithietke = model.Dientichtuoithietke;
            swTrambom.Dientichtuoithucte = model.Dientichtuoithucte;
            swTrambom.Khanangbomtieu = model.Khanangbomtieu;
            swTrambom.Luuluongtieuthietke = model.Luuluongtieuthietke;
            swTrambom.Luuluongtieuthucte = model.Luuluongtieuthucte;
            swTrambom.Dientichtieuthietke = model.Dientichtieuthietke;
            swTrambom.Dientichtieuthucte = model.Dientichtieuthucte;
            swTrambom.IdVanhanh = model.IdVanhanh;
            swTrambom.Sohothietke = model.Sohothietke;
            swTrambom.Sohothucte = model.Sohothucte;
            swTrambom.Phantramcongsuathuuich = model.Phantramcongsuathuuich;
            swTrambom.IdSong = model.IdSong;
            swTrambom.IdHochua = model.IdHochua;
            swTrambom.IdHotunhien = model.IdHotunhien;
            swTrambom.Hientrang = model.Hientrang;
            swTrambom.Note = model.Note;
            swTrambom.ImgLink = model.ImgLink;
            swTrambom.UpdatedAt = DateTime.Now.ToString();
            var result = await _swTrambomDAO.UpdateSwTranbom(swTrambom);
        }

        public async Task DeleteTrambom(SwTrambom swTrambom)
        {
            if (swTrambom == null)
                throw new ArgumentNullException(nameof(swTrambom));
            swTrambom.UpdatedAt = DateTime.Now.ToString();
            swTrambom.DeletedAt = 1;
            var result = await _swTrambomDAO.DeleteSwTrambom(swTrambom);
        }
        #endregion

        /// <summary>
        /// Get all
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        public async Task<PagedList<SwTrambomViewModel>> GetAll(int pageNumber, int pageSize)
        {
            var data = await _swTrambomDAO.GetAll();

            return new PagedList<SwTrambomViewModel>(data, pageNumber, pageSize);
        }
    }
}
