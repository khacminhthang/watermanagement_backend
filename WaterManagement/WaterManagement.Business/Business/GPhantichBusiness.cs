﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using System;
using System.Threading.Tasks;

namespace WaterManagement.Business.Business
{
    public class GPhantichBusiness
    {
        #region Fields

        private readonly IGPhantichDAO _gPhantichDAO;

        #endregion

        #region Ctor

        public GPhantichBusiness(IGPhantichDAO gPhantichDAO)
        {
            _gPhantichDAO = gPhantichDAO;
        }

        #endregion

        #region Method

        /// <summary>
        /// Insert GPhantich
        /// </summary>
        /// <param name="gPhantich"></param>
        /// <returns></returns>
        public async Task InsertGPhantich(GPhantich gPhantich)
        {
            if (gPhantich == null)
                throw new ArgumentNullException(nameof(gPhantich));

            gPhantich.CreatedAt = DateTime.Now.ToString();
            await _gPhantichDAO.InsertGPhantich(gPhantich);
        }

        /// <summary>
        /// Update GPhantich
        /// </summary>
        /// <param name="gPhantich"></param>
        /// <returns></returns>
        public async Task UpdateGPhantich(GPhantich gPhantich, GPhantich entity)
        {
            if (gPhantich == null)
                throw new ArgumentNullException(nameof(gPhantich));

            gPhantich.IdMau = entity.IdMau;
            gPhantich.Sohieuphantich = entity.Sohieuphantich;
            gPhantich.Nguoiphantich = entity.Nguoiphantich;
            gPhantich.Nguoikiemtra = entity.Nguoikiemtra;
            gPhantich.Thoigianphantich = entity.Thoigianphantich;
            gPhantich.Note = entity.Note;
            gPhantich.UpdatedAt = DateTime.Now.ToString();
            await _gPhantichDAO.UpdateGPhantich(gPhantich);
        }

        /// <summary>
        /// Delete GPhantich
        /// </summary>
        /// <param name="gPhantich"></param>
        /// <returns></returns>
        public async Task DeleteGPhantich(GPhantich gPhantich)
        {
            if (gPhantich == null)
                throw new ArgumentNullException(nameof(gPhantich));

            gPhantich.DeletedAt = 1;
            gPhantich.UpdatedAt = DateTime.Now.ToString();
            await _gPhantichDAO.DeleteGPhantich(gPhantich);
        }

        #endregion
    }
}
