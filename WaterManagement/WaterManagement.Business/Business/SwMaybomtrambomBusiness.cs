﻿using System;
using System.Threading.Tasks;
using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;
using WaterManagement.Utils.Common;
using WaterManagement.Utils.Common.Helpers;

namespace WaterManagement.Business.Business
{
    public class SwMaybomtrambomBusiness
    {
        private readonly ISwMaybomtrambomDAO _swMaybomtrambomDAO;

        public SwMaybomtrambomBusiness(ISwMaybomtrambomDAO swMaybomtrambomDAO)
        {
            _swMaybomtrambomDAO = swMaybomtrambomDAO;
        }

        public async Task InsertMaybomtrambom(SwMaybomtrambom swMaybomtrambom)
        {
            if (swMaybomtrambom == null)
                throw new ArgumentNullException(nameof(swMaybomtrambom));
            swMaybomtrambom.CreatedAt = DateTime.Now.ToString();
            await _swMaybomtrambomDAO.InsertSwMaybomtrambom(swMaybomtrambom);
        }

        public async Task UpdateMaybomtrambom(SwMaybomtrambom swMaybomtrambom, SwMaybomtrambom model)
        {
            if (swMaybomtrambom == null)
                throw new ArgumentNullException(nameof(swMaybomtrambom));
            swMaybomtrambom.IdTrambom = model.IdTrambom;
            swMaybomtrambom.Loaimaybom = model.Loaimaybom;
            swMaybomtrambom.Thoigianlapdat = model.Thoigianlapdat;
            swMaybomtrambom.Congsuat = model.Congsuat;
            swMaybomtrambom.Note = model.Note;
            swMaybomtrambom.UpdatedAt = DateTime.Now.ToString();
            await _swMaybomtrambomDAO.UpdateSwMaybomtrambom(swMaybomtrambom);
        }

        public async Task DeleteMaybomtrambom(SwMaybomtrambom swMaybomtrambom)
        {
            if (swMaybomtrambom == null)
                throw new ArgumentNullException(nameof(swMaybomtrambom));
            swMaybomtrambom.UpdatedAt = DateTime.Now.ToString();
            swMaybomtrambom.DeletedAt = 1;
            var result = await _swMaybomtrambomDAO.DeleteSwMaybomtrambom(swMaybomtrambom);
        }
    }
}
