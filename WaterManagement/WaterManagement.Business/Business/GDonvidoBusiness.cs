﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using System.Threading.Tasks;

namespace WaterManagement.Business.Business
{
    public class GDonvidoBusiness
    {
        private readonly IGDonvidoDAO _gDonvidoDAO;

        public GDonvidoBusiness(IGDonvidoDAO gDonvidoDAO)
        {
            _gDonvidoDAO = gDonvidoDAO;
        }


        public async Task Insert(GDonvido model)
        {

            await _gDonvidoDAO.Insert(model);
        }

    }
}
