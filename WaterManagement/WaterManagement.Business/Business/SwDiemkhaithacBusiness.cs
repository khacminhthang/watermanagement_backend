﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;
using WaterManagement.Utils.Common.Helpers;
using System;
using System.Threading.Tasks;

namespace WaterManagement.Business.Business
{
    public class SwDiemkhaithacBusiness
    {
        #region Fiedls

        private readonly ISwDiemkhaithacDAO _swDiemkhaithacDAO;
        private readonly IGenerateLocationDAO _generateLocationDAO;

        #endregion

        #region Ctor

        public SwDiemkhaithacBusiness(ISwDiemkhaithacDAO swDiemkhaithacDAO,
                                      IGenerateLocationDAO generateLocationDAO)
        {
            _swDiemkhaithacDAO = swDiemkhaithacDAO;
            _generateLocationDAO = generateLocationDAO;
        }

        #endregion

        #region Method

        /// <summary>
        /// Insert SwDiemkhaithac
        /// </summary>
        /// <param name="swDiemkhaithac"></param>
        /// <returns></returns>
        public async Task InsertSwDiemkhaithac(SwDiemkhaithac swDiemkhaithac)
        {
            if (swDiemkhaithac == null)
                throw new ArgumentNullException(nameof(swDiemkhaithac));

            swDiemkhaithac.Geom = _generateLocationDAO.GenerateLocation(swDiemkhaithac.Toadox.Value,
                                                             swDiemkhaithac.Toadoy.Value,
                                                             swDiemkhaithac.Srid.Value).Geometry;
            swDiemkhaithac.CreatedAt = DateTime.Now.ToString();
            await _swDiemkhaithacDAO.InsertSwDiemkhaithac(swDiemkhaithac);


        }

        /// <summary>
        /// Update SwDiemkhaithac
        /// </summary>
        /// <param name="swDiemkhaithac"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task UpdateSwDiemkhaithac(SwDiemkhaithac swDiemkhaithac, SwDiemkhaithac entity)
        {
            if (swDiemkhaithac == null)
                throw new ArgumentNullException(nameof(swDiemkhaithac));

            swDiemkhaithac.Geom = _generateLocationDAO.GenerateLocation(entity.Toadox.Value,
                                                               entity.Toadoy.Value,
                                                               entity.Srid.Value).Geometry;
            swDiemkhaithac.Sohieudiem = entity.Sohieudiem;
            swDiemkhaithac.Tendiem = entity.Tendiem;
            swDiemkhaithac.Toadox = entity.Toadox;
            swDiemkhaithac.Toadoy = entity.Toadoy;
            swDiemkhaithac.Caodoz = entity.Caodoz;
            swDiemkhaithac.Srid = entity.Srid;
            swDiemkhaithac.Vitri = entity.Vitri;
            swDiemkhaithac.Matinh = entity.Matinh;
            swDiemkhaithac.Mahuyen = entity.Mahuyen;
            swDiemkhaithac.Maxa = entity.Maxa;
            swDiemkhaithac.Thoigianxaydung = entity.Thoigianxaydung;
            swDiemkhaithac.Thoigianvanhanh = entity.Thoigianvanhanh;
            swDiemkhaithac.IdDonviquanly = entity.IdDonviquanly;
            swDiemkhaithac.IdChusudung = entity.IdChusudung;
            swDiemkhaithac.IdMucdichsudungchinh = entity.IdMucdichsudungchinh;
            swDiemkhaithac.Luuluongchopheplonnhat = entity.Luuluongchopheplonnhat;
            swDiemkhaithac.Cothietbigiamsat = entity.Cothietbigiamsat;
            swDiemkhaithac.IdVanhanh = entity.IdVanhanh;
            swDiemkhaithac.Thoiluongkhaithac = entity.Thoiluongkhaithac;
            swDiemkhaithac.Nhamay = entity.Nhamay;
            swDiemkhaithac.Dacapphep = entity.Dacapphep;
            swDiemkhaithac.Note = entity.Note;
            swDiemkhaithac.ImgLink = entity.ImgLink;
            swDiemkhaithac.ObjKey = entity.ObjKey;
            swDiemkhaithac.IdBody = entity.IdBody;
            swDiemkhaithac.BodyKey = entity.BodyKey;

            swDiemkhaithac.UpdatedAt = DateTime.Now.ToString();
            await _swDiemkhaithacDAO.UpdateSwDiemkhaithac(swDiemkhaithac);

        }

        /// <summary>
        /// Delete SwDiemkhaithac
        /// </summary>
        /// <param name="swDiemkhaithac"></param>
        /// <returns></returns>
        public async Task DeleteSwDiemkhaithac(SwDiemkhaithac swDiemkhaithac)
        {
            if (swDiemkhaithac == null)
                throw new ArgumentNullException(nameof(swDiemkhaithac));

            swDiemkhaithac.UpdatedAt = DateTime.Now.ToString();
            swDiemkhaithac.DeletedAt = 1;
            await _swDiemkhaithacDAO.DeleteSwDiemkhaithac(swDiemkhaithac);

        }

        /// <summary>
        /// Get all
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<PagedList<SwDiemkhaithacViewModel>> GetAll(int pageNumber, int pageSize)
        {
            var data = await _swDiemkhaithacDAO.GetAll();

            return new PagedList<SwDiemkhaithacViewModel>(data, pageNumber, pageSize);
        }

        #endregion
    }
}