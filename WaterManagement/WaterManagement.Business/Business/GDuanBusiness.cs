﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Models;
using System;
using System.Threading.Tasks;
namespace WaterManagement.Business.Business
{
    public class GDuanBusiness
    {
        #region Fields

        private readonly IGDuanDAO _gDuanDAO;

        #endregion

        #region Ctor

        public GDuanBusiness(IGDuanDAO gDuanDAO)
        {
            _gDuanDAO = gDuanDAO;
        }

        #endregion

        #region Method
        /// <summary>
        /// insert
        /// </summary>
        /// <param name="gDuan"></param>
        /// <returns></returns>
        public async Task InsertGDuan(GDuan gDuan)
        {
            if (gDuan == null)
                throw new ArgumentNullException(nameof(gDuan));

            await _gDuanDAO.InsertGDuan(gDuan);
        }
        /// <summary>
        /// update 
        /// </summary>
        /// <param name="gDuan"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task UpdateGDuan(GDuan gDuan, GDuan entity)
        {
            if (gDuan == null)
                throw new ArgumentNullException(nameof(gDuan));

            gDuan.ProjectCode = entity.ProjectCode;
            gDuan.ProjectName = entity.ProjectName;
            gDuan.SignedDate = entity.SignedDate;
            gDuan.SignedAgency = entity.SignedAgency;
            gDuan.CompleteDate = entity.CompleteDate;
            gDuan.ProjectLeader = entity.ProjectLeader;

            await _gDuanDAO.UpdateGDuan(gDuan);
        }
        /// <summary>
        /// delete
        /// </summary>
        /// <param name="gDuan"></param>
        /// <returns></returns>
        public async Task DeleteGDuan(GDuan gDuan)
        {
            if (gDuan == null)
                throw new ArgumentNullException(nameof(gDuan));

            await _gDuanDAO.DeleteGDuan(gDuan);
        }

        #endregion
    }
}
