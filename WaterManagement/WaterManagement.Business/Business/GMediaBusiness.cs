﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using WaterManagement.Models.RequestModel;
using WaterManagement.Utils.Common;
using WaterManagement.Utils.Common.Extensions;
using WaterManagement.Utils.Common.Helpers;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace WaterManagement.Business.Business
{
    public class GMediaBusiness
    {
        #region Fields

        private readonly IGMediaDAO _gMediaDAO;
        private readonly IGenerateLocationDAO _generateLocationDAO;
        private readonly IConfiguration _configuration;
        private readonly string _domain;
        private readonly string _path;
        private readonly string _folder;
        private readonly IEfRepository<GMedia> _efRepository;
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Ctor

        public GMediaBusiness(IGMediaDAO gMediaDAO,
                              IGenerateLocationDAO generateLocationDAO,
                              IConfiguration configuration,
                              IEfRepository<GMedia> efRepository,
                              IUnitOfWork unitOfWork)
        {
            _gMediaDAO = gMediaDAO;
            _generateLocationDAO = generateLocationDAO;
            _configuration = configuration;
            _domain = _configuration.GetSection("SaveImageInFolder:Domain").Value;
            _path = _configuration.GetSection("SaveImageInFolder:Path").Value;
            _folder = _configuration.GetSection("SaveImageInFolder:Folder").Value;
            _efRepository = efRepository;
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region Method

        /// <summary>
        /// Insert GMedia
        /// </summary>
        public async Task<List<GMedia>> InsertGMedia(GMediaRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            //
            string fldPath = _folder + DateTime.Now.Year.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Day.ToString();
            //
            string dirFullPath = Path.Combine(_path + fldPath);
            // create directory
            FileIO.CreateDirectory(dirFullPath);

            // TODO: 
            var listModel = new List<GMedia>();
            foreach (var item in request.JsonParameter)
            {
                // genUid
                string genUid = StringExtensions.GenUid();
                // create images save to database
                string folderPath1 = fldPath + "/" + "file_" + genUid;
                // create images save to folder
                string fileExtension = item.Type;
                string folderPath = _path + folderPath1 + fileExtension;
                //
                string pathToSave = Path.Combine(folderPath);

                // Save image in folder
                File.WriteAllBytes(pathToSave, Convert.FromBase64String(item.Base64));

                // HandleImage.SaveImageFromBase64(item.Base64, pathToSave, fileExtension);
                // save link url to database
                var model = new GMedia();
                model.Link = _domain + folderPath1 + fileExtension;
                model.CreatedAt = DateTime.Now.ToString();
                listModel.Add(model);
            }

            _efRepository.InsertRange(listModel);
            await _unitOfWork.SaveChangeAsync();

            return listModel;
        }

        /// <summary>
        /// Get all GMedia
        /// </summary>
        /// <param name="pageNumner"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<PagedList<GMedia>> GetAll(int pageNumner, int pageSize)
        {
            var data = await _gMediaDAO.GetAllGMedia();

            return new PagedList<GMedia>(data, pageNumner, pageSize);
        }

        /// <summary>
        /// Update gmedia
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task Update(int id, GMedia model)
        {
            var toUpdate = await _efRepository.SingleOrDefaultAsync(a => a.DeletedAt == 0 && a.Id == id);

            if (toUpdate == null)
                throw new ArgumentNullException(nameof(toUpdate));

            toUpdate.IdObj = model.IdObj;
            toUpdate.ObjKey = model.ObjKey;
            toUpdate.Link = model.Link;
            toUpdate.Tieude = model.Tieude;
            toUpdate.Thoigiankhoitao = model.Thoigiankhoitao;
            toUpdate.IdLoaitailieu = model.IdLoaitailieu;
            toUpdate.Tacgia = model.Tacgia;
            toUpdate.Nguoiky = model.Nguoiky;
            toUpdate.Note = model.Note;
            toUpdate.CreatedAt = model.CreatedAt;
            toUpdate.UpdatedAt = DateTime.Now.ToString();

            _efRepository.Update(toUpdate);

            await _unitOfWork.SaveChangeAsync();

        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task Delete(int id)
        {
            var toDelete = await _efRepository.SingleOrDefaultAsync(a => a.Id == id && a.DeletedAt == 0);

            if (toDelete == null)
                throw new ArgumentNullException(nameof(toDelete));

            toDelete.UpdatedAt = DateTime.Now.ToString();
            toDelete.DeletedAt = 1;

            _efRepository.Update(toDelete);
            await _unitOfWork.SaveChangeAsync();

        }
        #endregion
    }
}
