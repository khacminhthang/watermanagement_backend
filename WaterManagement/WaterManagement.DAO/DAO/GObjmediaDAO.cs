﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using WaterManagement.Models.RequestModel;
using WaterManagement.Models.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WaterManagement.DAO.DAO
{
    public class GObjmediaDAO : IGObjmediaDAO
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;
        private readonly IEfRepository<GObjmedia> _efRepository;
        private readonly IEfRepository<GMedia> _mediaEfRepository;


        #endregion

        #region Ctor

        public GObjmediaDAO(IUnitOfWork unitOfWork, IEfRepository<GObjmedia> efRepository, IEfRepository<GMedia> mediaEfRepository)
        {
            _unitOfWork = unitOfWork;
            _efRepository = efRepository;
            _mediaEfRepository = mediaEfRepository;
        }

        #endregion

        #region Method

        /// <summary>
        /// Get all images detail
        /// </summary>
        /// <param name="objId"></param>
        /// <param name="objType"></param>
        /// <returns></returns>
        public async Task Insert(CreateGObjMedia request)
        {
            // get all record with specify IdObj
            var existModel = await _efRepository.TableNoTracking.Where(a => a.IdObj == request.IdObj).ToListAsync();

            foreach (var idMedia in request.ListIdMedia)
            {
                var index = existModel.FindIndex(a => a.IdMedia == idMedia);

                if (index == -1)
                {
                    var model = new GObjmedia
                    {
                        IdObj = request.IdObj,
                        ObjKey = request.ObjKey,
                        IdMedia = idMedia,
                        IdObjmediaType = request.IdObjmediaType,
                        MediaType = request.MediaType

                    };
                    model.CreatedAt = DateTime.Now.ToString();
                    model.DeletedAt = 0;
                    //listInsertModel.Add(model);
                    _efRepository.Insert(model);
                }
                else
                {
                    var updateModel = existModel[index];
                    updateModel.UpdatedAt = DateTime.Now.ToString();
                    updateModel.MediaType = request.MediaType;
                    updateModel.IdObjmediaType = request.IdObjmediaType;
                    updateModel.IdObj = request.IdObj;
                    updateModel.ObjKey = request.ObjKey;
                    updateModel.DeletedAt = 0;

                    _efRepository.Update(existModel[index]);
                }
            }
            // Commit
            await _unitOfWork.SaveChangeAsync();

        }

        public async Task<IEnumerable<GObjmediaViewModel>> Get(int IdObj, short? ObjKey, int? IdObjmediaType)
        {
            var query = new List<GObjmedia>();
            // Kiểm tra các trường hợp request

            if (ObjKey == null && IdObjmediaType != null)
                query = await _efRepository.TableNoTracking.Where(a => a.IdObj == IdObj
                                                                  && a.IdObjmediaType == IdObjmediaType
                                                                  && a.DeletedAt == 0).ToListAsync();

            if (IdObjmediaType == null && ObjKey != null)
                query = await _efRepository.TableNoTracking.Where(a => a.IdObj == IdObj
                                                                  && a.ObjKey == ObjKey
                                                                  && a.DeletedAt == 0).ToListAsync();

            if (IdObjmediaType == null && ObjKey == null)
                query = await _efRepository.TableNoTracking.Where(a => a.IdObj == IdObj
                                                                  && a.DeletedAt == 0).ToListAsync();

            if (IdObjmediaType != null && ObjKey != null)
                query = await _efRepository.TableNoTracking.Where(a => a.IdObj == IdObj
                                                  && a.IdObjmediaType == IdObjmediaType
                                                  && a.ObjKey == ObjKey
                                                  && a.DeletedAt == 0).ToListAsync();

            if (query == null)
            {
                return null;
            }

            var resultModel = new List<GObjmediaViewModel>();

            foreach (var media in query)
            {
                var Media = await _mediaEfRepository.SingleOrDefaultAsync(a => a.Id == media.IdMedia && a.DeletedAt == 0);
                if (Media != null)
                {
                    var mediaDto = new GMediaViewModel()
                    {
                        IdObj = Media.IdObj,
                        ObjKey = Media.ObjKey,
                        Link = Media.Link,
                        Tieude = Media.Tieude,
                        Thoigiankhoitao = Media.Thoigiankhoitao,
                        IdLoaitailieu = Media.IdLoaitailieu,
                        Tacgia = Media.Tacgia,
                        Nguoiky = Media.Nguoiky,
                        Note = Media.Note,
                        CreatedAt = Media.CreatedAt,
                        UpdatedAt = Media.UpdatedAt,
                        DeletedAt = Media.DeletedAt,

                    };
                    var model = new GObjmediaViewModel()
                    {
                        Id = media.Id,
                        IdObj = media.IdObj,
                        IdMedia = media.IdMedia,
                        ObjKey = media.ObjKey,
                        IdObjmediaType = media.IdObjmediaType,
                        MediaType = media.MediaType,
                        CreatedAt = media.CreatedAt,
                        UpdatedAt = media.UpdatedAt,
                        Media = mediaDto
                    };

                    resultModel.Add(model);
                }

            }

            return resultModel;
        }
        #endregion
    }
}
