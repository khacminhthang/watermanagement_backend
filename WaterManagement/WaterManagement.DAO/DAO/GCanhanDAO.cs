﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;
using WaterManagement.Utils.Common;
using WaterManagement.Utils.Common.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WaterManagement.DAO.DAO
{
    public class GCanhanDAO : IGCanhanDAO
    {
        #region Fields

        private readonly IEfRepository<GCanhan> _efRepository;
        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor

        public GCanhanDAO(IEfRepository<GCanhan> efRepository,
            IUnitOfWork unitOfWork)
        {
            _efRepository = efRepository;
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region Method

        /// <summary>
        /// Get 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public async Task<GCanhan> Get(Expression<Func<GCanhan, bool>> expression)
        {
            return await _efRepository.SingleOrDefaultAsync(expression);
        }

        /// <summary>
        /// get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<GCanhan> GetbyId(int id)
        {
            return await _efRepository.SingleOrDefaultAsync(a => a.Id == id && a.DeletedAt == 0);
        }

        /// <summary>
        /// Get list
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public async Task<IList<GCanhanViewModel>> GetAll()
        {
            var status = Status.Active;
            var maxa = AppConstants.maxa;
            var mahuyen = AppConstants.mahuyen;
            var delete_at = AppConstants.deleted_at;

            //TODO
            var dbContext = _unitOfWork.DbContext;

            dbContext.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

            var query = (from a in dbContext.Set<GCanhan>()

                         join c in dbContext.Set<Dvhc>() on new { provinceId = a.Matinh, Status = status, districtId = mahuyen }
                                                         equals new { provinceId = c.Matinh, Status = c.Status.Value, districtId = c.Mahuyen } into cc
                         from c in cc.DefaultIfEmpty()
                         join d in dbContext.Set<Dvhc>() on new { districtId = a.Mahuyen, Status = status, wardId = maxa }
                                                         equals new { districtId = d.Mahuyen, Status = d.Status.Value, wardId = d.Maxa } into dd
                         from d in dd.DefaultIfEmpty()
                         join e in dbContext.Set<Dvhc>() on new { wardId = a.Maxa, Status = status }
                                                          equals new { wardId = e.Maxa, Status = e.Status.Value } into ee
                         from e in ee.DefaultIfEmpty()

                         where a.DeletedAt == delete_at
                         select new GCanhanViewModel
                         {
                             Id = a.Id,
                             Hovaten = a.Hovaten,
                             Diachi = a.Diachi,
                             Tentinh = c.Tendvhc,
                             Tenhuyen = d.Tendvhc,
                             Tenxa = e.Tendvhc,
                             Sodienthoai = a.Sodienthoai,
                             Email = a.Email,
                             Loaigiayto = a.Loaigiayto,
                             Socmthochieu = a.Socmthochieu,
                             Note = a.Note,
                             ImgLink = a.ImgLink,
                             Matinh = a.Matinh,
                             Mahuyen = a.Mahuyen,
                             Maxa = a.Maxa
                         });

            var data = await query.OrderByDescending(o => o.Id)
                                  .ToListAsync();

            return data;
        }

        /// <summary>
        /// Insert ca nhan
        /// </summary>
        /// <param name="gcanhan"></param>
        /// <returns></returns>
        public async Task InsertGCaNhan(GCanhan gcanhan)
        {
            if (gcanhan == null)
                throw new ArgumentNullException(nameof(gcanhan));


            _efRepository.Insert(gcanhan);
            await _unitOfWork.SaveChangeAsync();

        }

        /// <summary>
        /// delete
        /// </summary>
        /// <param name="gcanhan"></param>
        /// <returns></returns>
        public async Task DeleteGCanhan(GCanhan gcanhan)
        {
            if (gcanhan == null)
                throw new ArgumentNullException(nameof(gcanhan));

            _efRepository.Update(gcanhan);
            await _unitOfWork.SaveChangeAsync();
        }

        /// <summary>
        /// Update canhan
        /// </summary>
        /// <param name="gcanhan"></param>
        /// <returns></returns>
        public async Task UpdateGCanhan(GCanhan gCanhan)
        {
            if (gCanhan == null)
                throw new ArgumentNullException(nameof(gCanhan));

            _efRepository.Update(gCanhan);
            await _unitOfWork.SaveChangeAsync();
        }

        #endregion
    }
}
