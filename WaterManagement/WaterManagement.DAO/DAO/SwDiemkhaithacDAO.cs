﻿using System;
using System.Threading.Tasks;
using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;
using WaterManagement.Utils.Common;
using WaterManagement.Utils.Common.Enums;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace WaterManagement.DAO.DAO
{
    public class SwDiemkhaithacDAO : ISwDiemkhaithacDAO
    {
        #region Fields

        private readonly IEfRepository<SwDiemkhaithac> _efRepository;
        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor

        public SwDiemkhaithacDAO(IEfRepository<SwDiemkhaithac> efRepository,
                                 IUnitOfWork unitOfWork)
        {
            _efRepository = efRepository;
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region Method

        /// <summary>
        /// Get by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<SwDiemkhaithac> GetbyId(int id)
        {
            if (id <= 0)
                return null;

            return await _efRepository.SingleOrDefaultAsync(a => a.Id == id && a.DeletedAt == 0);
        }

        /// <summary>
        /// Get all
        /// </summary>
        /// <returns></returns>
        public async Task<IList<SwDiemkhaithacViewModel>> GetAll()
        {
            var status = Status.Active;
            var maxa = AppConstants.maxa;
            var mahuyen = AppConstants.mahuyen;
            var delete_at = AppConstants.deleted_at;
            var objType = AppConstants.typeSwDiemkhaithac;

            //TODO
            var dbContext = _unitOfWork.DbContext;

            dbContext.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

            var query = (from a in dbContext.Set<SwDiemkhaithac>()
                         join c in dbContext.Set<Dvhc>() on new { provinceId = a.Matinh, Status = status, districtId = mahuyen }
                                                         equals new { provinceId = c.Matinh, Status = c.Status.Value, districtId = c.Mahuyen } into cc
                         from c in cc.DefaultIfEmpty()
                         join d in dbContext.Set<Dvhc>() on new { districtId = a.Mahuyen, Status = status, wardId = maxa }
                                                         equals new { districtId = d.Mahuyen, Status = d.Status.Value, wardId = d.Maxa } into dd
                         from d in dd.DefaultIfEmpty()
                         join e in dbContext.Set<Dvhc>() on new { wardId = a.Maxa, Status = status }
                                                        equals new { wardId = e.Maxa, Status = e.Status.Value } into ee
                         from e in ee.DefaultIfEmpty()

                         where a.DeletedAt == delete_at
                         select new SwDiemkhaithacViewModel
                         {
                             Id = a.Id,
                             Geox = a.Geom.Coordinate.X,
                             Geoy = a.Geom.Coordinate.Y,
                             Sohieudiem = a.Sohieudiem,
                             Tendiem = a.Tendiem,
                             Toadox = a.Toadox,
                             Toadoy = a.Toadoy,
                             Caodoz = a.Caodoz,
                             Srid = a.Srid,
                             Vitri = a.Vitri,
                             Tentinh = c.Tendvhc,
                             Tenhuyen = d.Tendvhc,
                             Tenxa = e.Tendvhc,
                             IdDonviquanly = a.IdDonviquanly,
                             Note = a.Note,
                             ImgLink = a.ImgLink,
                             ObjType = objType,
                             Tendonvivanhanh = "",
                             Nhamay = a.Nhamay,
                             Thoigianxaydung = a.Thoigianxaydung,
                             Thoigianvanhanh = a.Thoigianvanhanh,
                             Luuluongchopheplonnhat = a.Luuluongchopheplonnhat,
                             Cothietbigiamsat = a.Cothietbigiamsat,
                             Thoiluongkhaithac = a.Thoiluongkhaithac,
                             Dacapphep = a.Dacapphep,
                             Matinh = a.Matinh,
                             Mahuyen = a.Mahuyen,
                             Maxa = a.Maxa,
                             IdMucdichsudungchinh = a.IdMucdichsudungchinh,
                             IdVanhanh = a.IdVanhanh,
                             IdChusudung = a.IdChusudung
                         });
            var data = await query.OrderByDescending(o => o.Id)
                                 .ToListAsync();

            return data;
        }

        /// <summary>
        /// Insert SwDiemkhaithac
        /// </summary>
        /// <param name="swDiemkhaithac"></param>
        /// <returns></returns>
        public async Task<SwDiemkhaithac> InsertSwDiemkhaithac(SwDiemkhaithac swDiemkhaithac)
        {
            if (swDiemkhaithac == null)
                throw new ArgumentNullException(nameof(swDiemkhaithac));

            _efRepository.Insert(swDiemkhaithac);
            await _unitOfWork.SaveChangeAsync();

            return swDiemkhaithac;
        }

        /// <summary>
        /// Update SwDiemkhaithac
        /// </summary>
        /// <param name="swDiemkhaithac"></param>
        /// <returns></returns>
        public async Task<SwDiemkhaithac> UpdateSwDiemkhaithac(SwDiemkhaithac swDiemkhaithac)
        {
            if (swDiemkhaithac == null)
                throw new ArgumentNullException(nameof(swDiemkhaithac));

            _efRepository.Update(swDiemkhaithac);
            await _unitOfWork.SaveChangeAsync();

            return swDiemkhaithac;
        }

        /// <summary>
        /// Delete SwDiemkhaithac
        /// </summary>
        /// <param name="swDiemkhaithac"></param>
        /// <returns></returns>
        public async Task<SwDiemkhaithac> DeleteSwDiemkhaithac(SwDiemkhaithac swDiemkhaithac)
        {
            if (swDiemkhaithac == null)
                throw new ArgumentNullException(nameof(swDiemkhaithac));

            _efRepository.Update(swDiemkhaithac);
            await _unitOfWork.SaveChangeAsync();

            return swDiemkhaithac;
        }

        #endregion
    }
}