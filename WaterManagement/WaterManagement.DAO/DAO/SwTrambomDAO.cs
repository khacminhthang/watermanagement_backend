﻿using System;
using System.Threading.Tasks;
using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;
using WaterManagement.Utils.Common;
using WaterManagement.Utils.Common.Enums;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace WaterManagement.DAO.DAO
{
    public class SwTrambomDAO : ISwTrambomDAO
    {
        #region Fields
        private readonly IEfRepository<SwTrambom> _swTrambomRepository;
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region ctor
        public SwTrambomDAO(IEfRepository<SwTrambom> swTrambomRepository, IUnitOfWork unitOfWork)
        {
            _swTrambomRepository = swTrambomRepository;
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Method

        /// <summary>
        /// delete
        /// </summary>
        /// <param name="swTrambom"></param>
        /// <returns></returns>
        public async Task<SwTrambom> DeleteSwTrambom(SwTrambom swTrambom)
        {
            if (swTrambom == null)
                throw new ArgumentNullException(nameof(swTrambom));
            _swTrambomRepository.Update(swTrambom);
            await _unitOfWork.SaveChangeAsync();
            return swTrambom;
        }

        /// <summary>
        /// get all
        /// </summary>
        /// <returns></returns>
        public async Task<IList<SwTrambomViewModel>> GetAll()
        {
            var status = Status.Active;
            var maxa = AppConstants.maxa;
            var mahuyen = AppConstants.mahuyen;
            var delete_at = AppConstants.deleted_at;
            //var objType = AppConstants.typeSwTrambom;

            //TODO
            var dbContext = _unitOfWork.DbContext;

            dbContext.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            var query = (from a in dbContext.Set<SwTrambom>()
                         join v in dbContext.Set<GCoquantochuc>() on new { IdCoquantochuc = a.IdVanhanh ?? 0, isDelete = delete_at }
                                                                  equals new { IdCoquantochuc = v.Id, isDelete = v.DeletedAt.Value } into vv
                         from v in vv.DefaultIfEmpty()
                         join c in dbContext.Set<Dvhc>() on new { provinceId = a.Matinh, Status = status, districtId = mahuyen }
                                                         equals new { provinceId = c.Matinh, Status = c.Status.Value, districtId = c.Mahuyen } into cc
                         from c in cc.DefaultIfEmpty()
                         join d in dbContext.Set<Dvhc>() on new { districtId = a.Mahuyen, Status = status, wardId = maxa }
                                                         equals new { districtId = d.Mahuyen, Status = d.Status.Value, wardId = d.Maxa } into dd
                         from d in dd.DefaultIfEmpty()
                         join e in dbContext.Set<Dvhc>() on new { wardId = a.Maxa, Status = status }
                                                        equals new { wardId = e.Maxa, Status = e.Status.Value } into ee
                         from e in ee.DefaultIfEmpty()

                         where a.DeletedAt == delete_at
                         select new SwTrambomViewModel
                         {
                             Id = a.Id,
                             Geox = a.Geom.Coordinate.X,
                             Geoy = a.Geom.Coordinate.Y,
                             Sohieudiem = a.Sohieutrambom,
                             Tendiem = a.Tentrambom,
                             Toadox = a.Toadox,
                             Toadoy = a.Toadoy,
                             Caodoz = a.Caodoz,
                             Srid = a.Srid,
                             Vitri = a.Vitri,
                             Tentinh = c.Tendvhc,
                             Tenhuyen = d.Tendvhc,
                             Tenxa = e.Tendvhc,
                             IdVanhanh = a.IdVanhanh,
                             Tendonvivanhanh = v.Tencoquan,
                             Idsong = a.IdSong,
                             Idhochua = a.IdHochua,
                             Tenhochua = null,
                             Tenhotunhien = null,
                             Idhotunhien = a.IdHotunhien,
                             Thoigianxaydung = a.Thoigianxaydung,
                             Thoigianvanhanh = a.Thoigianvanhanh,
                             Somaybomtieunuoc = a.Somaybomtieunuoc,
                             Somaybomtuoinuoc = a.Somaybomtuoinuoc,
                             Khanangbomtuoi = a.Khanangbomtuoi,
                             Luuluongtuoithietke = a.Luuluongtuoithietke,
                             Luuluongtuoithucte = a.Luuluongtuoithucte,
                             Dientichtuoithietke = a.Dientichtuoithietke,
                             Dientichtuoithucte = a.Dientichtuoithucte,
                             Khanangbomtieu = a.Khanangbomtieu,
                             Luuluongtieuthietke = a.Luuluongtieuthietke,
                             Luuluongtieuthucte = a.Luuluongtieuthucte,
                             Dientichtieuthietke = a.Dientichtieuthietke,
                             Dientichtieuthucte = a.Dientichtieuthucte,
                             Sohothietke = a.Sohothietke,
                             Sohothucte = a.Sohothucte,
                             Phantramcongsuathuuich = a.Phantramcongsuathuuich,
                             Matinh = a.Matinh,
                             Mahuyen = a.Mahuyen,
                             Maxa = a.Maxa,
                             Hientrang = a.Hientrang,
                             Note = a.Note,
                             ImgLink = a.ImgLink,
                             ObjKey = a.ObjKey
                             //ObjType = a.objType,
                         });

            var data = await query.OrderByDescending(o => o.Id)
                                  .ToListAsync();

            return data;
        }

        /// <summary>
        /// get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<SwTrambom> GetById(int id)
        {
            if (id <= 0)
                return null;
            return await _swTrambomRepository.SingleOrDefaultAsync(a => a.Id == id && a.DeletedAt == 0);
        }

        /// <summary>
        /// insert
        /// </summary>
        /// <param name="swTrambom"></param>
        /// <returns></returns>
        public async Task<SwTrambom> InsertSwTrambom(SwTrambom swTrambom)
        {
            if (swTrambom == null)
                throw new ArgumentNullException(nameof(swTrambom));
            _swTrambomRepository.Insert(swTrambom);
            await _unitOfWork.SaveChangeAsync();
            return swTrambom;
        }

        /// <summary>
        /// update
        /// </summary>
        /// <param name="swTrambom"></param>
        /// <returns></returns>
        public async Task<SwTrambom> UpdateSwTranbom(SwTrambom swTrambom)
        {
            if (swTrambom == null)
                throw new ArgumentNullException(nameof(swTrambom));
            _swTrambomRepository.Update(swTrambom);
            await _unitOfWork.SaveChangeAsync();
            return swTrambom;
        }

        #endregion
    }
}
