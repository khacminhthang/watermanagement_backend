﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WaterManagement.DAO.DAO
{
    public class GDuanDAO : IGDuanDAO
    {
        #region Fields
        private readonly IEfRepository<GDuan> _efRepository;
        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor
        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="efRepository"></param>
        /// <param name="unitOfWork"></param>
        public GDuanDAO(IEfRepository<GDuan> efRepository, IUnitOfWork unitOfWork)
        {
            _efRepository = efRepository;
            _unitOfWork = unitOfWork;

        }
        #endregion

        #region Method
        /// <summary>
        /// get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<GDuan> GetById(int id)
        {
            if (id <= 0)
                return null;

            return await _efRepository.SingleOrDefaultAsync(a => a.IdProject == id);
        }
        /// <summary>
        /// get all
        /// </summary>
        /// <returns></returns>
        public async Task<IList<GDuan>> GetAll()
        {
            var query = _efRepository.TableNoTracking;

            var data = await query.OrderByDescending(o => o.IdProject)
                                    .ToListAsync();
            return data;
        }
        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="gDuan"></param>
        /// <returns></returns>
        public async Task InsertGDuan(GDuan gDuan)
        {
            if (gDuan == null)
                throw new ArgumentNullException(nameof(gDuan));

            _efRepository.Insert(gDuan);
            await _unitOfWork.SaveChangeAsync();
        }
        /// <summary>
        /// update
        /// </summary>
        /// <param name="gDuan"></param>
        /// <returns></returns>
        public async Task UpdateGDuan(GDuan gDuan)
        {
            if (gDuan == null)
                throw new ArgumentNullException(nameof(gDuan));

            _efRepository.Update(gDuan);
            await _unitOfWork.SaveChangeAsync();
        }
        /// <summary>
        /// delete
        /// </summary>
        /// <param name="gDuan"></param>
        /// <returns></returns>
        public async Task DeleteGDuan(GDuan gDuan)
        {
            if (gDuan == null)
                throw new ArgumentNullException(nameof(gDuan));

            _efRepository.Delete(gDuan);
            await _unitOfWork.SaveChangeAsync();
        }

        #endregion
    }
}
