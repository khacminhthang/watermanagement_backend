﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WaterManagement.DAO.DAO
{
    public class GLoaisolieuDAO : IGLoaisolieuDAO
    {
        #region Fields
        private readonly IEfRepository<GLoaisolieu> _efRepository;
        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor
        /// <summary>
        /// contructor
        /// </summary>
        /// <param name="efRepository"></param>
        /// <param name="unitOfWork"></param>
        public GLoaisolieuDAO(IEfRepository<GLoaisolieu> efRepository, IUnitOfWork unitOfWork)
        {
            _efRepository = efRepository;
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region Method
        public async Task<GLoaisolieu> GetById(int id)
        {
            if (id <= 0)
                return null;

            return await _efRepository.SingleOrDefaultAsync(a => a.Id == id);
        }
        /// <summary>
        /// get all
        /// </summary>
        /// <returns></returns>
        public async Task<IList<GLoaisolieu>> GetAll()
        {
            var query = _efRepository.TableNoTracking;
            var data = await query.OrderBy(o => o.Id)
                                   .ToListAsync();

            return data;
        }
        /// <summary>
        /// insert
        /// </summary>
        /// <param name="gLoaisolieu"></param>
        /// <returns></returns>
        public async Task InsertGLoaisolieu(GLoaisolieu gLoaisolieu)
        {
            if (gLoaisolieu == null)
                throw new ArgumentNullException(nameof(gLoaisolieu));

            _efRepository.Insert(gLoaisolieu);
            await _unitOfWork.SaveChangeAsync();
        }
        /// <summary>
        /// update
        /// </summary>
        /// <param name="gLoaisolieu"></param>
        /// <returns></returns>
        public async Task UpdateGLoaisolieu(GLoaisolieu gLoaisolieu)
        {
            if (gLoaisolieu == null)
                throw new ArgumentNullException(nameof(gLoaisolieu));

            _efRepository.Update(gLoaisolieu);
            await _unitOfWork.SaveChangeAsync();
        }
        /// <summary>
        /// delete
        /// </summary>
        /// <param name="gLoaisolieu"></param>
        /// <returns></returns>
        public async Task DeleteGLoaisolieu(GLoaisolieu gLoaisolieu)
        {
            if (gLoaisolieu == null)
                throw new ArgumentNullException(nameof(gLoaisolieu));

            _efRepository.Delete(gLoaisolieu);
            await _unitOfWork.SaveChangeAsync();
        }

        #endregion
    }
}
