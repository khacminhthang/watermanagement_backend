﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using WaterManagement.Models.RequestModel;
using WaterManagement.Models.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WaterManagement.DAO.DAO
{
    public class GObjThamsoDAO: IGObjThamsoDAO
    {
        #region Fields

        private readonly IEfRepository<GObjThamso> _efRepository;
        private readonly IEfRepository<GThamso> _gThamsoRepository;
        private readonly IEfRepository<GDonvido> _gDonvidoRepository;
        private readonly IEfRepository<GNhomthamso> _gNhomthamsoRepository;
        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor

        public GObjThamsoDAO(IEfRepository<GObjThamso> efRepository,
                                                    IUnitOfWork unitOfWork,
                                                    IEfRepository<GThamso> gThamsoRepository,
                                                    IEfRepository<GDonvido> gDonvidoRepository,
                                                    IEfRepository<GNhomthamso> gNhomthamsoRepository)
        {
            _efRepository = efRepository;
            _unitOfWork = unitOfWork;
            _gThamsoRepository = gThamsoRepository;
            _gDonvidoRepository = gDonvidoRepository;
            _gNhomthamsoRepository = gNhomthamsoRepository;
        }

        #endregion

        public async Task<IEnumerable<GObjThamsoViewModel>> Get(int? idObj, short? ObjKey)
        {

            var query = (from a in _efRepository.TableNoTracking
                         join b in _gThamsoRepository.TableNoTracking on a.IdThamso equals b.Id into bb
                         from b in bb.DefaultIfEmpty()
                         join c in _gDonvidoRepository.TableNoTracking on b.IdDonvidomacdinh equals c.Id into cc
                         from c in cc.DefaultIfEmpty()
                         join d in _gNhomthamsoRepository.TableNoTracking on b.IdNhomthamso equals d.Id into dd
                         from d in dd.DefaultIfEmpty()
                         where a.IdObj == idObj && a.ObjKey == ObjKey
                         select new GObjThamsoViewModel
                         {
                             Id = a.Id,
                             IdObj = a.IdObj,
                             Idthamso = a.IdThamso,
                             ObjKey = a.ObjKey,
                             Tenthamso = b.Tenthamso,
                             Tennhom = d.Tennhom,
                             Tendonvido = c.Tendonvido,
                             Kyhieuthamso = b.Kyhieuthamso
                         }

                );

            if (query == null)
                return null;

            var data = await query.ToListAsync();

            return data;
        }

        public async Task Delete(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var toDelete = _efRepository.SingleOrDefault(a => a.Id == id);

            if (toDelete == null)
                throw new ArgumentNullException();

            _efRepository.Delete(toDelete);
            await _unitOfWork.SaveChangeAsync();

        }

        public async Task Insert(GObjThamsoRequest request)
        {

            if (request.JsonParameter == null)
                throw new ArgumentNullException();

            var list = new List<GObjThamso>();
            foreach (var item in request.JsonParameter)
            {
                var tempModel = new GObjThamso();
                tempModel.IdObj = item.IdObj;
                tempModel.IdThamso = item.IdThamso;
                tempModel.ObjKey = item.ObjKey;

                list.Add(tempModel);
            }

            _efRepository.InsertRange(list);
            await _unitOfWork.SaveChangeAsync();
        }
    }
}
