﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;
using WaterManagement.Utils.Common.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WaterManagement.DAO.DAO
{
    public class GThamsoDAO : IGThamsoDAO
    {
        #region Fields

        private readonly IEfRepository<GThamso> _efRepository;
        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor

        public GThamsoDAO(IEfRepository<GThamso> efRepository,
                          IUnitOfWork unitOfWork)
        {
            _efRepository = efRepository;
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region Method
        /// <summary>
        /// get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<GThamso> GetById(int id)
        {
            if (id <= 0)
                return null;
            return await _efRepository.SingleOrDefaultAsync(a => a.Id == id);
        }
        /// <summary>
        /// get all
        /// </summary>
        /// <returns></returns>
        public async Task<IList<GThamsoViewModel>> GetAll()
        {
            var status = Status.Active;
            var dbContext = _unitOfWork.DbContext;

            dbContext.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

            var query = (from a in dbContext.Set<GThamso>()


                         join b in dbContext.Set<GNhomthamso>() on a.IdNhomthamso equals b.Id into bb
                         from b in bb.DefaultIfEmpty()
                         select new GThamsoViewModel
                         {
                             Id = a.Id,
                             Tenthamso = a.Tenthamso,
                             Kyhieuthamso = a.Kyhieuthamso,
                             Tennhomthamso = b.Tennhom,
                             IdNhomthamso = a.IdNhomthamso,
                             IdDonvidomacdinh = a.IdDonvidomacdinh

                         });
            var data = await query.OrderByDescending(o => o.Id)
                                    .ToListAsync();

            return data;
        }
        /// <summary>
        /// insert
        /// </summary>
        /// <param name="thamso"></param>
        /// <returns></returns>
        public async Task Insert(GThamso thamso)
        {
            if (thamso == null)
                throw new ArgumentNullException(nameof(thamso));

            _efRepository.Insert(thamso);
            await _unitOfWork.SaveChangeAsync();
        }
        /// <summary>
        /// delete
        /// </summary>
        /// <param name="thamso"></param>
        /// <returns></returns>
        public async Task Delete(GThamso thamso)
        {
            if (thamso == null)
                throw new ArgumentNullException(nameof(thamso));

            _efRepository.Delete(thamso);
            await _unitOfWork.SaveChangeAsync();
        }
        /// <summary>
        /// update
        /// </summary>
        /// <param name="thamso"></param>
        /// <returns></returns>
        public async Task Update(GThamso thamso)
        {
            if (thamso == null)
                throw new ArgumentNullException(nameof(thamso));

            _efRepository.Update(thamso);
            await _unitOfWork.SaveChangeAsync();
        }

        #endregion
    }
}
