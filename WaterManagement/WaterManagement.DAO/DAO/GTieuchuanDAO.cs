﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WaterManagement.DAO.DAO
{
    public class GTieuchuanDAO : IGTieuchuanDAO
    {
        #region Fields

        private readonly IEfRepository<GTieuchuan> _efRepository;
        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor

        public GTieuchuanDAO(IEfRepository<GTieuchuan> efRepository,
                          IUnitOfWork unitOfWork)
        {
            _efRepository = efRepository;
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region Method
        /// <summary>
        /// get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<GTieuchuan> GetById(int id)
        {
            if (id <= 0)
                return null;
            return await _efRepository.SingleOrDefaultAsync(a => a.Id == id);
        }
        /// <summary>
        /// get all
        /// </summary>
        /// <returns></returns>
        public async Task<IList<GTieuchuanViewModel>> GetAll()
        {
            var dbContext = _unitOfWork.DbContext;

            dbContext.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

            var query = (from a in dbContext.Set<GTieuchuan>()


                         join b in dbContext.Set<GMedia>() on a.IdTailieu equals b.Id into bb
                         from b in bb.DefaultIfEmpty()
                         select new GTieuchuanViewModel
                         {
                             Id = a.Id,
                             Tentieuchuan = a.Tentieuchuan,
                             Sohieutieuchuan = a.Sohieutieuchuan,
                             Coquanbanhanh = a.Coquanbanhanh,
                             Thoigianbanhanh = a.Thoigianbanhanh,
                             Hientrang = a.Hientrang,
                             Tentailieu = b.Tieude,
                             IdTailieu = a.IdTailieu

                         });
            var data = await query.OrderByDescending(o => o.Id)
                                    .ToListAsync();

            return data;
        }
        /// <summary>
        /// insert
        /// </summary>
        /// <param name="gtieuchuan"></param>
        /// <returns></returns>
        public async Task InsertGTieuChuan(GTieuchuan gtieuchuan)
        {
            if (gtieuchuan == null)
                throw new ArgumentNullException(nameof(gtieuchuan));

            _efRepository.Insert(gtieuchuan);
            await _unitOfWork.SaveChangeAsync();
        }
        /// <summary>
        /// delete
        /// </summary>
        /// <param name="gtieuchuan"></param>
        /// <returns></returns>
        public async Task DeleteGTieuChuan(GTieuchuan gtieuchuan)
        {
            if (gtieuchuan == null)
                throw new ArgumentNullException(nameof(gtieuchuan));

            _efRepository.Delete(gtieuchuan);
            await _unitOfWork.SaveChangeAsync();
        }
        /// <summary>
        /// update
        /// </summary>
        /// <param name="gtieuchuan"></param>
        /// <returns></returns>
        public async Task UpdateGTieuChuan(GTieuchuan gtieuchuan)
        {
            if (gtieuchuan == null)
                throw new ArgumentNullException(nameof(gtieuchuan));

            _efRepository.Update(gtieuchuan);
            await _unitOfWork.SaveChangeAsync();
        }

        #endregion
    }
}
