﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;
using WaterManagement.Utils.Common;
using WaterManagement.Utils.Common.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WaterManagement.DAO.DAO
{
    public class GCoquantochucDAO : IGCoquantochucDAO
    {
        #region Fields

        private readonly IEfRepository<GCoquantochuc> _efRepository;
        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor

        public GCoquantochucDAO(IEfRepository<GCoquantochuc> efRepository,
                                IUnitOfWork unitOfWork)
        {
            _efRepository = efRepository;
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region Method

        /// <summary>
        /// Get 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public async Task<GCoquantochuc> Get(Expression<Func<GCoquantochuc, bool>> expression)
        {
            return await _efRepository.SingleOrDefaultAsync(expression);
        }

        /// <summary>
        ///  Insert GCoquantochuc
        /// </summary>
        /// <param name="gCoquantochuc"></param>
        /// <returns></returns>
        public async Task InsertGCoquantochuc(GCoquantochuc gCoquantochuc)
        {
            if (gCoquantochuc == null)
                throw new ArgumentNullException(nameof(gCoquantochuc));

            _efRepository.Insert(gCoquantochuc);
            await _unitOfWork.SaveChangeAsync();
        }

        /// <summary>
        ///  Update GCoquantochuc
        /// </summary>
        /// <param name="gCoquantochuc"></param>
        /// <returns></returns>
        public async Task UpdateGCoquantochuc(GCoquantochuc gCoquantochuc)
        {
            if (gCoquantochuc == null)
                throw new ArgumentNullException(nameof(gCoquantochuc));

            _efRepository.Update(gCoquantochuc);
            await _unitOfWork.SaveChangeAsync();
        }

        /// <summary>
        /// Delete GCoquantochuc
        /// </summary>
        /// <param name="gCoquantochuc"></param>
        /// <returns></returns>
        public async Task DeleteGCoquantochuc(GCoquantochuc gCoquantochuc)
        {
            if (gCoquantochuc == null)
                throw new ArgumentNullException(nameof(gCoquantochuc));

            _efRepository.Update(gCoquantochuc);
            await _unitOfWork.SaveChangeAsync();
        }

        /// <summary>
        /// GetListCoquantochuc 
        /// </summary>
        /// <returns></returns>
        public async Task<IList<GCoquantochucViewModel>> GetListCoquantochuc()
        {
            var status = Status.Active;
            var maxa = AppConstants.maxa;
            var mahuyen = AppConstants.mahuyen;
            var delete_at = AppConstants.deleted_at;

            var dbContext = _unitOfWork.DbContext;
            dbContext.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

            var gCoquantochucModel = (from a in dbContext.Set<GCoquantochuc>()
                                      join c in dbContext.Set<Dvhc>() on new { provinceId = a.Matinh, Status = status, districtId = mahuyen }
                                                 equals new { provinceId = c.Matinh, Status = c.Status.Value, districtId = c.Mahuyen } into cc
                                      from c in cc.DefaultIfEmpty()
                                      join d in dbContext.Set<Dvhc>() on new { districtId = a.Mahuyen, Status = status, wardId = maxa }
                                                                    equals new { districtId = d.Mahuyen, Status = d.Status.Value, wardId = d.Maxa } into dd
                                      from d in dd.DefaultIfEmpty()
                                      join e in dbContext.Set<Dvhc>() on new { wardId = a.Maxa, Status = status }
                                                                    equals new { wardId = e.Maxa, Status = e.Status.Value } into ee
                                      from e in ee.DefaultIfEmpty()
                                      where a.DeletedAt == delete_at
                                      select new GCoquantochucViewModel
                                      {
                                          Id = a.Id,
                                          Idcha = a.Idcha,
                                          Tencoquan = a.Tencoquan,
                                          Sodienthoai = a.Sodienthoai,
                                          Email = a.Email,
                                          Diachi = a.Diachi,
                                          Tentinh = c.Tendvhc,
                                          Tenhuyen = d.Tendvhc,
                                          Tenxa = e.Tendvhc,
                                          matinh = e.Matinh,
                                          mahuyen = e.Mahuyen,
                                          maxa = e.Maxa,
                                          Note = a.Note,
                                          ImgLink = a.ImgLink
                                      });

            var data = await gCoquantochucModel.OrderByDescending(o => o.Id)
                                               .ToListAsync();

            return data;
        }
        /// <summary>
        /// DeleteRange
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public async Task UpdateRange(List<GCoquantochuc> list)
        {
            if (list.Count() > 0)
                _efRepository.UpdateRange(list);

            await _unitOfWork.SaveChangeAsync();
        }
        #endregion
    }
}
