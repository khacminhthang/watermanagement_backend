﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;

namespace WaterManagement.DAO.DAO
{
    public class GenerateLocationDAO : IGenerateLocationDAO
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _configuration;
        private readonly int srIdGoogle;

        #endregion

        #region Ctor

        public GenerateLocationDAO(IUnitOfWork unitOfWork,
                                   IConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _configuration = configuration;
            srIdGoogle = Convert.ToInt32(_configuration.GetSection("SRIDGoogle:srId").Value);
        }

        #endregion

        #region Method

        /// <summary>
        /// Generate geometry
        /// </summary>
        /// <param name="toax"></param>
        /// <param name="toadoy"></param>
        /// <param name="srId"></param>
        /// <returns></returns>
        public virtual GenerateGeometryViewModel GenerateLocation(double toadox, double toadoy, int srId)
        {
            var data = new GenerateGeometryViewModel();

            string sql = $@"SELECT ST_Transform(ST_PointFromText('POINT({toadox} {toadoy})', {srId}), {srIdGoogle});";
            try
            {
                var dbContext = _unitOfWork.DbContext.Database;

                var command = dbContext.GetDbConnection().CreateCommand();

                command.CommandText = sql;

                dbContext.OpenConnection();

                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    data.Geometry = reader["st_transform"];
                }

                command.Dispose();
                reader.Close();
            }
            catch (Exception e)
            {
                throw e;
            }

            return data;
        }

        #endregion
    }
}
