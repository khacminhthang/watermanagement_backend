﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WaterManagement.DAO.DAO
{
    public class GMediaDAO : IGMediaDAO
    {
        #region Fields

        private readonly IEfRepository<GMedia> _efRepository;
        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor

        public GMediaDAO(IEfRepository<GMedia> efRepository,
                         IUnitOfWork unitOfWork)
        {
            _efRepository = efRepository;
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region Method

        /// <summary>
        /// Get all
        /// </summary>
        /// <returns></returns>
        public async Task<IList<GMedia>> GetAllGMedia()
        {
            var query = _efRepository.TableNoTracking;

            var data = await query.Where(a => a.DeletedAt == 0)
                                  .OrderByDescending(o => o.CreatedAt)
                                  .ToListAsync();

            return data;
        }

        /// <summary>
        /// get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<GMedia> GetGMediaById(int id)
        {
            if (id <= 0)
                return null;

            return await _efRepository.SingleOrDefaultAsync(a => a.Id == id && a.DeletedAt == 0);
        }

        /// <summary>
        /// Insert GMedia
        /// </summary>
        /// <param name="gMedia"></param>
        public GMedia InsertGMedia(GMedia gMedia)
        {
            if (gMedia == null)
                throw new ArgumentNullException(nameof(gMedia));

            _efRepository.Insert(gMedia);
            _unitOfWork.SaveChange();

            return gMedia;
        }

        #endregion
    }
}
