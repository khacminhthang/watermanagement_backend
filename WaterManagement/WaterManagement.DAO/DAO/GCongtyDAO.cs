﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;
using WaterManagement.Utils.Common;
using WaterManagement.Utils.Common.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WaterManagement.DAO.DAO
{
    public class GCongtyDAO : IGCongtyDAO
    {
        #region Fields

        private readonly IEfRepository<GCongty> _efRepository;
        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor

        public GCongtyDAO(IEfRepository<GCongty> efRepository,
                          IUnitOfWork unitOfWork)
        {
            _efRepository = efRepository;
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region Method

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public async Task<GCongty> Get(Expression<Func<GCongty, bool>> expression)
        {
            return await _efRepository.SingleOrDefaultAsync(expression);
        }

        /// <summary>
        ///  Get all
        /// </summary>
        /// <returns></returns>
        public async Task<IList<GCongtyViewModel>> GetAll()
        {
            var status = Status.Active;
            var maxa = AppConstants.maxa;
            var mahuyen = AppConstants.mahuyen;
            var delete_at = AppConstants.deleted_at;

            var dbContext = _unitOfWork.DbContext;
            dbContext.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

            var query = (from a in dbContext.Set<GCongty>()
                         join c in dbContext.Set<Dvhc>() on new { provinceId = a.Matinh, Status = status, districtId = mahuyen }
                                                       equals new { provinceId = c.Matinh, Status = c.Status.Value, districtId = c.Mahuyen } into cc
                         from c in cc.DefaultIfEmpty()
                         join d in dbContext.Set<Dvhc>() on new { districtId = a.Mahuyen, Status = status, wardId = maxa }
                                                         equals new { districtId = d.Mahuyen, Status = d.Status.Value, wardId = d.Maxa } into dd
                         from d in dd.DefaultIfEmpty()
                         join e in dbContext.Set<Dvhc>() on new { wardId = a.Maxa, Status = status }
                                                          equals new { wardId = e.Maxa, Status = e.Status.Value } into ee
                         from e in ee.DefaultIfEmpty()
                         where a.DeletedAt == delete_at
                         select new GCongtyViewModel
                         {
                             Id = a.Id,
                             Tencongty = a.Tencongty,
                             Diachi = a.Diachi,
                             Tentinh = c.Tendvhc,
                             Tenhuyen = d.Tendvhc,
                             Tenxa = e.Tendvhc,
                             Sodienthoai = a.Sodienthoai,
                             Email = a.Email,
                             Note = a.Note,
                             ImgLink = a.ImgLink,
                             Matinh = a.Matinh,
                             Mahuyen = a.Mahuyen,
                             Maxa = a.Maxa
                         });

            var data = await query.OrderByDescending(o => o.Id)
                                  .ToListAsync();

            return data;
        }

        /// <summary>
        ///  Insert GCongty
        /// </summary>
        /// <param name="gCongty"></param>
        /// <returns></returns>
        public async Task InsertGCongty(GCongty gCongty)
        {
            if (gCongty == null)
                throw new ArgumentNullException(nameof(gCongty));

            _efRepository.Insert(gCongty);
            await _unitOfWork.SaveChangeAsync();
        }

        /// <summary>
        /// Update GCongty
        /// </summary>
        /// <param name="gCongty"></param>
        /// <returns></returns>
        public async Task UpdateGCongty(GCongty gCongty)
        {
            if (gCongty == null)
                throw new ArgumentNullException(nameof(gCongty));

            _efRepository.Update(gCongty);
            await _unitOfWork.SaveChangeAsync();
        }

        /// <summary>
        /// Delete GCongty
        /// </summary>
        /// <param name="gCongty"></param>
        /// <returns></returns>
        public async Task DeleteGCongty(GCongty gCongty)
        {
            if (gCongty == null)
                throw new ArgumentNullException(nameof(gCongty));

            _efRepository.Update(gCongty);
            await _unitOfWork.SaveChangeAsync();
        }

        #endregion
    }
}
