﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WaterManagement.DAO.DAO
{
    public class GDlquantractsDAO : IGDlquantractsDAO
    {
        #region Fields

        private readonly IEfRepository<GDlquantracts> _efRepository;

        #endregion

        #region Ctor

        public GDlquantractsDAO(IEfRepository<GDlquantracts> efRepository)
        {
            _efRepository = efRepository;
        }

        #endregion


        /// <summary>
        /// Get all 
        /// </summary>
        /// <param name="expression">expression</param>
        /// <returns></returns>
        public async Task<IList<GDlquantracts>> GetAll(Expression<Func<GDlquantracts, bool>> expression)
        {
            var query = _efRepository.TableNoTracking.Where(expression);

            var data = await query.OrderBy(o => o.FullTime)
                                   .ToListAsync();

            return data;
        }
    }
}
