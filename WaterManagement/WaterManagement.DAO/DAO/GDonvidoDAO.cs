﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WaterManagement.DAO.DAO
{
    public class GDonvidoDAO : IGDonvidoDAO
    {
        #region Fields
        private readonly IEfRepository<GDonvido> _efRepository;
        private readonly IUnitOfWork _unitOfWork;

        #endregion
        #region Ctor
        public GDonvidoDAO(IEfRepository<GDonvido> efRepository, IUnitOfWork unitOfWork)
        {
            _efRepository = efRepository;
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region Method
        /// <summary>
        /// Get
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public async Task<GDonvido> Get(Expression<Func<GDonvido, bool>> expression)
        {
            return await _efRepository.SingleOrDefaultAsync(expression);
        }

        /// <summary>
        /// Get all
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public async Task<IList<GDonvido>> GetList()
        {
            return await _efRepository.TableNoTracking.ToListAsync();
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task Insert(GDonvido model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            _efRepository.Insert(model);

            await _unitOfWork.SaveChangeAsync();
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task Update(GDonvido model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            _efRepository.Update(model);

            await _unitOfWork.SaveChangeAsync();
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task Delete(GDonvido model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            _efRepository.Delete(model);

            await _unitOfWork.SaveChangeAsync();
        }

        #endregion
    }
}
