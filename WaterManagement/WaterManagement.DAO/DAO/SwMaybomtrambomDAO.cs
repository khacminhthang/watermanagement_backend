﻿using System;
using System.Threading.Tasks;
using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;
using WaterManagement.Utils.Common;
using WaterManagement.Utils.Common.Enums;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace WaterManagement.DAO.DAO
{
    public class SwMaybomtrambomDAO : ISwMaybomtrambomDAO
    {
        private readonly IEfRepository<SwMaybomtrambom> _efRepository;
        private readonly IUnitOfWork _unitOfWork;

        public SwMaybomtrambomDAO(IEfRepository<SwMaybomtrambom> efRepository, IUnitOfWork unitOfWork)
        {
            _efRepository = efRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<SwMaybomtrambom> DeleteSwMaybomtrambom(SwMaybomtrambom model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));
            _efRepository.Update(model);
            await _unitOfWork.SaveChangeAsync();
            return model;
        }

        public async Task<IList<SwMaybomtrambom>> GetAllById(int id)
        {
            if (id <= 0)
                return null;
            var dbContext = _unitOfWork.DbContext;
            dbContext.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            var query = (from a in dbContext.Set<SwMaybomtrambom>()
                         where a.DeletedAt == 0 && a.IdTrambom == id
                         select new SwMaybomtrambom
                         {
                             Id = a.Id,
                             IdTrambom = a.IdTrambom,
                             Loaimaybom = a.Loaimaybom,
                             Thoigianlapdat = a.Thoigianlapdat,
                             Congsuat = a.Congsuat,
                             Note = a.Note
                         });
            var data = await query.OrderByDescending(o => o.Id).ToListAsync();
            return data;
        }

        public async Task<SwMaybomtrambom> GetById(int id)
        {
            if (id <= 0)
                return null;
            return await _efRepository.SingleOrDefaultAsync(a => a.Id == id && a.DeletedAt == 0);
        }

        public async Task<SwMaybomtrambom> InsertSwMaybomtrambom(SwMaybomtrambom model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));
            _efRepository.Insert(model);
            await _unitOfWork.SaveChangeAsync();
            return model;
        }

        public async Task<SwMaybomtrambom> UpdateSwMaybomtrambom(SwMaybomtrambom model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));
            _efRepository.Update(model);
            await _unitOfWork.SaveChangeAsync();
            return model;
        }
    }
}
