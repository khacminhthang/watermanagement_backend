﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WaterManagement.DAO.DAO
{
    public class GPhantichDAO : IGPhantichDAO
    {
        #region Fields

        private readonly IEfRepository<GPhantich> _efRepository;
        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor

        public GPhantichDAO(IEfRepository<GPhantich> efRepository,
                            IUnitOfWork unitOfWork)
        {
            _efRepository = efRepository;
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region Method

        /// <summary>
        /// Get by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<GPhantich> GetbyId(int id)
        {
            if (id <= 0)
                return null;

            return await _efRepository.SingleOrDefaultAsync(a => a.Id == id && a.DeletedAt == 0);
        }

        /// <summary>
        /// Get all
        /// </summary>
        /// <returns></returns>
        public async Task<IList<GPhantich>> GetAllGPhantich()
        {
            var query = _efRepository.TableNoTracking;

            var data = await query.Where(a => a.DeletedAt == 0)
                                 .OrderByDescending(o => o.Id)
                                 .ToListAsync();

            return data;
        }

        /// <summary>
        /// Delete GPhantich
        /// </summary>
        /// <param name="gPhantich"></param>
        /// <returns></returns>
        public async Task DeleteGPhantich(GPhantich gPhantich)
        {
            if (gPhantich == null)
                throw new ArgumentNullException(nameof(gPhantich));

            _efRepository.Update(gPhantich);
            await _unitOfWork.SaveChangeAsync();
        }

        /// <summary>
        /// Insert GPhantich
        /// </summary>
        /// <param name="gPhantich"></param>
        /// <returns></returns>
        public async Task InsertGPhantich(GPhantich gPhantich)
        {
            if (gPhantich == null)
                throw new ArgumentNullException(nameof(gPhantich));

            _efRepository.Insert(gPhantich);
            await _unitOfWork.SaveChangeAsync();
        }

        /// <summary>
        /// Update GPhantich
        /// </summary>
        /// <param name="gPhantich"></param>
        /// <returns></returns>
        public async Task UpdateGPhantich(GPhantich gPhantich)
        {
            if (gPhantich == null)
                throw new ArgumentNullException(nameof(gPhantich));

            _efRepository.Update(gPhantich);
            await _unitOfWork.SaveChangeAsync();
        }

        #endregion
    }
}
