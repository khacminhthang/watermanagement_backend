﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WaterManagement.DAO.DAO
{
    public class GNhomthamsoDAO : IGNhomthamsoDAO
    {
        #region Fileds

        private readonly IEfRepository<GNhomthamso> _efRepository;
        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor

        public GNhomthamsoDAO(IEfRepository<GNhomthamso> efRepository,
                               IUnitOfWork unitOfWork)
        {
            _efRepository = efRepository;
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region Method

        /// <summary>
        ///  Get
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public async Task<GNhomthamso> Get(Expression<Func<GNhomthamso, bool>> expression)
        {
            return await _efRepository.SingleOrDefaultAsync(expression);
        }

        /// <summary>
        ///  Get list
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public async Task<IList<GNhomthamso>> GetList(Expression<Func<GNhomthamso, bool>> expression)
        {
            var query = _efRepository.TableNoTracking.Where(expression);

            var data = await query.OrderBy(o => o.Id)
                                   .ToListAsync();

            return data;
        }

        /// <summary>
        /// Insert 
        /// </summary>
        /// <param name="gNhomthamso"></param>
        /// <returns></returns>
        public async Task InsertGNhomthamso(GNhomthamso gNhomthamso)
        {
            if (gNhomthamso == null)
                throw new ArgumentNullException(nameof(gNhomthamso));

            _efRepository.Insert(gNhomthamso);
            await _unitOfWork.SaveChangeAsync();
        }

        /// <summary>
        /// Update 
        /// </summary>
        /// <param name="gNhomthamso"></param>
        /// <returns></returns>
        public async Task UpdateGNhomthamso(GNhomthamso gNhomthamso)
        {
            if (gNhomthamso == null)
                throw new ArgumentNullException(nameof(gNhomthamso));

            _efRepository.Update(gNhomthamso);
            await _unitOfWork.SaveChangeAsync();
        }

        /// <summary>
        ///  Delete 
        /// </summary>
        /// <param name="gNhomthamso"></param>
        /// <returns></returns>
        public async Task DeleteGNhomthamso(GNhomthamso gNhomthamso)
        {
            if (gNhomthamso == null)
                throw new ArgumentNullException(nameof(gNhomthamso));

            _efRepository.Delete(gNhomthamso);
            await _unitOfWork.SaveChangeAsync();
        }

        #endregion
    }
}
