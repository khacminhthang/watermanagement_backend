﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WaterManagement.DAO.DAO
{
    public class GTieuchuanchatluongDAO : IGTieuchuanchatluongDAO
    {
        #region Fields

        private readonly IEfRepository<GTieuchuanchatluong> _efRepository;
        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor

        public GTieuchuanchatluongDAO(IEfRepository<GTieuchuanchatluong> efRepository,
                          IUnitOfWork unitOfWork)
        {
            _efRepository = efRepository;
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region Method
        /// <summary>
        /// get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<GTieuchuanchatluong> GetById(int id)
        {
            if (id <= 0)
                return null;
            return await _efRepository.SingleOrDefaultAsync(a => a.Id == id);
        }
        /// <summary>
        /// get all
        /// </summary>
        /// <returns></returns>
        public async Task<IList<GTieuchuanchatluongViewModel>> GetAll()
        {
            var dbContext = _unitOfWork.DbContext;
            dbContext.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            var query = (from a in dbContext.Set<GTieuchuanchatluong>()
                         join b in dbContext.Set<GTieuchuan>() on a.IdTieuchuan equals b.Id into bb
                         from b in bb.DefaultIfEmpty()
                         join c in dbContext.Set<GThamso>() on a.IdThamso equals c.Id into cc
                         from c in cc.DefaultIfEmpty()
                         select new GTieuchuanchatluongViewModel
                         {
                             Id = a.Id,
                             IdTieuchuan = a.IdTieuchuan,
                             IdDonvido = a.IdDonvido,
                             IdThamso = a.IdThamso,
                             Gioihantren = a.Gioihantren,
                             Gioihanduoi = a.Gioihanduoi,
                             Phuongphapthu = a.Phuongphapthu,
                             Captieuchuan = a.Captieuchuan,
                             Mucdogiamsat = a.Mucdogiamsat,
                             Tenthamso = c.Tenthamso,
                             Tentieuchuan = b.Tentieuchuan,


                         });
            var data = await query.OrderByDescending(o => o.Id)
                      .ToListAsync();

            return data;
        }
        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="gTieuchuanchatluong"></param>
        /// <returns></returns>
        public async Task InsertGTieuchuanchatluong(GTieuchuanchatluong gTieuchuanchatluong)
        {
            if (gTieuchuanchatluong == null)
                throw new ArgumentNullException(nameof(gTieuchuanchatluong));

            _efRepository.Insert(gTieuchuanchatluong);
            await _unitOfWork.SaveChangeAsync();
        }
        /// <summary>
        /// update
        /// </summary>
        /// <param name="gTieuchuanchatluong"></param>
        /// <returns></returns>
        public async Task UpdateGTieuchuanchatluong(GTieuchuanchatluong gTieuchuanchatluong)
        {
            if (gTieuchuanchatluong == null)
                throw new ArgumentNullException(nameof(gTieuchuanchatluong));

            _efRepository.Update(gTieuchuanchatluong);
            await _unitOfWork.SaveChangeAsync();
        }

        public async Task DeleteGTieuchuanchatluong(GTieuchuanchatluong gTieuchuanchatluong)
        {
            if (gTieuchuanchatluong == null)
                throw new ArgumentNullException(nameof(gTieuchuanchatluong));

            _efRepository.Delete(gTieuchuanchatluong);
            await _unitOfWork.SaveChangeAsync();
        }

        #endregion
    }
}
