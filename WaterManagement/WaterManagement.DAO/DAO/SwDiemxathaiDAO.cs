﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;
using WaterManagement.Utils.Common;
using WaterManagement.Utils.Common.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WaterManagement.DAO.DAO
{
    public class SwDiemxathaiDAO : ISwDiemxathaiDAO
    {
        #region Fields

        private readonly IEfRepository<SwDiemxathai> _efRepository;
        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor

        public SwDiemxathaiDAO(IEfRepository<SwDiemxathai> efRepository,
                                IUnitOfWork unitOfWork)
        {
            _efRepository = efRepository;
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region Method

        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<SwDiemxathai> GetbyId(int id)
        {
            if (id <= 0)
                return null;

            return await _efRepository.SingleOrDefaultAsync(a => a.Id == id && a.DeletedAt == 0);
        }

        /// <summary>
        /// Get all
        /// </summary>
        /// <returns></returns>
        public async Task<IList<SwDiemxathaiViewModel>> GetAll()
        {
            var status = Status.Active;
            var maxa = AppConstants.maxa;
            var mahuyen = AppConstants.mahuyen;
            var delete_at = AppConstants.deleted_at;
            var objType = AppConstants.typeSwDiemxathai;

            //TODO
            var dbContext = _unitOfWork.DbContext;

            dbContext.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

            var query = (from a in dbContext.Set<SwDiemxathai>()
                         join b in dbContext.Set<GCoquantochuc>() on new { IdCoquantochuc = a.IdDonviquanly ?? 0, isDelete = delete_at }
                                                                  equals new { IdCoquantochuc = b.Id, isDelete = b.DeletedAt.Value } into bb
                         from b in bb.DefaultIfEmpty()
                         join gCanhan in dbContext.Set<GCanhan>() on new { isDelete = delete_at, IdChusudung = a.IdChusudung ?? 0 }
                                                                  equals new { isDelete = gCanhan.DeletedAt.Value, IdChusudung = gCanhan.Id } into gg
                         from gCanhan in gg.DefaultIfEmpty()

                         join c in dbContext.Set<Dvhc>() on new { provinceId = a.Matinh, Status = status, districtId = mahuyen }
                                                         equals new { provinceId = c.Matinh, Status = c.Status.Value, districtId = c.Mahuyen } into cc
                         from c in cc.DefaultIfEmpty()
                         join d in dbContext.Set<Dvhc>() on new { districtId = a.Mahuyen, Status = status, wardId = maxa }
                                                         equals new { districtId = d.Mahuyen, Status = d.Status.Value, wardId = d.Maxa } into dd
                         from d in dd.DefaultIfEmpty()
                         join e in dbContext.Set<Dvhc>() on new { wardId = a.Maxa, Status = status }
                                                          equals new { wardId = e.Maxa, Status = e.Status.Value } into ee
                         from e in ee.DefaultIfEmpty()

                         where a.DeletedAt == delete_at
                         select new SwDiemxathaiViewModel
                         {
                             Id = a.Id,
                             Geox = a.Geom.Coordinate.X,
                             Geoy = a.Geom.Coordinate.Y,
                             Sohieudiem = a.Sohieudiem,
                             Tendiem = a.Tendiem,
                             Toadox = a.Toadox,
                             Toadoy = a.Toadoy,
                             Caodoz = a.Caodoz,
                             Srid = a.Srid,
                             Vitri = a.Vitri,
                             Tentinh = c.Tendvhc,
                             Tenhuyen = d.Tendvhc,
                             Tenxa = e.Tendvhc,
                             IdDonviquanly = a.IdDonviquanly,
                             IdChusudung = a.IdChusudung,
                             Note = a.Note,
                             ImgLink = a.ImgLink,
                             ObjType = objType,
                             Tenchusudung = gCanhan.Hovaten,
                             Thuyvuctiepnhan = a.Thuyvuctiepnhan,
                             Phuongthucxa = a.Phuongthucxa,
                             Hinhthucdan = a.Hinhthucdan,
                             Luuluongxatrungbinh = a.Luuluongxatrungbinh,
                             Luuluonglonnhatchophep = a.Luuluonglonnhatchophep,
                             Quychuanapdung = a.Quychuanapdung,
                             Cothietbigiamsat = a.Cothietbigiamsat,
                             Dacapphep = a.Dacapphep,
                             Thoigianxaydung = a.Thoigianxaydung,
                             Thoigianvanhanh = a.Thoigianvanhanh,
                             Matinh = a.Matinh,
                             Mahuyen = a.Mahuyen,
                             Maxa = a.Maxa,
                         });

            var data = await query.OrderByDescending(o => o.Id)
                                  .ToListAsync();

            return data;
        }

        /// <summary>
        /// Insert SwDiemxathai
        /// </summary>
        /// <param name="swDiemxathai"></param>
        /// <returns></returns>
        public async Task<SwDiemxathai> InsertSwDiemxathai(SwDiemxathai swDiemxathai)
        {
            if (swDiemxathai == null)
                throw new ArgumentNullException(nameof(swDiemxathai));

            _efRepository.Insert(swDiemxathai);
            await _unitOfWork.SaveChangeAsync();

            return swDiemxathai;
        }

        /// <summary>
        /// Update SwDiemxathai
        /// </summary>
        /// <param name="swDiemxathai"></param>
        /// <returns></returns>
        public async Task<SwDiemxathai> UpdateSwDiemxathai(SwDiemxathai swDiemxathai)
        {
            if (swDiemxathai == null)
                throw new ArgumentNullException(nameof(swDiemxathai));

            _efRepository.Update(swDiemxathai);
            await _unitOfWork.SaveChangeAsync();

            return swDiemxathai;
        }

        /// <summary>
        /// Delete SwDiemxathai
        /// </summary>
        /// <param name="swDiemxathai"></param>
        /// <returns></returns>
        public async Task<SwDiemxathai> DeleteSwDiemxathai(SwDiemxathai swDiemxathai)
        {
            if (swDiemxathai == null)
                throw new ArgumentNullException(nameof(swDiemxathai));

            _efRepository.Update(swDiemxathai);
            await _unitOfWork.SaveChangeAsync();

            return swDiemxathai;
        }

        #endregion
    }
}
