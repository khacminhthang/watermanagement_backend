﻿using System.Threading.Tasks;
using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using System;
using WaterManagement.Models.Models;
using WaterManagement.Models.ViewModels;
using WaterManagement.Utils.Common;
using WaterManagement.Utils.Common.Enums;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;


namespace WaterManagement.DAO.DAO
{
    public class SwMaybomdiemkhaithacDAO : ISwMaybomdiemkhaithacDAO
    {

        #region Fields
        private readonly IEfRepository<SwMaybomdiemkhaithac> _efRepository;
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region ctor
        public SwMaybomdiemkhaithacDAO(IEfRepository<SwMaybomdiemkhaithac> swMaybomdiemkhaithacRepo, IUnitOfWork unitOfWork)
        {
            _efRepository = swMaybomdiemkhaithacRepo;
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Method
        public async Task<SwMaybomdiemkhaithac> DeleteSwMaybomdiemkhaithac(SwMaybomdiemkhaithac model)
        {
            if(model == null)
                throw new ArgumentNullException(nameof(model));
            _efRepository.Update(model);
            await _unitOfWork.SaveChangeAsync();
            return model;
        }

        public async Task<IList<SwMaybomdiemkhaithac>> GetAll(int id)
        {
            if (id <= 0)
                return null;
            var dbContext = _unitOfWork.DbContext;
            dbContext.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            var query = (from a in dbContext.Set<SwMaybomdiemkhaithac>()
                         where a.DeletedAt == 0 && a.IdDiemkhaithac == id
                         select new SwMaybomdiemkhaithac
                         {
                            Id = a.Id,
                            IdDiemkhaithac = a.IdDiemkhaithac,
                            Loaimaybom = a.Loaimaybom,
                            Dosaulapdat = a.Dosaulapdat,
                            Thoigianlapdat = a.Thoigianlapdat,
                            Congsuat = a.Congsuat,
                            Note = a.Note
                         });
            var data = await query.OrderByDescending(o => o.Id).ToListAsync();
            return data;
        }

        public async Task<SwMaybomdiemkhaithac> GetById(int id)
        {
            if (id <= 0)
                return null;
            return await _efRepository.SingleOrDefaultAsync(a => a.Id == id && a.DeletedAt == 0);
        }

        public async Task<SwMaybomdiemkhaithac> InsertSwMaybomdiemkhaithac(SwMaybomdiemkhaithac model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));
            _efRepository.Insert(model);
            await _unitOfWork.SaveChangeAsync();
            return model;
        }

        public async Task<SwMaybomdiemkhaithac> UpdateSwMaybomdiemkhaithac(SwMaybomdiemkhaithac model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));
            _efRepository.Update(model);
            await _unitOfWork.SaveChangeAsync();
            return model;
        }
        #endregion
    }
}
