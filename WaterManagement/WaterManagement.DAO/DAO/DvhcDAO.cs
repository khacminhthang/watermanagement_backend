﻿using WaterManagement.Interface.IDAO;
using WaterManagement.Models.Core.Repositories;
using WaterManagement.Models.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WaterManagement.DAO.DVHC
{
    public class DvhcDAO : IDvhcDAO
    {
        #region Fields

        private readonly IEfRepository<Dvhc> _efRepository;
        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor

        public DvhcDAO(IEfRepository<Dvhc> efRepository,
                        IUnitOfWork unitOfWork)
        {
            _efRepository = efRepository;
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region Method

        /// <summary>
        /// Get all 
        /// </summary>
        /// <param name="expression">expression</param>
        /// <returns></returns>
        public async Task<IList<Dvhc>> GetAll(Expression<Func<Dvhc, bool>> expression)
        {
            var query = _efRepository.TableNoTracking.Where(expression);

            var data = await query.OrderBy(o => o.Id)
                                  .ToListAsync();

            return data;
        }

        /// <summary>
        ///  Get 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public async Task<Dvhc> Get(Expression<Func<Dvhc, bool>> expression)
        {
            return await _efRepository.SingleOrDefaultAsync(expression);
        }

        /// <summary>
        /// Insert Dvhc
        /// </summary>
        /// <param name="dvhc"></param>
        /// <returns></returns>
        public async Task InsertDvhc(Dvhc dvhc)
        {
            if (dvhc == null)
                throw new ArgumentNullException(nameof(dvhc));

            _efRepository.Insert(dvhc);
            await _unitOfWork.SaveChangeAsync();
        }

        /// <summary>
        /// Update Dvhc
        /// </summary>
        /// <param name="dvhc"></param>
        /// <returns></returns>
        public async Task UpdateDvhc(Dvhc dvhc)
        {
            if (dvhc == null)
                throw new ArgumentNullException(nameof(dvhc));

            _efRepository.Update(dvhc);
            await _unitOfWork.SaveChangeAsync();
        }

        /// <summary>
        /// Delete Dvhc
        /// </summary>
        /// <param name="dvhc"></param>
        /// <returns></returns>
        public async Task DeleteDvhc(Dvhc dvhc)
        {
            if (dvhc == null)
                throw new ArgumentNullException(nameof(dvhc));

            _efRepository.Delete(dvhc);
            await _unitOfWork.SaveChangeAsync();
        }

        /// <summary>
        /// DeleteRange
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public async Task DeleteRange(IList<Dvhc> list)
        {

            _efRepository.DeleteRange(list);
            // commit change
            await _unitOfWork.SaveChangeAsync();
        }

        #endregion
    }
}
