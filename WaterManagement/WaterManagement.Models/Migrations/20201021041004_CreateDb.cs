﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NetTopologySuite.Geometries;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace WaterManagement.Models.Migrations
{
    public partial class CreateDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:PostgresExtension:postgis", ",,");

            migrationBuilder.CreateSequence(
                name: "sw_diemkhaithac_id_seq");

            migrationBuilder.CreateSequence(
                name: "sw_diemxathai_id_seq");

            migrationBuilder.CreateSequence(
                name: "sw_maybomdiemkhaithac_id_seq");

            migrationBuilder.CreateSequence(
                name: "sw_maybomtrambom_id_seq");

            migrationBuilder.CreateSequence(
                name: "sw_trambom_id_seq");

            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    FullName = table.Column<string>(type: "varchar(150)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "dvhc",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    tendvhc = table.Column<string>(maxLength: 150, nullable: true),
                    parentid = table.Column<int>(nullable: true),
                    matinh = table.Column<string>(maxLength: 40, nullable: true),
                    mahuyen = table.Column<string>(maxLength: 40, nullable: true),
                    maxa = table.Column<string>(maxLength: 40, nullable: true),
                    status = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dvhc", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "g_canhan",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    hovaten = table.Column<string>(maxLength: 40, nullable: true),
                    diachi = table.Column<string>(maxLength: 255, nullable: true),
                    matinh = table.Column<string>(maxLength: 40, nullable: true),
                    mahuyen = table.Column<string>(maxLength: 40, nullable: true),
                    maxa = table.Column<string>(maxLength: 40, nullable: true),
                    sodienthoai = table.Column<string>(maxLength: 40, nullable: true),
                    email = table.Column<string>(maxLength: 40, nullable: true),
                    loaigiayto = table.Column<string>(maxLength: 40, nullable: true),
                    socmthochieu = table.Column<string>(maxLength: 40, nullable: true),
                    note = table.Column<string>(nullable: true, defaultValueSql: "'FALSE'::text"),
                    img_link = table.Column<string>(nullable: true),
                    created_at = table.Column<string>(fixedLength: true, maxLength: 40, nullable: true),
                    updated_at = table.Column<string>(fixedLength: true, maxLength: 40, nullable: true),
                    deleted_at = table.Column<short>(nullable: true, defaultValueSql: "0")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_g_canhan", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "g_congty",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    tencongty = table.Column<string>(maxLength: 255, nullable: true),
                    diachi = table.Column<string>(maxLength: 255, nullable: true),
                    matinh = table.Column<string>(maxLength: 40, nullable: true),
                    mahuyen = table.Column<string>(maxLength: 40, nullable: true),
                    maxa = table.Column<string>(maxLength: 40, nullable: true),
                    sodienthoai = table.Column<string>(maxLength: 40, nullable: true),
                    email = table.Column<string>(maxLength: 255, nullable: true),
                    note = table.Column<string>(nullable: true),
                    img_link = table.Column<string>(nullable: true),
                    created_at = table.Column<string>(fixedLength: true, maxLength: 40, nullable: true),
                    updated_at = table.Column<string>(fixedLength: true, maxLength: 40, nullable: true),
                    deleted_at = table.Column<short>(nullable: true, defaultValueSql: "0")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_g_congty", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "g_coquantochuc",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    idcha = table.Column<int>(nullable: true),
                    tencoquan = table.Column<string>(maxLength: 255, nullable: true),
                    diachi = table.Column<string>(maxLength: 255, nullable: true),
                    sodienthoai = table.Column<string>(maxLength: 40, nullable: true),
                    email = table.Column<string>(maxLength: 40, nullable: true),
                    matinh = table.Column<string>(maxLength: 40, nullable: true),
                    mahuyen = table.Column<string>(maxLength: 40, nullable: true),
                    maxa = table.Column<string>(maxLength: 40, nullable: true),
                    note = table.Column<string>(nullable: true),
                    img_link = table.Column<string>(nullable: true),
                    created_at = table.Column<string>(fixedLength: true, maxLength: 40, nullable: true),
                    updated_at = table.Column<string>(fixedLength: true, maxLength: 40, nullable: true),
                    deleted_at = table.Column<short>(nullable: true, defaultValueSql: "0")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_g_coquantochuc", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "g_dlgiamsatts",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    id_diem = table.Column<int>(nullable: false),
                    mucdich = table.Column<string>(maxLength: 40, nullable: true),
                    id_thamsodo = table.Column<int>(nullable: true),
                    phuongphapdo = table.Column<string>(maxLength: 40, nullable: true),
                    namdo = table.Column<int>(nullable: true),
                    thangdo = table.Column<int>(nullable: true),
                    ngaydo = table.Column<int>(nullable: true),
                    thoigiando = table.Column<string>(maxLength: 40, nullable: true, defaultValueSql: "CURRENT_DATE"),
                    full_time = table.Column<DateTime>(nullable: true),
                    ketquado = table.Column<double>(nullable: true),
                    loaisolieu = table.Column<string>(maxLength: 40, nullable: true),
                    note = table.Column<string>(nullable: true),
                    created_at = table.Column<string>(maxLength: 40, nullable: true),
                    updated_at = table.Column<string>(maxLength: 40, nullable: true),
                    deleted_at = table.Column<short>(nullable: true, defaultValueSql: "0")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_g_dlgiamsatts", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "g_dlquantracts",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    id_tramquantrac = table.Column<int>(nullable: true),
                    obj_key = table.Column<short>(nullable: true),
                    id_thamsodo = table.Column<int>(nullable: true),
                    phuongphapdo = table.Column<string>(maxLength: 40, nullable: true),
                    namdo = table.Column<int>(nullable: true),
                    thangdo = table.Column<int>(nullable: true),
                    ngaydo = table.Column<int>(nullable: true),
                    thoigiando = table.Column<string>(maxLength: 40, nullable: true),
                    full_time = table.Column<string>(maxLength: 40, nullable: true),
                    ketquado = table.Column<double>(nullable: true),
                    loaisolieu = table.Column<string>(maxLength: 40, nullable: true),
                    note = table.Column<string>(nullable: true),
                    created_at = table.Column<string>(maxLength: 40, nullable: true),
                    updated_at = table.Column<string>(maxLength: 40, nullable: true),
                    deleted_at = table.Column<short>(nullable: true, defaultValueSql: "0"),
                    status = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_g_dlquantracts", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "g_dlquantracts_his",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    id_tramquantrac = table.Column<int>(nullable: true),
                    obj_key = table.Column<short>(nullable: true),
                    id_thamsodo = table.Column<int>(nullable: true),
                    phuongphapdo = table.Column<string>(maxLength: 40, nullable: true),
                    namdo = table.Column<int>(nullable: true),
                    thangdo = table.Column<int>(nullable: true),
                    ngaydo = table.Column<int>(nullable: true),
                    thoigiando = table.Column<string>(maxLength: 40, nullable: true),
                    full_time = table.Column<string>(maxLength: 40, nullable: true),
                    ketquado = table.Column<double>(nullable: true),
                    loaisolieu = table.Column<string>(maxLength: 40, nullable: true),
                    note = table.Column<string>(nullable: true),
                    created_at = table.Column<string>(maxLength: 40, nullable: true),
                    updated_at = table.Column<string>(maxLength: 40, nullable: true),
                    deleted_at = table.Column<short>(nullable: true, defaultValueSql: "0"),
                    status = table.Column<short>(nullable: true, defaultValueSql: "0")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_g_dlquantracts_his", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "g_donvido",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false),
                    tendonvido = table.Column<string>(maxLength: 40, nullable: false),
                    kyhieudonvido = table.Column<string>(maxLength: 40, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_g_donvido", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "g_duan",
                columns: table => new
                {
                    id_project = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    project_code = table.Column<string>(nullable: true),
                    project_name = table.Column<string>(nullable: true),
                    signed_date = table.Column<string>(maxLength: 40, nullable: true),
                    signed_agency = table.Column<string>(nullable: true),
                    complete_date = table.Column<string>(maxLength: 40, nullable: true),
                    project_leader = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_g_duan", x => x.id_project);
                });

            migrationBuilder.CreateTable(
                name: "g_kiemtracongtrinh",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    id_obj = table.Column<int>(nullable: true),
                    obj_key = table.Column<short>(nullable: true),
                    tendotkiemtra = table.Column<string>(maxLength: 40, nullable: true),
                    thoigiankiemtra = table.Column<string>(maxLength: 40, nullable: true),
                    ketquado1 = table.Column<double>(nullable: true),
                    ketquado2 = table.Column<double>(nullable: true),
                    note = table.Column<string>(maxLength: 40, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_g_kiemtracongtrinh", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "g_loaisolieu",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    tenloaisolieu = table.Column<string>(maxLength: 255, nullable: true),
                    tenviettat = table.Column<string>(maxLength: 40, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_g_loaisolieu", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "g_media",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    id_obj = table.Column<int>(nullable: true),
                    link = table.Column<string>(nullable: true),
                    tieude = table.Column<string>(maxLength: 255, nullable: true),
                    thoigiankhoitao = table.Column<string>(maxLength: 40, nullable: true),
                    id_loaitailieu = table.Column<int>(nullable: true),
                    tacgia = table.Column<string>(maxLength: 40, nullable: true),
                    nguoiky = table.Column<string>(maxLength: 40, nullable: true),
                    note = table.Column<string>(nullable: true),
                    created_at = table.Column<string>(fixedLength: true, maxLength: 40, nullable: true),
                    updated_at = table.Column<string>(fixedLength: true, maxLength: 40, nullable: true),
                    deleted_at = table.Column<short>(nullable: true, defaultValueSql: "0"),
                    obj_key = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_g_media", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "g_nhomthamso",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    tennhom = table.Column<string>(maxLength: 255, nullable: true),
                    kyhieunhom = table.Column<string>(maxLength: 40, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_g_nhomthamso", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "g_obj_thamso",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    id_obj = table.Column<int>(nullable: true),
                    obj_key = table.Column<short>(nullable: true),
                    id_thamso = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_g_obj_thamso", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "g_objmedia",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    id_obj = table.Column<int>(nullable: true),
                    id_media = table.Column<int>(nullable: true),
                    created_at = table.Column<string>(fixedLength: true, maxLength: 40, nullable: true),
                    updated_at = table.Column<string>(fixedLength: true, maxLength: 40, nullable: true),
                    deleted_at = table.Column<short>(nullable: true),
                    obj_key = table.Column<short>(nullable: true),
                    media_type = table.Column<string>(maxLength: 40, nullable: true),
                    id_objmedia_type = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_g_objmedia", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "g_phantich",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    id_mau = table.Column<int>(nullable: true),
                    sohieuphantich = table.Column<string>(maxLength: 40, nullable: true),
                    nguoiphantich = table.Column<string>(maxLength: 40, nullable: true),
                    nguoikiemtra = table.Column<string>(maxLength: 40, nullable: true),
                    thoigianphantich = table.Column<string>(maxLength: 40, nullable: true),
                    note = table.Column<string>(nullable: true),
                    created_at = table.Column<string>(maxLength: 40, nullable: true),
                    updated_at = table.Column<string>(maxLength: 40, nullable: true),
                    deleted_at = table.Column<short>(nullable: true, defaultValueSql: "0")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_g_phantich", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "g_thamso",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    tenthamso = table.Column<string>(nullable: true),
                    kyhieuthamso = table.Column<string>(nullable: true),
                    id_nhomthamso = table.Column<int>(nullable: true),
                    id_donvidomacdinh = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_g_thamso", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "g_tieuchuan",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    sohieutieuchuan = table.Column<string>(maxLength: 40, nullable: true),
                    tentieuchuan = table.Column<string>(maxLength: 255, nullable: true),
                    thoigianbanhanh = table.Column<string>(maxLength: 40, nullable: true),
                    coquanbanhanh = table.Column<string>(maxLength: 255, nullable: true),
                    hientrang = table.Column<string>(maxLength: 40, nullable: true),
                    id_tailieu = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_g_tieuchuan", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "g_tieuchuanchatluong",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    id_tieuchuan = table.Column<int>(nullable: true),
                    id_thamso = table.Column<int>(nullable: true),
                    id_donvido = table.Column<int>(nullable: true),
                    gioihantren = table.Column<double>(nullable: true),
                    gioihanduoi = table.Column<double>(nullable: true),
                    phuongphapthu = table.Column<string>(maxLength: 255, nullable: true),
                    captieuchuan = table.Column<string>(maxLength: 40, nullable: true),
                    mucdogiamsat = table.Column<string>(maxLength: 40, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_g_tieuchuanchatluong", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "gw_cautrucgieng",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    id_gieng = table.Column<int>(nullable: true),
                    obj_key = table.Column<short>(nullable: true),
                    chieusautu = table.Column<double>(nullable: true),
                    chieusauden = table.Column<double>(nullable: true),
                    duongkinh = table.Column<double>(nullable: true),
                    loaiong = table.Column<string>(maxLength: 40, nullable: true),
                    note = table.Column<string>(nullable: true),
                    created_at = table.Column<string>(maxLength: 40, nullable: true),
                    updated_at = table.Column<string>(maxLength: 40, nullable: true),
                    deleted_at = table.Column<short>(nullable: true, defaultValueSql: "0")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_gw_cautrucgieng", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "gw_diatanggieng",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    id_gieng = table.Column<int>(nullable: true),
                    obj_key = table.Column<short>(nullable: true),
                    chieusautu = table.Column<double>(nullable: true),
                    chieusauden = table.Column<double>(nullable: true),
                    diatang = table.Column<string>(maxLength: 40, nullable: true),
                    vatlieu = table.Column<string>(maxLength: 40, nullable: true),
                    note = table.Column<string>(nullable: true),
                    created_at = table.Column<string>(maxLength: 40, nullable: true),
                    updated_at = table.Column<string>(maxLength: 40, nullable: true),
                    deleted_at = table.Column<short>(nullable: true, defaultValueSql: "0")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_gw_diatanggieng", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "gw_giengkhaithac",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    geom = table.Column<Geometry>(nullable: true),
                    sohieugieng = table.Column<string>(maxLength: 40, nullable: true),
                    tengieng = table.Column<string>(maxLength: 40, nullable: true),
                    toadox = table.Column<double>(nullable: true),
                    toadoy = table.Column<double>(nullable: true),
                    caodoz = table.Column<double>(nullable: true),
                    srid = table.Column<int>(nullable: true),
                    vitri = table.Column<string>(maxLength: 255, nullable: true),
                    matinh = table.Column<string>(maxLength: 40, nullable: true),
                    mahuyen = table.Column<string>(maxLength: 40, nullable: true),
                    maxa = table.Column<string>(maxLength: 40, nullable: true),
                    thoigianxaydung = table.Column<string>(maxLength: 40, nullable: true),
                    thoigianvanhanh = table.Column<string>(maxLength: 40, nullable: true),
                    id_donviquanly = table.Column<int>(nullable: true),
                    id_chusudung = table.Column<int>(nullable: true),
                    id_mucdichsudungchinh = table.Column<int>(nullable: true),
                    luuluongchopheplonnhat = table.Column<double>(nullable: true, defaultValueSql: "0"),
                    cothietbigiamsat = table.Column<bool>(nullable: true),
                    id_tangchuanuockt = table.Column<int>(nullable: true),
                    chedokhaithac = table.Column<string>(maxLength: 40, nullable: true),
                    id_tram = table.Column<int>(nullable: true),
                    capgieng = table.Column<string>(maxLength: 40, nullable: true),
                    loaigieng = table.Column<string>(maxLength: 40, nullable: true),
                    chieusaugieng = table.Column<string>(maxLength: 40, nullable: true),
                    duongkinhgieng = table.Column<double>(nullable: true),
                    chieusaumucnuoctinh = table.Column<double>(nullable: true),
                    chieusaumucnuocdong = table.Column<double>(nullable: true),
                    chieusaudoanthunuoctu = table.Column<double>(nullable: true),
                    chieusaudoanthunuocden = table.Column<double>(nullable: true),
                    hientrang = table.Column<string>(maxLength: 255, nullable: true),
                    note = table.Column<string>(nullable: true),
                    img_link = table.Column<string>(nullable: true),
                    created_at = table.Column<string>(maxLength: 40, nullable: true),
                    updated_at = table.Column<string>(maxLength: 40, nullable: true),
                    deleted_at = table.Column<short>(nullable: true, defaultValueSql: "0"),
                    obj_key = table.Column<short>(nullable: true, defaultValueSql: "3"),
                    site_id = table.Column<string>(type: "character varying", nullable: true),
                    user_id = table.Column<string>(type: "character varying", nullable: true),
                    kieutoado = table.Column<string>(type: "character varying", nullable: true),
                    status = table.Column<string>(type: "character varying", nullable: true),
                    guid = table.Column<string>(type: "character varying", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_gw_giengkhaithac", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "gw_giengquantrac",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    geom = table.Column<Geometry>(nullable: true),
                    sohieugieng = table.Column<string>(maxLength: 40, nullable: true),
                    tengieng = table.Column<string>(maxLength: 40, nullable: true),
                    toadox = table.Column<double>(nullable: true),
                    toadoy = table.Column<double>(nullable: true),
                    caodoz = table.Column<double>(nullable: true),
                    srid = table.Column<int>(nullable: true),
                    vitri = table.Column<string>(maxLength: 255, nullable: true),
                    matinh = table.Column<string>(maxLength: 40, nullable: true),
                    mahuyen = table.Column<string>(maxLength: 40, nullable: true),
                    maxa = table.Column<string>(maxLength: 40, nullable: true),
                    thoigianxaydung = table.Column<string>(maxLength: 40, nullable: true),
                    thoigianvanhanh = table.Column<string>(maxLength: 40, nullable: true),
                    id_donviquanly = table.Column<int>(nullable: true),
                    phuongphapquantrac = table.Column<string>(maxLength: 40, nullable: true),
                    id_tangchuanuocqt = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    chedoquantrac = table.Column<string>(maxLength: 40, nullable: true),
                    id_tram = table.Column<int>(nullable: true),
                    chieusaugieng = table.Column<double>(nullable: true),
                    duongkinhgieng = table.Column<double>(nullable: true),
                    chieusaumucnuoctinh = table.Column<double>(nullable: true),
                    chieusaumucnuocdong = table.Column<double>(nullable: true),
                    chieusaudoanthunuoctu = table.Column<double>(nullable: true),
                    chieusaudoanthunuocden = table.Column<double>(nullable: true),
                    caodomieng = table.Column<double>(nullable: true),
                    tuyenquantrac = table.Column<string>(maxLength: 40, nullable: true),
                    mangquantrac = table.Column<string>(maxLength: 40, nullable: true),
                    id_luuvucsong = table.Column<int>(nullable: true),
                    chieucaoongbaove = table.Column<double>(nullable: true),
                    luuluong = table.Column<double>(nullable: true),
                    hathapmucnuoc = table.Column<double>(nullable: true),
                    hesotham = table.Column<double>(nullable: true),
                    hesodan = table.Column<double>(nullable: true),
                    capquanly = table.Column<string>(maxLength: 255, nullable: true),
                    hientrang = table.Column<string>(maxLength: 255, nullable: true),
                    note = table.Column<string>(nullable: true),
                    img_link = table.Column<string>(nullable: true),
                    created_at = table.Column<string>(maxLength: 40, nullable: true),
                    updated_at = table.Column<string>(maxLength: 40, nullable: true),
                    deleted_at = table.Column<short>(nullable: true, defaultValueSql: "0"),
                    obj_key = table.Column<short>(nullable: true, defaultValueSql: "1"),
                    guid = table.Column<string>(type: "character varying", nullable: true),
                    kieutoado = table.Column<string>(maxLength: 40, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_gw_giengquantrac", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "gw_tangchuanuoc",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    tentangchuanuoc = table.Column<string>(maxLength: 40, nullable: true),
                    kieutangchuanuoc = table.Column<string>(maxLength: 40, nullable: true),
                    hesotrunuoc = table.Column<double>(nullable: true),
                    hesotham = table.Column<double>(nullable: true),
                    bedaytangchua = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_gw_tangchuanuoc", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "gw_tram",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    geom = table.Column<Geometry>(nullable: true),
                    sohieutram = table.Column<string>(maxLength: 40, nullable: true),
                    tentram = table.Column<string>(maxLength: 40, nullable: true),
                    toadox = table.Column<double>(nullable: true),
                    toadoy = table.Column<double>(nullable: true),
                    caodoz = table.Column<double>(nullable: true),
                    srid = table.Column<int>(nullable: true),
                    vitri = table.Column<string>(maxLength: 255, nullable: true),
                    matinh = table.Column<string>(maxLength: 40, nullable: true),
                    mahuyen = table.Column<string>(maxLength: 40, nullable: true),
                    maxa = table.Column<string>(maxLength: 40, nullable: true),
                    thoigianxaydung = table.Column<string>(maxLength: 40, nullable: true),
                    thoigianvanhanh = table.Column<string>(maxLength: 40, nullable: true),
                    id_duan = table.Column<int>(nullable: true),
                    loaitram = table.Column<string>(maxLength: 40, nullable: true),
                    sogieng = table.Column<int>(nullable: true),
                    tongluuluong = table.Column<double>(nullable: true),
                    dientich = table.Column<double>(nullable: true),
                    id_donviquanly = table.Column<int>(nullable: true),
                    tuyenquantrac = table.Column<string>(maxLength: 40, nullable: true),
                    mangquantrac = table.Column<string>(maxLength: 40, nullable: true),
                    id_luuvucsong = table.Column<int>(nullable: true),
                    hientrang = table.Column<string>(maxLength: 255, nullable: true),
                    note = table.Column<string>(nullable: true),
                    img_link = table.Column<string>(nullable: true),
                    created_at = table.Column<string>(maxLength: 40, nullable: true),
                    updated_at = table.Column<string>(maxLength: 40, nullable: true),
                    deleted_at = table.Column<short>(nullable: true, defaultValueSql: "0"),
                    obj_key = table.Column<short>(nullable: true, defaultValueSql: "2")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_gw_tram", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "sw_diemkhaithac",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    geom = table.Column<Geometry>(nullable: true),
                    sohieudiem = table.Column<string>(maxLength: 40, nullable: true),
                    tendiem = table.Column<string>(maxLength: 40, nullable: true),
                    toadox = table.Column<double>(nullable: true),
                    toadoy = table.Column<double>(nullable: true),
                    caodoz = table.Column<double>(nullable: true),
                    srid = table.Column<int>(nullable: true),
                    vitri = table.Column<string>(maxLength: 255, nullable: true),
                    matinh = table.Column<string>(maxLength: 40, nullable: true),
                    mahuyen = table.Column<string>(maxLength: 40, nullable: true),
                    maxa = table.Column<string>(maxLength: 40, nullable: true),
                    thoigianxaydung = table.Column<string>(maxLength: 40, nullable: true),
                    thoigianvanhanh = table.Column<string>(maxLength: 40, nullable: true),
                    id_donviquanly = table.Column<int>(nullable: true),
                    id_chusudung = table.Column<int>(nullable: true),
                    id_mucdichsudungchinh = table.Column<int>(nullable: true),
                    luuluongchopheplonnhat = table.Column<double>(nullable: true, defaultValueSql: "0"),
                    cothietbigiamsat = table.Column<bool>(nullable: true),
                    id_vanhanh = table.Column<int>(nullable: true),
                    thoiluongkhaithac = table.Column<string>(fixedLength: true, maxLength: 40, nullable: true),
                    nhamay = table.Column<string>(maxLength: 255, nullable: true),
                    id_song = table.Column<int>(nullable: true),
                    hochua = table.Column<string>(maxLength: 255, nullable: true),
                    hotunhien = table.Column<string>(maxLength: 255, nullable: true),
                    dacapphep = table.Column<bool>(nullable: true),
                    note = table.Column<string>(nullable: true),
                    img_link = table.Column<string>(nullable: true),
                    created_at = table.Column<string>(maxLength: 40, nullable: true),
                    updated_at = table.Column<string>(maxLength: 40, nullable: true),
                    deleted_at = table.Column<short>(nullable: true, defaultValueSql: "0"),
                    obj_key = table.Column<short>(nullable: true),
                    id_body = table.Column<int>(nullable: true),
                    body_key = table.Column<short>(nullable: true),
                    guid = table.Column<string>(type: "character varying", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sw_diemkhaithac", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "sw_diemxathai",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    geom = table.Column<Geometry>(nullable: true),
                    sohieudiem = table.Column<string>(maxLength: 40, nullable: true),
                    tendiem = table.Column<string>(maxLength: 255, nullable: true),
                    toadox = table.Column<double>(nullable: true),
                    toadoy = table.Column<double>(nullable: true),
                    caodoz = table.Column<double>(nullable: true),
                    srid = table.Column<int>(nullable: true),
                    vitri = table.Column<string>(maxLength: 255, nullable: true),
                    matinh = table.Column<string>(maxLength: 40, nullable: true),
                    mahuyen = table.Column<string>(maxLength: 40, nullable: true),
                    maxa = table.Column<string>(maxLength: 40, nullable: true),
                    thoigianxaydung = table.Column<string>(maxLength: 40, nullable: true),
                    thoigianvanhanh = table.Column<string>(maxLength: 40, nullable: true),
                    id_donviquanly = table.Column<int>(nullable: true),
                    id_chusudung = table.Column<int>(nullable: true),
                    id_song = table.Column<int>(nullable: true),
                    thuyvuctiepnhan = table.Column<string>(maxLength: 255, nullable: true),
                    phuongthucxa = table.Column<string>(fixedLength: true, maxLength: 40, nullable: true),
                    hinhthucdan = table.Column<string>(maxLength: 40, nullable: true),
                    luuluongxatrungbinh = table.Column<double>(nullable: true),
                    luuluonglonnhatchophep = table.Column<double>(nullable: true),
                    quychuanapdung = table.Column<string>(fixedLength: true, maxLength: 40, nullable: true),
                    cothietbigiamsat = table.Column<bool>(nullable: true),
                    dacapphep = table.Column<bool>(nullable: true),
                    note = table.Column<string>(nullable: true, defaultValueSql: "'0'::text"),
                    img_link = table.Column<string>(nullable: true),
                    created_at = table.Column<string>(maxLength: 40, nullable: true),
                    updated_at = table.Column<string>(maxLength: 40, nullable: true),
                    deleted_at = table.Column<short>(nullable: true, defaultValueSql: "0"),
                    guid = table.Column<string>(type: "character varying", nullable: true),
                    obj_key = table.Column<short>(nullable: true),
                    id_body = table.Column<int>(nullable: true),
                    body_key = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sw_diemxathai", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "sw_maybomdiemkhaithac",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    id_diemkhaithac = table.Column<int>(nullable: true),
                    loaimaybom = table.Column<string>(maxLength: 40, nullable: true),
                    dosaulapdat = table.Column<int>(nullable: true),
                    thoigianlapdat = table.Column<string>(maxLength: 40, nullable: true),
                    congsuat = table.Column<double>(nullable: true),
                    note = table.Column<string>(nullable: true),
                    created_at = table.Column<string>(maxLength: 40, nullable: true),
                    updated_at = table.Column<string>(maxLength: 40, nullable: true),
                    deleted_at = table.Column<short>(nullable: true, defaultValueSql: "0")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sw_maybomdiemkhaithac", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "sw_maybomtrambom",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    id_trambom = table.Column<int>(nullable: true),
                    loaimaybom = table.Column<string>(maxLength: 40, nullable: true),
                    thoigianlapdat = table.Column<string>(maxLength: 40, nullable: true),
                    congsuat = table.Column<double>(nullable: true),
                    note = table.Column<string>(nullable: true),
                    created_at = table.Column<string>(maxLength: 40, nullable: true),
                    updated_at = table.Column<string>(maxLength: 40, nullable: true),
                    deleted_at = table.Column<short>(nullable: true, defaultValueSql: "0")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sw_maybomtrambom", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "sw_trambom",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    geom = table.Column<Geometry>(nullable: true),
                    obj_key = table.Column<int>(nullable: true),
                    sohieutrambom = table.Column<string>(maxLength: 40, nullable: true),
                    tentrambom = table.Column<string>(maxLength: 40, nullable: true),
                    toadox = table.Column<double>(nullable: true),
                    toadoy = table.Column<double>(nullable: true),
                    caodoz = table.Column<double>(nullable: true),
                    srid = table.Column<int>(nullable: true),
                    vitri = table.Column<string>(maxLength: 40, nullable: true),
                    matinh = table.Column<string>(maxLength: 40, nullable: true),
                    mahuyen = table.Column<string>(maxLength: 40, nullable: true),
                    maxa = table.Column<string>(maxLength: 40, nullable: true),
                    thoigianxaydung = table.Column<string>(maxLength: 40, nullable: true),
                    thoigianvanhanh = table.Column<string>(maxLength: 40, nullable: true),
                    somaybomtieunuoc = table.Column<int>(nullable: true),
                    somaybomtuoinuoc = table.Column<int>(nullable: true),
                    khanangbomtuoi = table.Column<double>(nullable: true),
                    luuluongtuoithietke = table.Column<double>(nullable: true),
                    luuluongtuoithucte = table.Column<double>(nullable: true),
                    dientichtuoithietke = table.Column<double>(nullable: true),
                    dientichtuoithucte = table.Column<double>(nullable: true),
                    khanangbomtieu = table.Column<double>(nullable: true),
                    luuluongtieuthietke = table.Column<double>(nullable: true),
                    luuluongtieuthucte = table.Column<double>(nullable: true),
                    dientichtieuthietke = table.Column<double>(nullable: true),
                    dientichtieuthucte = table.Column<double>(nullable: true),
                    id_donvivanhanh = table.Column<int>(nullable: true),
                    id_vanhanh = table.Column<int>(nullable: true),
                    sohothietke = table.Column<int>(nullable: true),
                    sohothucte = table.Column<int>(nullable: true),
                    phantramcongsuathuuich = table.Column<double>(nullable: true),
                    id_song = table.Column<int>(nullable: true),
                    id_hochua = table.Column<int>(nullable: true),
                    id_hotunhien = table.Column<int>(nullable: true),
                    hientrang = table.Column<string>(maxLength: 40, nullable: true),
                    note = table.Column<string>(nullable: true),
                    img_link = table.Column<string>(nullable: true),
                    created_at = table.Column<string>(maxLength: 40, nullable: true),
                    updated_at = table.Column<string>(maxLength: 40, nullable: true),
                    deleted_at = table.Column<short>(nullable: true, defaultValueSql: "0"),
                    status = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sw_trambom", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "test",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    id_tramquantrac = table.Column<int>(nullable: true),
                    obj_key = table.Column<short>(nullable: true),
                    id_thamsodo = table.Column<int>(nullable: true),
                    phuongphapdo = table.Column<string>(maxLength: 40, nullable: true),
                    namdo = table.Column<int>(nullable: true),
                    thangdo = table.Column<int>(nullable: true),
                    ngaydo = table.Column<int>(nullable: true),
                    thoigiando = table.Column<string>(maxLength: 40, nullable: true),
                    full_time = table.Column<string>(maxLength: 40, nullable: true),
                    ketquado = table.Column<double>(nullable: true),
                    loaisolieu = table.Column<string>(maxLength: 40, nullable: true),
                    note = table.Column<string>(nullable: true),
                    created_at = table.Column<string>(maxLength: 40, nullable: true),
                    updated_at = table.Column<string>(maxLength: 40, nullable: true),
                    deleted_at = table.Column<short>(nullable: true, defaultValueSql: "0"),
                    status = table.Column<short>(nullable: true, defaultValueSql: "0")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_test", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "dvhc");

            migrationBuilder.DropTable(
                name: "g_canhan");

            migrationBuilder.DropTable(
                name: "g_congty");

            migrationBuilder.DropTable(
                name: "g_coquantochuc");

            migrationBuilder.DropTable(
                name: "g_dlgiamsatts");

            migrationBuilder.DropTable(
                name: "g_dlquantracts");

            migrationBuilder.DropTable(
                name: "g_dlquantracts_his");

            migrationBuilder.DropTable(
                name: "g_donvido");

            migrationBuilder.DropTable(
                name: "g_duan");

            migrationBuilder.DropTable(
                name: "g_kiemtracongtrinh");

            migrationBuilder.DropTable(
                name: "g_loaisolieu");

            migrationBuilder.DropTable(
                name: "g_media");

            migrationBuilder.DropTable(
                name: "g_nhomthamso");

            migrationBuilder.DropTable(
                name: "g_obj_thamso");

            migrationBuilder.DropTable(
                name: "g_objmedia");

            migrationBuilder.DropTable(
                name: "g_phantich");

            migrationBuilder.DropTable(
                name: "g_thamso");

            migrationBuilder.DropTable(
                name: "g_tieuchuan");

            migrationBuilder.DropTable(
                name: "g_tieuchuanchatluong");

            migrationBuilder.DropTable(
                name: "gw_cautrucgieng");

            migrationBuilder.DropTable(
                name: "gw_diatanggieng");

            migrationBuilder.DropTable(
                name: "gw_giengkhaithac");

            migrationBuilder.DropTable(
                name: "gw_giengquantrac");

            migrationBuilder.DropTable(
                name: "gw_tangchuanuoc");

            migrationBuilder.DropTable(
                name: "gw_tram");

            migrationBuilder.DropTable(
                name: "sw_diemkhaithac");

            migrationBuilder.DropTable(
                name: "sw_diemxathai");

            migrationBuilder.DropTable(
                name: "sw_maybomdiemkhaithac");

            migrationBuilder.DropTable(
                name: "sw_maybomtrambom");

            migrationBuilder.DropTable(
                name: "sw_trambom");

            migrationBuilder.DropTable(
                name: "test");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropSequence(
                name: "sw_diemkhaithac_id_seq");

            migrationBuilder.DropSequence(
                name: "sw_diemxathai_id_seq");

            migrationBuilder.DropSequence(
                name: "sw_maybomdiemkhaithac_id_seq");

            migrationBuilder.DropSequence(
                name: "sw_maybomtrambom_id_seq");

            migrationBuilder.DropSequence(
                name: "sw_trambom_id_seq");
        }
    }
}
