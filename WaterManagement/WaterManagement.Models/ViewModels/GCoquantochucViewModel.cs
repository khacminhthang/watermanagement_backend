﻿namespace WaterManagement.Models.ViewModels
{
    public partial class GCoquantochucViewModel
    {
        public int Id { get; set; }
        public int? Idcha { get; set; }
        public string Tencoquan { get; set; }
        public string Sodienthoai { get; set; }
        public string Email { get; set; }
        public string Diachi { get; set; }
        public string Tentinh { get; set; }
        public string Tenhuyen { get; set; }
        public string Tenxa { get; set; }
        public string matinh { get; set; }
        public string mahuyen { get; set; }
        public string maxa { get; set; }
        public string Note { get; set; }
        public string ImgLink { get; set; }
    }
}
