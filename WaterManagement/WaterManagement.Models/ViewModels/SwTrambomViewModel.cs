﻿

namespace WaterManagement.Models.ViewModels
{
    public partial class SwTrambomViewModel : CoordinateViewModel
    {
        public string Thoigianxaydung { get; set; }
        public string Thoigianvanhanh { get; set; }
        public int? Somaybomtieunuoc { get; set; }
        public int? Somaybomtuoinuoc { get; set; }
        public double? Khanangbomtuoi { get; set; }
        public double? Luuluongtuoithietke { get; set; }
        public double? Luuluongtuoithucte { get; set; }
        public double? Dientichtuoithietke { get; set; }
        public double? Dientichtuoithucte { get; set; }
        public double? Khanangbomtieu { get; set; }
        public double? Luuluongtieuthietke { get; set; }
        public double? Luuluongtieuthucte { get; set; }
        public double? Dientichtieuthietke { get; set; }
        public double? Dientichtieuthucte { get; set; }
        public int? IdDonviquanly { get; set; }
        public int? IdVanhanh { get; set; }
        public int? Sohothietke { get; set; }
        public int? Sohothucte { get; set; }
        public double? Phantramcongsuathuuich { get; set; }
        public int? Idsong { get; set; }
        public string Tensong { get; set; }
        public int? Idhochua { get; set; }
        public string Tenhochua { get; set; }
        public int? Idhotunhien { get; set; }
        public string Tenhotunhien { get; set; }
        public string Hientrang { get; set; }
        public string Tendonvivanhanh { get; set; }
        public object? Donviquanly { get; set; }
        public int? ObjKey { get; set; }
    }
}
