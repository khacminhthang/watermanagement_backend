﻿using WaterManagement.Models.Models;

namespace WaterManagement.Models.ViewModels
{
    public partial class GCongtyViewModel : GCongty
    {
        public string Tentinh { get; set; }
        public string Tenhuyen { get; set; }
        public string Tenxa { get; set; }
    }
}
