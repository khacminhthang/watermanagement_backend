﻿
namespace WaterManagement.Models.ViewModels
{
    public partial class CoordinateViewModel
    {
        public int Id { get; set; }
        public double Geox { get; set; }
        public double Geoy { get; set; }
        public string Sohieudiem { get; set; }
        public string Tendiem { get; set; }
        public double? Toadox { get; set; }
        public double? Toadoy { get; set; }
        public double? Caodoz { get; set; }
        public int? Srid { get; set; }
        public string Vitri { get; set; }
        public string Matinh { get; set; }
        public string Mahuyen { get; set; }
        public string Maxa { get; set; }
        public string Tentinh { get; set; }
        public string Tenhuyen { get; set; }
        public string Tenxa { get; set; }
        public int? IdDonviquanly { get; set; }
        public int? IdChusudung { get; set; }
        public int? IdMucdichsudungchinh { get; set; }
        public int? IdVanhanh { get; set; }
        public string Note { get; set; }
        public string ImgLink { get; set; }
        public int ObjType { get; set; }
    }
}
