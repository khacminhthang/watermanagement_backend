﻿namespace WaterManagement.Models.ViewModels
{
    public class GMediaViewModel
    {
        public int Id { get; set; }
        public int? IdObj { get; set; }
        public short? ObjKey { get; set; }
        public string Link { get; set; }
        public string Tieude { get; set; }
        public string Thoigiankhoitao { get; set; }
        public int? IdLoaitailieu { get; set; }
        public string Tacgia { get; set; }
        public string Nguoiky { get; set; }
        public string Note { get; set; }
        public string CreatedAt { get; set; }
        public string UpdatedAt { get; set; }
        public short? DeletedAt { get; set; }
        public string Kieutoado { get; set; }
        public string TenFile { get; set; }
        public string Type { get; set; }
        public string IconFile { get; set; }
        public bool Checked { get; set; }
    }
}
