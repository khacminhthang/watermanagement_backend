﻿
namespace WaterManagement.Models.ViewModels
{
    public partial class GwGiengkhaithacViewModel : CoordinateViewModel
    {
        public string Tenchusudung { get; set; }
        public string Tentangchuanuoc { get; set; }
        public string Tentram { get; set; }
        public string Thoigianxaydung { get; set; }
        public string Thoigianvanhanh { get; set; }
        public double? Luuluongchopheplonnhat { get; set; }
        public bool? Cothietbigiamsat { get; set; }
        public string Chedokhaithac { get; set; }
        public string Capgieng { get; set; }
        public string Loaigieng { get; set; }
        public string Chieusaugieng { get; set; }
        public double? Duongkinhgieng { get; set; }
        public double? Chieusaumucnuoctinh { get; set; }
        public double? Chieusaumucnuocdong { get; set; }
        public double? Chieusaudoanthunuoctu { get; set; }
        public double? Chieusaudoanthunuocden { get; set; }
        public string Hientrang { get; set; }
        public string Sohieugieng { get; set; }
        public string Tengieng { get; set; }
        public string Donviquanly { get; set; }
        public int? ObjKey { get; set; }
        public int? IdTangchuanuockt { get; set; }
        public int? IdTram { get; set; }

    }
}
