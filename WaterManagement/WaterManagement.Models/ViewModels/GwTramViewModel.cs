﻿
namespace WaterManagement.Models.ViewModels
{
    public partial class GwTramViewModel : CoordinateViewModel
    {
        public string Tenduan { get; set; }
        public string Tenluuvuc { get; set; }
        public string Loaitram { get; set; }
        public int? Sogieng { get; set; }
        public double? Tongluuluong { get; set; }
        public double? Dientich { get; set; }
        public string Hientrang { get; set; }
        public string Thoigianxaydung { get; set; }
        public string Thoigianvanhanh { get; set; }
        public string Mangquantrac { get; set; }
        public string Tuyenquantrac { get; set; }
        public string Ten { get; set; }
        public string Sohieu { get; set; }
        public string Donviquanly { get; set; }
        public short? ObjKey { get; set; }
    }
}
