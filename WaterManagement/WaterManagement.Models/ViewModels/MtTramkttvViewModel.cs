﻿namespace WaterManagement.Models.ViewModels
{
    public partial class MtTramkttvViewModel : CoordinateViewModel
    {
        public string Thoigianxaydung { get; set; }
        public string Thoigianvanhanh { get; set; }
        public string Doituongquantracgiamsat { get; set; }
        public string Hientrang { get; set; }
    }
}
