﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WaterManagement.Models.ViewModels
{
    public class GDlquantractsViewModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Gets or sets the Fulltime
        /// </summary>
        public string FullTime { get; set; }

        /// <summary>
        /// Gets or sets the Ketquado
        /// </summary>
        public double Ketquado { get; set; }

        /// <summary>
        /// Gets or sets the Thangdo
        /// </summary>
        public int? Thangdo { get; set; }

        /// <summary>
        /// Gets or sets the Namdo
        /// </summary>
        public int? Namdo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? IdTramquantrac { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public short? ObjKey { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? IdThamsodo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Phuongphapdo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? Ngaydo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Thoigiando { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Loaisolieu { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Note { get; set; }
        /// <summary>
        /// AvgKetquado
        /// </summary>
        public double? AvgKetquado { get; set; }

    }
}

