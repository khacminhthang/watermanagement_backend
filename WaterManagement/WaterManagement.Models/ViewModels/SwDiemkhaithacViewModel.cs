﻿
namespace WaterManagement.Models.ViewModels
{
    public partial class SwDiemkhaithacViewModel : CoordinateViewModel
    {
        public string Tenchusudung { get; set; }
        public string Tendonvivanhanh { get; set; }
        public string Nhamay { get; set; }
        public string Thoigianxaydung { get; set; }
        public string Thoigianvanhanh { get; set; }
        public double? Luuluongchopheplonnhat { get; set; }
        public bool? Cothietbigiamsat { get; set; }
        public string Thoiluongkhaithac { get; set; }
        public bool? Dacapphep { get; set; }

    }
}
