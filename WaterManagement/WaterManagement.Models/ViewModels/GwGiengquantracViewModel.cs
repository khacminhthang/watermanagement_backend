﻿
namespace WaterManagement.Models.ViewModels
{
    public partial class GwGiengquantracViewModel : CoordinateViewModel
    {
        public string Tentram { get; set; }
        public string Tentangchuanuoc { get; set; }
        public string Tenluuvuc { get; set; }
        public string Thoigianxaydung { get; set; }
        public string Thoigianvanhanh { get; set; }
        public string Phuongphapquantrac { get; set; }
        public string Chedoquantrac { get; set; }
        public double? Chieusaugieng { get; set; }
        public double? Duongkinhgieng { get; set; }
        public double? Chieusaumucnuoctinh { get; set; }
        public double? Chieusaumucnuocdong { get; set; }
        public double? Chieusaudoanthunuoctu { get; set; }
        public double? Chieusaudoanthunuocden { get; set; }
        public double? Caodomieng { get; set; }
        public double? Chieucaoongbaove { get; set; }
        public double? Luuluong { get; set; }
        public double? Hathapmucnuoc { get; set; }
        public double? Hesotham { get; set; }
        public double? Hesodan { get; set; }
        public string Capquanly { get; set; }
        public string Hientrang { get; set; }
        public string Tuyenquantrac { get; set; }
        public string Mangquantrac { get; set; }
        public string Tengieng { get; set; }
        public string Sohieugieng { get; set; }
        public string Donviquanly { get; set; }
        public int? IdTangchuanuocqt { get; set; }
        public int? IdLuuvucsong { get; set; }
        public int? IdTram { get; set; }
        public short? ObjKey { get; set; }
    }
}
