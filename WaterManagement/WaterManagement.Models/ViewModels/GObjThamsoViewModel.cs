﻿namespace WaterManagement.Models.ViewModels
{
    public class GObjThamsoViewModel
    {
        public int Id { get; set; }
        public int? IdObj { get; set; }
        public short? ObjKey { get; set; }
        public int? Idthamso { get; set; }
        public string Tenthamso { get; set; }
        public string Kyhieuthamso { get; set; }
        public string Tennhom { get; set; }
        public string Tendonvido { get; set; }
    }
}
