﻿
namespace WaterManagement.Models.ViewModels
{
    public partial class SwDiemxathaiViewModel : CoordinateViewModel
    {
        public string Tenchusudung { get; set; }
        public string Tensong { get; set; }
        public string Thuyvuctiepnhan { get; set; }
        public string Phuongthucxa { get; set; }
        public string Hinhthucdan { get; set; }
        public double? Luuluongxatrungbinh { get; set; }
        public double? Luuluonglonnhatchophep { get; set; }
        public string Quychuanapdung { get; set; }
        public bool? Cothietbigiamsat { get; set; }
        public bool? Dacapphep { get; set; }
        public string Thoigianxaydung { get; set; }
        public string Thoigianvanhanh { get; set; }
    }
}
