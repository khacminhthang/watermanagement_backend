﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace WaterManagement.Models.Core.Repositories
{
    /// <summary>
    /// UnitOfWork 
    /// </summary>
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        /// <summary>
        /// Disposed default false
        /// </summary>
        private bool _disposed = false;

        /// <summary>
        /// Database Context
        /// </summary>
        public DbContext DbContext { get; }

        #region Ctor

        public UnitOfWork(DbContext dbContext)
        {
            DbContext = dbContext;
        }

        #endregion

        #region Method

        /// <summary>
        /// Sync Save Change
        /// </summary>
        /// <returns></returns>
        public int SaveChange()
        {
            var result = DbContext.SaveChanges();
            return result;
        }

        /// <summary>
        /// Asyn  Save Change
        /// </summary>
        /// <returns></returns>
        public async Task<int> SaveChangeAsync()
        {
            var result = await DbContext.SaveChangesAsync();
            return result;
        }

        /// <summary>
        /// Dispose
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                DbContext?.Dispose();

            _disposed = true;
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
