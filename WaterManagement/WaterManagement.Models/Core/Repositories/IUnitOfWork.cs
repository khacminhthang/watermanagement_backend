﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace WaterManagement.Models.Core.Repositories
{
    /// <summary>
    /// IUnitOfWork
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// DbContext
        /// </summary>
        DbContext DbContext { get; }

        /// <summary>
        /// Sync SaveChange
        /// </summary>
        /// <returns></returns>
        int SaveChange();

        /// <summary>
        /// Async SaveChange
        /// </summary>
        /// <returns></returns>
        Task<int> SaveChangeAsync();
    }
}
