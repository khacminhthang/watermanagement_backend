﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WaterManagement.Models.RequestModel
{
    public class ChangePassWordRequest
    {
        public string id { get; set; }
        public string newpassword { get; set; }

    }
}
