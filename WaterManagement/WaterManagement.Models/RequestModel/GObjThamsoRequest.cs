﻿using System.Collections.Generic;

namespace WaterManagement.Models.RequestModel
{
    public class GObjThamsoRequest
    {

        /// <summary>
        /// Gets or sets the identifier
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Json input
        /// </summary>
        public List<JsonInput> JsonParameter { get; set; }

        /// <summary>
        /// Input
        /// </summary>
        public class JsonInput
        {
            /// <summary>
            /// Gets or sets the object table identifier
            /// </summary>
            public int IdObj { get; set; }

            /// <summary>
            /// Gets or sets the objKey
            /// </summary>
            public short ObjKey { get; set; }

            /// <summary>
            /// Gets or sets the Thamso identifier
            /// </summary>
            public int IdThamso { get; set; }
        }
    }
}
