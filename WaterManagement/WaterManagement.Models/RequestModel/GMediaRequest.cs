﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WaterManagement.Models.RequestModel
{
    public class GMediaRequest
    {
        /// <summary>
        /// JsonParameter
        /// </summary>
        public List<JsonInput> JsonParameter { get; set; }

        /// <summary>
        /// Input
        /// </summary>
        public class JsonInput
        {
            /// <summary>
            /// Base64
            /// </summary>
            public String Base64 { get; set; }
            public String Type { get; set; }
        }
    }
}
