﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WaterManagement.Models.RequestModel
{
    public class CreateGObjMedia
    {
        public int Id { get; set; }
        public int? IdObj { get; set; }
        public short? ObjKey { get; set; }
        public string MediaType { get; set; }
        public short? IdObjmediaType { get; set; }
        public int[] ListIdMedia { get; set; }
    }
}
