﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WaterManagement.Models.RequestModel
{
    public class LoginModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
