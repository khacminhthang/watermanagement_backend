﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace WaterManagement.Models.RequestModel
{
    public class UploadDlquantractsRequest
    {
        public IFormFile File { get; set; }
        public int IdTramquantrac { get; set; }
        public short ObjKey { get; set; }
        public int IdThamsodo { get; set; }
    }
}
