﻿using System;
using System.Collections.Generic;

namespace WaterManagement.Models.Models
{
    public partial class SwMaybomtrambom
    {
        public int Id { get; set; }
        public int? IdTrambom { get; set; }
        public string Loaimaybom { get; set; }
        public string Thoigianlapdat { get; set; }
        public double? Congsuat { get; set; }
        public string Note { get; set; }
        public string CreatedAt { get; set; }
        public string UpdatedAt { get; set; }
        public short? DeletedAt { get; set; }
    }
}
