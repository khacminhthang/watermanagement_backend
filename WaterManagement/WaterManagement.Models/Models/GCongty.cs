﻿using System;
using System.Collections.Generic;

namespace WaterManagement.Models.Models
{
    public partial class GCongty
    {
        public int Id { get; set; }
        public string Tencongty { get; set; }
        public string Diachi { get; set; }
        public string Matinh { get; set; }
        public string Mahuyen { get; set; }
        public string Maxa { get; set; }
        public string Sodienthoai { get; set; }
        public string Email { get; set; }
        public string Note { get; set; }
        public string ImgLink { get; set; }
        public string CreatedAt { get; set; }
        public string UpdatedAt { get; set; }
        public short? DeletedAt { get; set; }
    }
}
