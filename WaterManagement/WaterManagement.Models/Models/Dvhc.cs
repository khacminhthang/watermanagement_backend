﻿using System;
using System.Collections.Generic;

namespace WaterManagement.Models.Models
{
    public partial class Dvhc
    {
        public int Id { get; set; }
        public string Tendvhc { get; set; }
        public int? Parentid { get; set; }
        public string Matinh { get; set; }
        public string Mahuyen { get; set; }
        public string Maxa { get; set; }
        public short? Status { get; set; }
    }
}
