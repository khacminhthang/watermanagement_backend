﻿using System;
using System.Collections.Generic;

namespace WaterManagement.Models.Models
{
    public partial class GDuan
    {
        public int IdProject { get; set; }
        public string ProjectCode { get; set; }
        public string ProjectName { get; set; }
        public string SignedDate { get; set; }
        public string SignedAgency { get; set; }
        public string CompleteDate { get; set; }
        public string ProjectLeader { get; set; }
    }
}
