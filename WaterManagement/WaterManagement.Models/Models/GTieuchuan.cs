﻿using System;
using System.Collections.Generic;

namespace WaterManagement.Models.Models
{
    public partial class GTieuchuan
    {
        public int Id { get; set; }
        public string Sohieutieuchuan { get; set; }
        public string Tentieuchuan { get; set; }
        public string Thoigianbanhanh { get; set; }
        public string Coquanbanhanh { get; set; }
        public string Hientrang { get; set; }
        public int? IdTailieu { get; set; }
    }
}
