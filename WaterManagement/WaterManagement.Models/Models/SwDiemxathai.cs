﻿using System;
using System.Collections.Generic;
using NetTopologySuite.Geometries;

namespace WaterManagement.Models.Models
{
    public partial class SwDiemxathai
    {
        public int Id { get; set; }
        public Geometry Geom { get; set; }
        public string Sohieudiem { get; set; }
        public string Tendiem { get; set; }
        public double? Toadox { get; set; }
        public double? Toadoy { get; set; }
        public double? Caodoz { get; set; }
        public int? Srid { get; set; }
        public string Vitri { get; set; }
        public string Matinh { get; set; }
        public string Mahuyen { get; set; }
        public string Maxa { get; set; }
        public string Thoigianxaydung { get; set; }
        public string Thoigianvanhanh { get; set; }
        public int? IdDonviquanly { get; set; }
        public int? IdChusudung { get; set; }
        public int? IdSong { get; set; }
        public string Thuyvuctiepnhan { get; set; }
        public string Phuongthucxa { get; set; }
        public string Hinhthucdan { get; set; }
        public double? Luuluongxatrungbinh { get; set; }
        public double? Luuluonglonnhatchophep { get; set; }
        public string Quychuanapdung { get; set; }
        public bool? Cothietbigiamsat { get; set; }
        public bool? Dacapphep { get; set; }
        public string Note { get; set; }
        public string ImgLink { get; set; }
        public string CreatedAt { get; set; }
        public string UpdatedAt { get; set; }
        public short? DeletedAt { get; set; }
        public string Guid { get; set; }
        public short? ObjKey { get; set; }
        public int? IdBody { get; set; }
        public short? BodyKey { get; set; }
    }
}
