﻿using System;
using System.Collections.Generic;

namespace WaterManagement.Models.Models
{
    public partial class GDlquantractsHis
    {
        public long Id { get; set; }
        public int? IdTramquantrac { get; set; }
        public short? ObjKey { get; set; }
        public int? IdThamsodo { get; set; }
        public string Phuongphapdo { get; set; }
        public int? Namdo { get; set; }
        public int? Thangdo { get; set; }
        public int? Ngaydo { get; set; }
        public string Thoigiando { get; set; }
        public string FullTime { get; set; }
        public double? Ketquado { get; set; }
        public string Loaisolieu { get; set; }
        public string Note { get; set; }
        public string CreatedAt { get; set; }
        public string UpdatedAt { get; set; }
        public short? DeletedAt { get; set; }
        public short? Status { get; set; }
    }
}
