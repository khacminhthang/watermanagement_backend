﻿using System;
using System.Collections.Generic;

namespace WaterManagement.Models.Models
{
    public partial class GObjmedia
    {
        public int Id { get; set; }
        public int? IdObj { get; set; }
        public int? IdMedia { get; set; }
        public string CreatedAt { get; set; }
        public string UpdatedAt { get; set; }
        public short? DeletedAt { get; set; }
        public short? ObjKey { get; set; }
        public string MediaType { get; set; }
        public short? IdObjmediaType { get; set; }
    }
}
