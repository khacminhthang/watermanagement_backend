﻿using System;
using System.Collections.Generic;

namespace WaterManagement.Models.Models
{
    public partial class GThamso
    {
        public int Id { get; set; }
        public string Tenthamso { get; set; }
        public string Kyhieuthamso { get; set; }
        public int? IdNhomthamso { get; set; }
        public int? IdDonvidomacdinh { get; set; }
    }
}
