﻿using System;
using System.Collections.Generic;

namespace WaterManagement.Models.Models
{
    public partial class GTieuchuanchatluong
    {
        public int Id { get; set; }
        public int? IdTieuchuan { get; set; }
        public int? IdThamso { get; set; }
        public int? IdDonvido { get; set; }
        public double? Gioihantren { get; set; }
        public double? Gioihanduoi { get; set; }
        public string Phuongphapthu { get; set; }
        public string Captieuchuan { get; set; }
        public string Mucdogiamsat { get; set; }
    }
}
