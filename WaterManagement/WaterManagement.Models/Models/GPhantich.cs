﻿using System;
using System.Collections.Generic;

namespace WaterManagement.Models.Models
{
    public partial class GPhantich
    {
        public int Id { get; set; }
        public int? IdMau { get; set; }
        public string Sohieuphantich { get; set; }
        public string Nguoiphantich { get; set; }
        public string Nguoikiemtra { get; set; }
        public string Thoigianphantich { get; set; }
        public string Note { get; set; }
        public string CreatedAt { get; set; }
        public string UpdatedAt { get; set; }
        public short? DeletedAt { get; set; }
    }
}
