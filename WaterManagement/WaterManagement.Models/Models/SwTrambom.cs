﻿using System;
using System.Collections.Generic;
using NetTopologySuite.Geometries;

namespace WaterManagement.Models.Models
{
    public partial class SwTrambom
    {
        public int Id { get; set; }
        public Geometry Geom { get; set; }
        public int? ObjKey { get; set; }
        public string Sohieutrambom { get; set; }
        public string Tentrambom { get; set; }
        public double? Toadox { get; set; }
        public double? Toadoy { get; set; }
        public double? Caodoz { get; set; }
        public int? Srid { get; set; }
        public string Vitri { get; set; }
        public string Matinh { get; set; }
        public string Mahuyen { get; set; }
        public string Maxa { get; set; }
        public string Thoigianxaydung { get; set; }
        public string Thoigianvanhanh { get; set; }
        public int? Somaybomtieunuoc { get; set; }
        public int? Somaybomtuoinuoc { get; set; }
        public double? Khanangbomtuoi { get; set; }
        public double? Luuluongtuoithietke { get; set; }
        public double? Luuluongtuoithucte { get; set; }
        public double? Dientichtuoithietke { get; set; }
        public double? Dientichtuoithucte { get; set; }
        public double? Khanangbomtieu { get; set; }
        public double? Luuluongtieuthietke { get; set; }
        public double? Luuluongtieuthucte { get; set; }
        public double? Dientichtieuthietke { get; set; }
        public double? Dientichtieuthucte { get; set; }
        public int? IdDonvivanhanh { get; set; }
        public int? IdVanhanh { get; set; }
        public int? Sohothietke { get; set; }
        public int? Sohothucte { get; set; }
        public double? Phantramcongsuathuuich { get; set; }
        public int? IdSong { get; set; }
        public int? IdHochua { get; set; }
        public int? IdHotunhien { get; set; }
        public string Hientrang { get; set; }
        public string Note { get; set; }
        public string ImgLink { get; set; }
        public string CreatedAt { get; set; }
        public string UpdatedAt { get; set; }
        public short? DeletedAt { get; set; }
        public short? Status { get; set; }
    }
}
