﻿using System;
using System.Collections.Generic;
using NetTopologySuite.Geometries;

namespace WaterManagement.Models.Models
{
    public partial class GMedia
    {
        public int Id { get; set; }
       // public Geometry Geom { get; set; }
        public int? IdObj { get; set; }
        public string Link { get; set; }
        public string Tieude { get; set; }
        public string Thoigiankhoitao { get; set; }
        public int? IdLoaitailieu { get; set; }
        public string Tacgia { get; set; }
        public string Nguoiky { get; set; }
        public string Note { get; set; }
        public string CreatedAt { get; set; }
        public string UpdatedAt { get; set; }
        public short? DeletedAt { get; set; }
        public short? ObjKey { get; set; }
    }
}
