﻿using System;
using System.Collections.Generic;

namespace WaterManagement.Models.Models
{
    public partial class GNhomthamso
    {
        public int Id { get; set; }
        public string Tennhom { get; set; }
        public string Kyhieunhom { get; set; }
    }
}
