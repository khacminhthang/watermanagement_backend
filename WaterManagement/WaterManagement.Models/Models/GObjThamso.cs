﻿using System;
using System.Collections.Generic;

namespace WaterManagement.Models.Models
{
    public partial class GObjThamso
    {
        public int Id { get; set; }
        public int? IdObj { get; set; }
        public short? ObjKey { get; set; }
        public int? IdThamso { get; set; }
    }
}
