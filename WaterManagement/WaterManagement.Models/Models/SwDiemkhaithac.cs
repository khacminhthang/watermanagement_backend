﻿using System;
using System.Collections.Generic;
using NetTopologySuite.Geometries;

namespace WaterManagement.Models.Models
{
    public partial class SwDiemkhaithac
    {
        public int Id { get; set; }
        public Geometry Geom { get; set; }
        public string Sohieudiem { get; set; }
        public string Tendiem { get; set; }
        public double? Toadox { get; set; }
        public double? Toadoy { get; set; }
        public double? Caodoz { get; set; }
        public int? Srid { get; set; }
        public string Vitri { get; set; }
        public string Matinh { get; set; }
        public string Mahuyen { get; set; }
        public string Maxa { get; set; }
        public string Thoigianxaydung { get; set; }
        public string Thoigianvanhanh { get; set; }
        public int? IdDonviquanly { get; set; }
        public int? IdChusudung { get; set; }
        public int? IdMucdichsudungchinh { get; set; }
        public double? Luuluongchopheplonnhat { get; set; }
        public bool? Cothietbigiamsat { get; set; }
        public int? IdVanhanh { get; set; }
        public string Thoiluongkhaithac { get; set; }
        public string Nhamay { get; set; }
        public int? IdSong { get; set; }
        public string Hochua { get; set; }
        public string Hotunhien { get; set; }
        public bool? Dacapphep { get; set; }
        public string Note { get; set; }
        public string ImgLink { get; set; }
        public string CreatedAt { get; set; }
        public string UpdatedAt { get; set; }
        public short? DeletedAt { get; set; }
        public short? ObjKey { get; set; }
        public int? IdBody { get; set; }
        public short? BodyKey { get; set; }
        public string Guid { get; set; }
    }
}
