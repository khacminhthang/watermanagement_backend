﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.AspNetCore.Identity;
namespace WaterManagement.Models.Models
{
    public partial class User : IdentityUser
    {
        [Column(TypeName = "varchar(150)")]
        public string FullName { get; set; }
    }
}
