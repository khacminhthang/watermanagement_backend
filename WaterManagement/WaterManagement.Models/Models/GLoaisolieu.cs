﻿using System;
using System.Collections.Generic;

namespace WaterManagement.Models.Models
{
    public partial class GLoaisolieu
    {
        public int Id { get; set; }
        public string Tenloaisolieu { get; set; }
        public string Tenviettat { get; set; }
    }
}
