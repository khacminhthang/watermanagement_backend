﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WaterManagement.Models.Models
{
    public partial class DatabaseContext : IdentityDbContext
    {
        public DatabaseContext()
        {
        }

        public DatabaseContext(DbContextOptions<DatabaseContext> options)
           : base(options)
        {
        }


        public virtual DbSet<Dvhc> Dvhc { get; set; }
        public virtual DbSet<GCanhan> GCanhan { get; set; }
        public virtual DbSet<GCongty> GCongty { get; set; }
        public virtual DbSet<GCoquantochuc> GCoquantochuc { get; set; }
        public virtual DbSet<GDlquantracts> GDlquantracts { get; set; }
        public virtual DbSet<GDlquantractsHis> GDlquantractsHis { get; set; }
        public virtual DbSet<GDonvido> GDonvido { get; set; }
        public virtual DbSet<GDuan> GDuan { get; set; }
        public virtual DbSet<GLoaisolieu> GLoaisolieu { get; set; }
        public virtual DbSet<GMedia> GMedia { get; set; }
        public virtual DbSet<GNhomthamso> GNhomthamso { get; set; }
        public virtual DbSet<GObjThamso> GObjThamso { get; set; }
        public virtual DbSet<GObjmedia> GObjmedia { get; set; }
        public virtual DbSet<GPhantich> GPhantich { get; set; }
        public virtual DbSet<GThamso> GThamso { get; set; }
        public virtual DbSet<GTieuchuan> GTieuchuan { get; set; }
        public virtual DbSet<GTieuchuanchatluong> GTieuchuanchatluong { get; set; }
        public virtual DbSet<SwDiemkhaithac> SwDiemkhaithac { get; set; }
        public virtual DbSet<SwDiemxathai> SwDiemxathai { get; set; }
        public virtual DbSet<SwMaybomdiemkhaithac> SwMaybomdiemkhaithac { get; set; }
        public virtual DbSet<SwMaybomtrambom> SwMaybomtrambom { get; set; }
        public virtual DbSet<SwTrambom> SwTrambom { get; set; }
        public virtual DbSet<Test> Test { get; set; }
        public DbSet<User> User { get; set; }

        //        protected override void onconfiguring(dbcontextoptionsbuilder optionsbuilder)
        //        {
        //            if (!optionsbuilder.isconfigured)
        //            {
        //#warning to protect potentially sensitive information in your connection string, you should move it out of source code. see http://go.microsoft.com/fwlink/?linkid=723263 for guidance on storing connection strings.
        //                optionsbuilder.usenpgsql("host=localhost;port=5432;database=watergis;username=postgres;password=admin", x => x.usenettopologysuite());
        //            }
        //        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasPostgresExtension("postgis");

       
            modelBuilder.Entity<Dvhc>(entity =>
            {
                entity.ToTable("dvhc");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Mahuyen)
                    .HasColumnName("mahuyen")
                    .HasMaxLength(40);

                entity.Property(e => e.Matinh)
                    .HasColumnName("matinh")
                    .HasMaxLength(40);

                entity.Property(e => e.Maxa)
                    .HasColumnName("maxa")
                    .HasMaxLength(40);

                entity.Property(e => e.Parentid).HasColumnName("parentid");

                entity.Property(e => e.Status).HasColumnName("status");

                entity.Property(e => e.Tendvhc)
                    .HasColumnName("tendvhc")
                    .HasMaxLength(150);
            });

            modelBuilder.Entity<GCanhan>(entity =>
            {
                entity.ToTable("g_canhan");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasMaxLength(40)
                    .IsFixedLength();

                entity.Property(e => e.DeletedAt)
                    .HasColumnName("deleted_at")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Diachi)
                    .HasColumnName("diachi")
                    .HasMaxLength(255);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(40);

                entity.Property(e => e.Hovaten)
                    .HasColumnName("hovaten")
                    .HasMaxLength(40);

                entity.Property(e => e.ImgLink).HasColumnName("img_link");

                entity.Property(e => e.Loaigiayto)
                    .HasColumnName("loaigiayto")
                    .HasMaxLength(40);

                entity.Property(e => e.Mahuyen)
                    .HasColumnName("mahuyen")
                    .HasMaxLength(40);

                entity.Property(e => e.Matinh)
                    .HasColumnName("matinh")
                    .HasMaxLength(40);

                entity.Property(e => e.Maxa)
                    .HasColumnName("maxa")
                    .HasMaxLength(40);

                entity.Property(e => e.Note)
                    .HasColumnName("note")
                    .HasDefaultValueSql("'FALSE'::text");

                entity.Property(e => e.Socmthochieu)
                    .HasColumnName("socmthochieu")
                    .HasMaxLength(40);

                entity.Property(e => e.Sodienthoai)
                    .HasColumnName("sodienthoai")
                    .HasMaxLength(40);

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasMaxLength(40)
                    .IsFixedLength();
            });

            modelBuilder.Entity<GCongty>(entity =>
            {
                entity.ToTable("g_congty");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasMaxLength(40)
                    .IsFixedLength();

                entity.Property(e => e.DeletedAt)
                    .HasColumnName("deleted_at")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Diachi)
                    .HasColumnName("diachi")
                    .HasMaxLength(255);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(255);

                entity.Property(e => e.ImgLink).HasColumnName("img_link");

                entity.Property(e => e.Mahuyen)
                    .HasColumnName("mahuyen")
                    .HasMaxLength(40);

                entity.Property(e => e.Matinh)
                    .HasColumnName("matinh")
                    .HasMaxLength(40);

                entity.Property(e => e.Maxa)
                    .HasColumnName("maxa")
                    .HasMaxLength(40);

                entity.Property(e => e.Note).HasColumnName("note");

                entity.Property(e => e.Sodienthoai)
                    .HasColumnName("sodienthoai")
                    .HasMaxLength(40);

                entity.Property(e => e.Tencongty)
                    .HasColumnName("tencongty")
                    .HasMaxLength(255);

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasMaxLength(40)
                    .IsFixedLength();
            });

            modelBuilder.Entity<GCoquantochuc>(entity =>
            {
                entity.ToTable("g_coquantochuc");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasMaxLength(40)
                    .IsFixedLength();

                entity.Property(e => e.DeletedAt)
                    .HasColumnName("deleted_at")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Diachi)
                    .HasColumnName("diachi")
                    .HasMaxLength(255);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(40);

                entity.Property(e => e.Idcha).HasColumnName("idcha");

                entity.Property(e => e.ImgLink).HasColumnName("img_link");

                entity.Property(e => e.Mahuyen)
                    .HasColumnName("mahuyen")
                    .HasMaxLength(40);

                entity.Property(e => e.Matinh)
                    .HasColumnName("matinh")
                    .HasMaxLength(40);

                entity.Property(e => e.Maxa)
                    .HasColumnName("maxa")
                    .HasMaxLength(40);

                entity.Property(e => e.Note).HasColumnName("note");

                entity.Property(e => e.Sodienthoai)
                    .HasColumnName("sodienthoai")
                    .HasMaxLength(40);

                entity.Property(e => e.Tencoquan)
                    .HasColumnName("tencoquan")
                    .HasMaxLength(255);

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasMaxLength(40)
                    .IsFixedLength();
            });

            modelBuilder.Entity<GDlquantracts>(entity =>
            {
                entity.ToTable("g_dlquantracts");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasMaxLength(40);

                entity.Property(e => e.DeletedAt)
                    .HasColumnName("deleted_at")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.FullTime)
                    .HasColumnName("full_time")
                    .HasMaxLength(40);

                entity.Property(e => e.IdThamsodo).HasColumnName("id_thamsodo");

                entity.Property(e => e.IdTramquantrac).HasColumnName("id_tramquantrac");

                entity.Property(e => e.Ketquado).HasColumnName("ketquado");

                entity.Property(e => e.Loaisolieu)
                    .HasColumnName("loaisolieu")
                    .HasMaxLength(40);

                entity.Property(e => e.Namdo).HasColumnName("namdo");

                entity.Property(e => e.Ngaydo).HasColumnName("ngaydo");

                entity.Property(e => e.Note).HasColumnName("note");

                entity.Property(e => e.ObjKey).HasColumnName("obj_key");

                entity.Property(e => e.Phuongphapdo)
                    .HasColumnName("phuongphapdo")
                    .HasMaxLength(40);

                entity.Property(e => e.Status).HasColumnName("status");

                entity.Property(e => e.Thangdo).HasColumnName("thangdo");

                entity.Property(e => e.Thoigiando)
                    .HasColumnName("thoigiando")
                    .HasMaxLength(40);

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasMaxLength(40);
            });

            modelBuilder.Entity<GDlquantractsHis>(entity =>
            {
                entity.ToTable("g_dlquantracts_his");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasMaxLength(40);

                entity.Property(e => e.DeletedAt)
                    .HasColumnName("deleted_at")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.FullTime)
                    .HasColumnName("full_time")
                    .HasMaxLength(40);

                entity.Property(e => e.IdThamsodo).HasColumnName("id_thamsodo");

                entity.Property(e => e.IdTramquantrac).HasColumnName("id_tramquantrac");

                entity.Property(e => e.Ketquado).HasColumnName("ketquado");

                entity.Property(e => e.Loaisolieu)
                    .HasColumnName("loaisolieu")
                    .HasMaxLength(40);

                entity.Property(e => e.Namdo).HasColumnName("namdo");

                entity.Property(e => e.Ngaydo).HasColumnName("ngaydo");

                entity.Property(e => e.Note).HasColumnName("note");

                entity.Property(e => e.ObjKey).HasColumnName("obj_key");

                entity.Property(e => e.Phuongphapdo)
                    .HasColumnName("phuongphapdo")
                    .HasMaxLength(40);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Thangdo).HasColumnName("thangdo");

                entity.Property(e => e.Thoigiando)
                    .HasColumnName("thoigiando")
                    .HasMaxLength(40);

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasMaxLength(40);
            });

            modelBuilder.Entity<GDonvido>(entity =>
            {
                entity.ToTable("g_donvido");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Kyhieudonvido)
                    .HasColumnName("kyhieudonvido")
                    .HasMaxLength(40);

                entity.Property(e => e.Tendonvido)
                    .IsRequired()
                    .HasColumnName("tendonvido")
                    .HasMaxLength(40);

                entity.Property(e => e.IdNhomdonvido)
                    .HasColumnName("idNhomdonvido")
                    .HasMaxLength(40);

            });

            modelBuilder.Entity<GDuan>(entity =>
            {
                entity.HasKey(e => e.IdProject)
                    .HasName("pk_g_duan");

                entity.ToTable("g_duan");

                entity.Property(e => e.IdProject).HasColumnName("id_project");

                entity.Property(e => e.CompleteDate)
                    .HasColumnName("complete_date")
                    .HasMaxLength(40);

                entity.Property(e => e.ProjectCode).HasColumnName("project_code");

                entity.Property(e => e.ProjectLeader).HasColumnName("project_leader");

                entity.Property(e => e.ProjectName).HasColumnName("project_name");

                entity.Property(e => e.SignedAgency).HasColumnName("signed_agency");

                entity.Property(e => e.SignedDate)
                    .HasColumnName("signed_date")
                    .HasMaxLength(40);
            });

            modelBuilder.Entity<GLoaisolieu>(entity =>
            {
                entity.ToTable("g_loaisolieu");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Tenloaisolieu)
                    .HasColumnName("tenloaisolieu")
                    .HasMaxLength(255);

                entity.Property(e => e.Tenviettat)
                    .HasColumnName("tenviettat")
                    .HasMaxLength(40);
            });

            modelBuilder.Entity<GMedia>(entity =>
            {
                entity.ToTable("g_media");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasMaxLength(40)
                    .IsFixedLength();

                entity.Property(e => e.DeletedAt)
                    .HasColumnName("deleted_at")
                    .HasDefaultValueSql("0");

                //entity.Property(e => e.Geom).HasColumnName("geom");

                entity.Property(e => e.IdLoaitailieu).HasColumnName("id_loaitailieu");

                entity.Property(e => e.IdObj).HasColumnName("id_obj");

                entity.Property(e => e.Link).HasColumnName("link");

                entity.Property(e => e.Nguoiky)
                    .HasColumnName("nguoiky")
                    .HasMaxLength(40);

                entity.Property(e => e.Note).HasColumnName("note");

                entity.Property(e => e.ObjKey).HasColumnName("obj_key");

                entity.Property(e => e.Tacgia)
                    .HasColumnName("tacgia")
                    .HasMaxLength(40);

                entity.Property(e => e.Thoigiankhoitao)
                    .HasColumnName("thoigiankhoitao")
                    .HasMaxLength(40);

                entity.Property(e => e.Tieude)
                    .HasColumnName("tieude")
                    .HasMaxLength(255);

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasMaxLength(40)
                    .IsFixedLength();

            });

            modelBuilder.Entity<GNhomthamso>(entity =>
            {
                entity.ToTable("g_nhomthamso");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Kyhieunhom)
                    .HasColumnName("kyhieunhom")
                    .HasMaxLength(40);

                entity.Property(e => e.Tennhom)
                    .HasColumnName("tennhom")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<GObjThamso>(entity =>
            {
                entity.ToTable("g_obj_thamso");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IdObj).HasColumnName("id_obj");

                entity.Property(e => e.IdThamso).HasColumnName("id_thamso");

                entity.Property(e => e.ObjKey).HasColumnName("obj_key");
            });

            modelBuilder.Entity<GObjmedia>(entity =>
            {
                entity.ToTable("g_objmedia");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasMaxLength(40)
                    .IsFixedLength();

                entity.Property(e => e.DeletedAt).HasColumnName("deleted_at");

                entity.Property(e => e.IdMedia).HasColumnName("id_media");

                entity.Property(e => e.IdObj).HasColumnName("id_obj");

                entity.Property(e => e.IdObjmediaType).HasColumnName("id_objmedia_type");

                entity.Property(e => e.MediaType)
                    .HasColumnName("media_type")
                    .HasMaxLength(40);

                entity.Property(e => e.ObjKey).HasColumnName("obj_key");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasMaxLength(40)
                    .IsFixedLength();
            });

            modelBuilder.Entity<GPhantich>(entity =>
            {
                entity.ToTable("g_phantich");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasMaxLength(40);

                entity.Property(e => e.DeletedAt)
                    .HasColumnName("deleted_at")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdMau).HasColumnName("id_mau");

                entity.Property(e => e.Nguoikiemtra)
                    .HasColumnName("nguoikiemtra")
                    .HasMaxLength(40);

                entity.Property(e => e.Nguoiphantich)
                    .HasColumnName("nguoiphantich")
                    .HasMaxLength(40);

                entity.Property(e => e.Note).HasColumnName("note");

                entity.Property(e => e.Sohieuphantich)
                    .HasColumnName("sohieuphantich")
                    .HasMaxLength(40);

                entity.Property(e => e.Thoigianphantich)
                    .HasColumnName("thoigianphantich")
                    .HasMaxLength(40);

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasMaxLength(40);
            });

            modelBuilder.Entity<GThamso>(entity =>
            {
                entity.ToTable("g_thamso");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IdDonvidomacdinh).HasColumnName("id_donvidomacdinh");

                entity.Property(e => e.IdNhomthamso).HasColumnName("id_nhomthamso");

                entity.Property(e => e.Kyhieuthamso).HasColumnName("kyhieuthamso");

                entity.Property(e => e.Tenthamso).HasColumnName("tenthamso");
            });

            modelBuilder.Entity<GTieuchuan>(entity =>
            {
                entity.ToTable("g_tieuchuan");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Coquanbanhanh)
                    .HasColumnName("coquanbanhanh")
                    .HasMaxLength(255);

                entity.Property(e => e.Hientrang)
                    .HasColumnName("hientrang")
                    .HasMaxLength(40);

                entity.Property(e => e.IdTailieu).HasColumnName("id_tailieu");

                entity.Property(e => e.Sohieutieuchuan)
                    .HasColumnName("sohieutieuchuan")
                    .HasMaxLength(40);

                entity.Property(e => e.Tentieuchuan)
                    .HasColumnName("tentieuchuan")
                    .HasMaxLength(255);

                entity.Property(e => e.Thoigianbanhanh)
                    .HasColumnName("thoigianbanhanh")
                    .HasMaxLength(40);
            });

            modelBuilder.Entity<GTieuchuanchatluong>(entity =>
            {
                entity.ToTable("g_tieuchuanchatluong");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Captieuchuan)
                    .HasColumnName("captieuchuan")
                    .HasMaxLength(40);

                entity.Property(e => e.Gioihanduoi).HasColumnName("gioihanduoi");

                entity.Property(e => e.Gioihantren).HasColumnName("gioihantren");

                entity.Property(e => e.IdDonvido).HasColumnName("id_donvido");

                entity.Property(e => e.IdThamso).HasColumnName("id_thamso");

                entity.Property(e => e.IdTieuchuan).HasColumnName("id_tieuchuan");

                entity.Property(e => e.Mucdogiamsat)
                    .HasColumnName("mucdogiamsat")
                    .HasMaxLength(40);

                entity.Property(e => e.Phuongphapthu)
                    .HasColumnName("phuongphapthu")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<SwDiemkhaithac>(entity =>
            {
                entity.ToTable("sw_diemkhaithac");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BodyKey).HasColumnName("body_key");

                entity.Property(e => e.Caodoz).HasColumnName("caodoz");

                entity.Property(e => e.Cothietbigiamsat).HasColumnName("cothietbigiamsat");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasMaxLength(40);

                entity.Property(e => e.Dacapphep).HasColumnName("dacapphep");

                entity.Property(e => e.DeletedAt)
                    .HasColumnName("deleted_at")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Geom).HasColumnName("geom");

                entity.Property(e => e.Guid)
                    .HasColumnName("guid")
                    .HasColumnType("character varying");

                entity.Property(e => e.Hochua)
                    .HasColumnName("hochua")
                    .HasMaxLength(255);

                entity.Property(e => e.Hotunhien)
                    .HasColumnName("hotunhien")
                    .HasMaxLength(255);

                entity.Property(e => e.IdBody).HasColumnName("id_body");

                entity.Property(e => e.IdChusudung).HasColumnName("id_chusudung");

                entity.Property(e => e.IdDonviquanly).HasColumnName("id_donviquanly");

                entity.Property(e => e.IdMucdichsudungchinh).HasColumnName("id_mucdichsudungchinh");

                entity.Property(e => e.IdSong).HasColumnName("id_song");

                entity.Property(e => e.IdVanhanh).HasColumnName("id_vanhanh");

                entity.Property(e => e.ImgLink).HasColumnName("img_link");

                entity.Property(e => e.Luuluongchopheplonnhat)
                    .HasColumnName("luuluongchopheplonnhat")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Mahuyen)
                    .HasColumnName("mahuyen")
                    .HasMaxLength(40);

                entity.Property(e => e.Matinh)
                    .HasColumnName("matinh")
                    .HasMaxLength(40);

                entity.Property(e => e.Maxa)
                    .HasColumnName("maxa")
                    .HasMaxLength(40);

                entity.Property(e => e.Nhamay)
                    .HasColumnName("nhamay")
                    .HasMaxLength(255);

                entity.Property(e => e.Note).HasColumnName("note");

                entity.Property(e => e.ObjKey).HasColumnName("obj_key");

                entity.Property(e => e.Sohieudiem)
                    .HasColumnName("sohieudiem")
                    .HasMaxLength(40);

                entity.Property(e => e.Srid).HasColumnName("srid");

                entity.Property(e => e.Tendiem)
                    .HasColumnName("tendiem")
                    .HasMaxLength(40);

                entity.Property(e => e.Thoigianvanhanh)
                    .HasColumnName("thoigianvanhanh")
                    .HasMaxLength(40);

                entity.Property(e => e.Thoigianxaydung)
                    .HasColumnName("thoigianxaydung")
                    .HasMaxLength(40);

                entity.Property(e => e.Thoiluongkhaithac)
                    .HasColumnName("thoiluongkhaithac")
                    .HasMaxLength(40)
                    .IsFixedLength();

                entity.Property(e => e.Toadox).HasColumnName("toadox");

                entity.Property(e => e.Toadoy).HasColumnName("toadoy");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasMaxLength(40);

                entity.Property(e => e.Vitri)
                    .HasColumnName("vitri")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<SwDiemxathai>(entity =>
            {
                entity.ToTable("sw_diemxathai");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BodyKey).HasColumnName("body_key");

                entity.Property(e => e.Caodoz).HasColumnName("caodoz");

                entity.Property(e => e.Cothietbigiamsat).HasColumnName("cothietbigiamsat");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasMaxLength(40);

                entity.Property(e => e.Dacapphep).HasColumnName("dacapphep");

                entity.Property(e => e.DeletedAt)
                    .HasColumnName("deleted_at")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Geom).HasColumnName("geom");

                entity.Property(e => e.Guid)
                    .HasColumnName("guid")
                    .HasColumnType("character varying");

                entity.Property(e => e.Hinhthucdan)
                    .HasColumnName("hinhthucdan")
                    .HasMaxLength(40);

                entity.Property(e => e.IdBody).HasColumnName("id_body");

                entity.Property(e => e.IdChusudung).HasColumnName("id_chusudung");

                entity.Property(e => e.IdDonviquanly).HasColumnName("id_donviquanly");

                entity.Property(e => e.IdSong).HasColumnName("id_song");

                entity.Property(e => e.ImgLink).HasColumnName("img_link");

                entity.Property(e => e.Luuluonglonnhatchophep).HasColumnName("luuluonglonnhatchophep");

                entity.Property(e => e.Luuluongxatrungbinh).HasColumnName("luuluongxatrungbinh");

                entity.Property(e => e.Mahuyen)
                    .HasColumnName("mahuyen")
                    .HasMaxLength(40);

                entity.Property(e => e.Matinh)
                    .HasColumnName("matinh")
                    .HasMaxLength(40);

                entity.Property(e => e.Maxa)
                    .HasColumnName("maxa")
                    .HasMaxLength(40);

                entity.Property(e => e.Note)
                    .HasColumnName("note")
                    .HasDefaultValueSql("'0'::text");

                entity.Property(e => e.ObjKey).HasColumnName("obj_key");

                entity.Property(e => e.Phuongthucxa)
                    .HasColumnName("phuongthucxa")
                    .HasMaxLength(40)
                    .IsFixedLength();

                entity.Property(e => e.Quychuanapdung)
                    .HasColumnName("quychuanapdung")
                    .HasMaxLength(40)
                    .IsFixedLength();

                entity.Property(e => e.Sohieudiem)
                    .HasColumnName("sohieudiem")
                    .HasMaxLength(40);

                entity.Property(e => e.Srid).HasColumnName("srid");

                entity.Property(e => e.Tendiem)
                    .HasColumnName("tendiem")
                    .HasMaxLength(255);

                entity.Property(e => e.Thoigianvanhanh)
                    .HasColumnName("thoigianvanhanh")
                    .HasMaxLength(40);

                entity.Property(e => e.Thoigianxaydung)
                    .HasColumnName("thoigianxaydung")
                    .HasMaxLength(40);

                entity.Property(e => e.Thuyvuctiepnhan)
                    .HasColumnName("thuyvuctiepnhan")
                    .HasMaxLength(255);

                entity.Property(e => e.Toadox).HasColumnName("toadox");

                entity.Property(e => e.Toadoy).HasColumnName("toadoy");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasMaxLength(40);

                entity.Property(e => e.Vitri)
                    .HasColumnName("vitri")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<SwMaybomdiemkhaithac>(entity =>
            {
                entity.ToTable("sw_maybomdiemkhaithac");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Congsuat).HasColumnName("congsuat");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasMaxLength(40);

                entity.Property(e => e.DeletedAt)
                    .HasColumnName("deleted_at")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Dosaulapdat).HasColumnName("dosaulapdat");

                entity.Property(e => e.IdDiemkhaithac).HasColumnName("id_diemkhaithac");

                entity.Property(e => e.Loaimaybom)
                    .HasColumnName("loaimaybom")
                    .HasMaxLength(40);

                entity.Property(e => e.Note).HasColumnName("note");

                entity.Property(e => e.Thoigianlapdat)
                    .HasColumnName("thoigianlapdat")
                    .HasMaxLength(40);

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasMaxLength(40);
            });

            modelBuilder.Entity<SwMaybomtrambom>(entity =>
            {
                entity.ToTable("sw_maybomtrambom");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Congsuat).HasColumnName("congsuat");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasMaxLength(40);

                entity.Property(e => e.DeletedAt)
                    .HasColumnName("deleted_at")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdTrambom).HasColumnName("id_trambom");

                entity.Property(e => e.Loaimaybom)
                    .HasColumnName("loaimaybom")
                    .HasMaxLength(40);

                entity.Property(e => e.Note).HasColumnName("note");

                entity.Property(e => e.Thoigianlapdat)
                    .HasColumnName("thoigianlapdat")
                    .HasMaxLength(40);

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasMaxLength(40);
            });

            modelBuilder.Entity<SwTrambom>(entity =>
            {
                entity.ToTable("sw_trambom");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Caodoz).HasColumnName("caodoz");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasMaxLength(40);

                entity.Property(e => e.DeletedAt)
                    .HasColumnName("deleted_at")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Dientichtieuthietke).HasColumnName("dientichtieuthietke");

                entity.Property(e => e.Dientichtieuthucte).HasColumnName("dientichtieuthucte");

                entity.Property(e => e.Dientichtuoithietke).HasColumnName("dientichtuoithietke");

                entity.Property(e => e.Dientichtuoithucte).HasColumnName("dientichtuoithucte");

                entity.Property(e => e.Geom).HasColumnName("geom");

                entity.Property(e => e.Hientrang)
                    .HasColumnName("hientrang")
                    .HasMaxLength(40);

                entity.Property(e => e.IdDonvivanhanh).HasColumnName("id_donvivanhanh");

                entity.Property(e => e.IdHochua).HasColumnName("id_hochua");

                entity.Property(e => e.IdHotunhien).HasColumnName("id_hotunhien");

                entity.Property(e => e.IdSong).HasColumnName("id_song");

                entity.Property(e => e.IdVanhanh).HasColumnName("id_vanhanh");

                entity.Property(e => e.ImgLink).HasColumnName("img_link");

                entity.Property(e => e.Khanangbomtieu).HasColumnName("khanangbomtieu");

                entity.Property(e => e.Khanangbomtuoi).HasColumnName("khanangbomtuoi");

                entity.Property(e => e.Luuluongtieuthietke).HasColumnName("luuluongtieuthietke");

                entity.Property(e => e.Luuluongtieuthucte).HasColumnName("luuluongtieuthucte");

                entity.Property(e => e.Luuluongtuoithietke).HasColumnName("luuluongtuoithietke");

                entity.Property(e => e.Luuluongtuoithucte).HasColumnName("luuluongtuoithucte");

                entity.Property(e => e.Mahuyen)
                    .HasColumnName("mahuyen")
                    .HasMaxLength(40);

                entity.Property(e => e.Matinh)
                    .HasColumnName("matinh")
                    .HasMaxLength(40);

                entity.Property(e => e.Maxa)
                    .HasColumnName("maxa")
                    .HasMaxLength(40);

                entity.Property(e => e.Note).HasColumnName("note");

                entity.Property(e => e.ObjKey).HasColumnName("obj_key");

                entity.Property(e => e.Phantramcongsuathuuich).HasColumnName("phantramcongsuathuuich");

                entity.Property(e => e.Sohieutrambom)
                    .HasColumnName("sohieutrambom")
                    .HasMaxLength(40);

                entity.Property(e => e.Sohothietke).HasColumnName("sohothietke");

                entity.Property(e => e.Sohothucte).HasColumnName("sohothucte");

                entity.Property(e => e.Somaybomtieunuoc).HasColumnName("somaybomtieunuoc");

                entity.Property(e => e.Somaybomtuoinuoc).HasColumnName("somaybomtuoinuoc");

                entity.Property(e => e.Srid).HasColumnName("srid");

                entity.Property(e => e.Status).HasColumnName("status");

                entity.Property(e => e.Tentrambom)
                    .HasColumnName("tentrambom")
                    .HasMaxLength(40);

                entity.Property(e => e.Thoigianvanhanh)
                    .HasColumnName("thoigianvanhanh")
                    .HasMaxLength(40);

                entity.Property(e => e.Thoigianxaydung)
                    .HasColumnName("thoigianxaydung")
                    .HasMaxLength(40);

                entity.Property(e => e.Toadox).HasColumnName("toadox");

                entity.Property(e => e.Toadoy).HasColumnName("toadoy");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasMaxLength(40);

                entity.Property(e => e.Vitri)
                    .HasColumnName("vitri")
                    .HasMaxLength(40);
            });

            modelBuilder.Entity<Test>(entity =>
            {
                entity.ToTable("test");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasMaxLength(40);

                entity.Property(e => e.DeletedAt)
                    .HasColumnName("deleted_at")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.FullTime)
                    .HasColumnName("full_time")
                    .HasMaxLength(40);

                entity.Property(e => e.IdThamsodo).HasColumnName("id_thamsodo");

                entity.Property(e => e.IdTramquantrac).HasColumnName("id_tramquantrac");

                entity.Property(e => e.Ketquado).HasColumnName("ketquado");

                entity.Property(e => e.Loaisolieu)
                    .HasColumnName("loaisolieu")
                    .HasMaxLength(40);

                entity.Property(e => e.Namdo).HasColumnName("namdo");

                entity.Property(e => e.Ngaydo).HasColumnName("ngaydo");

                entity.Property(e => e.Note).HasColumnName("note");

                entity.Property(e => e.ObjKey).HasColumnName("obj_key");

                entity.Property(e => e.Phuongphapdo)
                    .HasColumnName("phuongphapdo")
                    .HasMaxLength(40);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Thangdo).HasColumnName("thangdo");

                entity.Property(e => e.Thoigiando)
                    .HasColumnName("thoigiando")
                    .HasMaxLength(40);

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasMaxLength(40);
            });

            modelBuilder.HasSequence("sw_diemkhaithac_id_seq");

            modelBuilder.HasSequence("sw_diemxathai_id_seq");

            modelBuilder.HasSequence("sw_maybomdiemkhaithac_id_seq");

            modelBuilder.HasSequence("sw_maybomtrambom_id_seq");

            modelBuilder.HasSequence("sw_trambom_id_seq");

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
